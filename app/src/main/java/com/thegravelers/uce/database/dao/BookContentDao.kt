/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.Dao
import com.thegravelers.uce.database.entity.BookContent

/**
 * [BookContentDao] only uses the [BaseDao] functionality
 */
@Dao
interface BookContentDao: BaseDao<BookContent>