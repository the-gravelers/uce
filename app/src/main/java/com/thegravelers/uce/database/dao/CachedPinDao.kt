/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.*
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.CachedPin
import com.thegravelers.uce.database.entity.CachedPinWithInfo
import kotlinx.coroutines.flow.Flow

/**
 * Defines methods for using the [CachedPin] class with Room.
 */
@Dao
interface CachedPinDao : BaseDao<CachedPin> {

    /**
     * Clear the entire [cached_pins][CachedPin] table.
     */
    @Query("DELETE FROM cached_pins")
    suspend fun deleteAll()

    /**
     * Delete all entities of type [CachedPin] from the [cached_pins][CachedPin] table with a specific [CachedPin.pinId]
     * @param[key] The [Pin] that the [CachedPin] has a reference to.
     */
    @Query("DELETE FROM cached_pins WHERE pin_id = :key")
    suspend fun delete(key: Long)

    /**
     * Retrieves all of our references to our [cached_pins][CachedPin] in the [cached_pins][CachedPin] table.
     * The pins are sorted in an ascending order, so the oldest pins are first.
     * @return A list of [CachedPinWithInfo] objects that hold the [Pin]. It is encapsulated with a [Flow] object.
     */
    @Transaction
    @Query("SELECT * FROM cached_pins ORDER BY last_accessed ASC")
    suspend fun getCachedPins(): List<CachedPinWithInfo>

    /**
     * Does the same as [getCachedPins], but limits the amount of results returned.
     * @param[amount] The amount of entries you want returned.
     */
    @Transaction
    @Query("SELECT * FROM cached_pins ORDER BY last_accessed ASC LIMIT :amount")
    fun getOldestPins(amount: Int): Flow<List<CachedPinWithInfo>>
}