/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.*
import com.thegravelers.uce.database.entity.*
import kotlinx.coroutines.flow.Flow

/**
 * Defines methods for using the [Pin] class with Room.
 */
@Dao
interface PinDao : BaseDao<Pin> {

    /**
     * Clear the entire [pins][Pin] table.
     */
    @Query("DELETE FROM pins")
    suspend fun deleteAll()

    /**
     * Delete a specific [Pin] from the [pins][Pin] table.
     * @param[key] The [Pin.id] from the object that needs to be deleted.
     */
    @Query("DELETE FROM pins WHERE id = :key")
    suspend fun delete(key: Long)

    /**
     * Selects and returns the row that matches the supplied [Pin.id], which is our key.
     * @param key The [Pin.id] of the target [Pin].
     */
    @Query("SELECT * from pins WHERE id = :key")
    fun get(key: Long): Pin

    /**
     * Selects and returns the row that matches the supplied [Pin.id], which is our key.
     * @param key The [Pin.id] of the target [Pin].
     */
    @Query("SELECT * from pins WHERE id = :key")
    fun getFlow(key: Long): Flow<Pin>

    /**
     * Selects and returns the first row that matches the supplied [Pin.serverId].
     * @param id The [Pin.serverId] of the target [Pin].
     * @return A Flow object with the Pin inside. This Flow object can be empty!
     */
    @Query("SELECT * from pins WHERE server_id = :id")
    suspend fun getWithServerId(id: Long): Pin?
    /**
     * Selects and returns all rows in the [pins][Pin] table, sorted by time uploaded in descending order,
     * as live data.
     */
    @Query("SELECT * FROM pins ORDER BY last_edited DESC")
    fun getAllFlow(): Flow<List<Pin>>

    /**
     * Selects and returns all rows in the [pins][Pin] table, sorted by time uploaded in descending order,
     * as live data.
     */
    @Query("SELECT * FROM pins ORDER BY last_edited DESC")
    fun getAll(): List<Pin>

    /**
     * Selects and returns all [pins][Pin] that belong to a specific [User].
     * @param[user] The [id][User.id] from the user that we want the pins.
     */
    @Query("SELECT * FROM pins WHERE user_id = :user")
    fun getAllFromUser(user: Long): Flow<List<Pin>>

    /**
     * Selects all [Pin] objects that are marked as saved by the user.
     */
    @Query("SELECT * FROM pins WHERE status = 'Saved'")
    fun getAllSaved(): Flow<List<Pin>>

    /**
     * Selects all [pins][Pin] that have been created by the user but that have not yet been uploaded to the server.
     * These pins can be identified by their [Pin.serverId] being negative.
     */
    @Query("SELECT * FROM pins WHERE server_id < 0")
    suspend fun getAllLocalWithContents(): List<PinWithContents>

    /**
     * Returns the info of a single [Pin] in our [pins][Pin] table combined with its [PinContent].
     * @param[key] The [id][Pin.id] of the [Pin].
     */
    @Transaction
    @Query("SELECT * FROM pins WHERE id = :key")
    suspend fun getPinWithContents(key: Long): PinWithContents

    /**
     * Selects all pins that belong to the [PinGroup] with a specific [PinGroup.id].
     * @param[key] The [PinGroup.id] of the [PinGroup] that we want returned.
     */
    @Transaction
    @Query("SELECT * FROM pin_groups WHERE id = :key")
    fun getPinsPerPinGroup(key: Long): Flow<PinGroupWithPins>

    @Query("SELECT * FROM pins WHERE status = 'Deleted'")
    suspend fun getDeletedPins(): List<Pin>
}