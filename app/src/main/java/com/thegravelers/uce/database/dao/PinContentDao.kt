/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.*
import com.thegravelers.uce.database.entity.*
import kotlinx.coroutines.flow.Flow

/**
 * Defines methods for using the [PinContent] class with Room.
 */
@Dao
interface PinContentDao : BaseDao<PinContent> {

    /**
     * Deletes a single pin_contents entry
     * @param[key] The id of the entry to be deleted
     */
    @Query("DELETE FROM pin_contents WHERE id = :key")
    suspend fun delete(key: Long)

    /**
     * Deletes all pin_contents entries associated with a single pin
     * @param[pinId] The [Pin] whose contents are to be deleted.
     */
    @Query("DELETE FROM pin_contents WHERE pin_id = :pinId")
    suspend fun deletePinContents(pinId: Long)

    /**
     * Selects and returns the row that matches the supplied [PinContent.id], which is our key, as flow data.
     * @param key The [PinContent.id] of the target [PinContent].
     */
    @Query("SELECT * from pin_contents WHERE id = :key ORDER BY content_index ASC")
    fun get (key: Long): Flow<PinContent>

    /**
     * Selects and returns the rows that matches the supplied pinID, which is our key, in order,
     * as live data.
     * @param pinId Id to match
     */
    @Query("SELECT * from pin_contents WHERE pin_id = :pinId ORDER BY content_index ASC")
    fun getAllFlow(pinId: Long): Flow<List<PinContent>>

    /**
     * Selects and returns the rows that matches the supplied pinID, which is our key, in order,
     * as live data.
     * @param pinId Id to match
     */
    @Query("SELECT * from pin_contents WHERE pin_id = :pinId ORDER BY content_index ASC")
    fun getAll(pinId: Long): List<PinContent>


}
