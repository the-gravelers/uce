/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.thegravelers.uce.database.entity.User
import kotlinx.coroutines.flow.Flow

/**
 * Defines methods for using the [User] class with Room.
 */
@Dao
interface UserDao : BaseDao<User> {

    /**
     * Deletes a [user][User] with the specified key as [User.id] from our users table.
     */
    @Query("DELETE FROM users WHERE id = :key")
    suspend fun delete(key: Long)

    /**
     * Selects and returns the row that matches the supplied [User.id], which is our key, as flow data.
     * @param key The [User.id] of the target [User].
     */
    @Query("SELECT * from users WHERE id = :key")
    fun getFlow(key: Long): Flow<User>

    /**
     * Selects and returns the row that matches the supplied [User.id], which is our key
     * @param key The [User.id] of the target [User].
     */
    @Query("SELECT * from users WHERE id = :key")
    suspend fun get(key: Long): User?

    /**
     * Selects and returns all stored users, as a flow object.
     */
    @Query("SELECT * from users")
    fun getAll(): Flow<List<User>>

    /**
     * Deletes all entries in the users table.
     */
    @Query("DELETE FROM users")
    suspend fun deleteAll()
}
