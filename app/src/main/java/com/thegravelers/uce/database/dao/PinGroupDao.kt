/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.PinGroup
import com.thegravelers.uce.database.entity.PinGroupWithPins
import com.thegravelers.uce.database.entity.PinPerPinGroup
import kotlinx.coroutines.flow.Flow

/**
 * Defines methods for using the [PinGroup] class with Room.
 */
@Dao
interface PinGroupDao : BaseDao<PinGroup>{
    /**
     * Inserts a new [PinPerPinGroup] object into the cross group relation table with [Pin] and [PinGroup]
     * @param pinPerPinGroup The new relation that you want inserted
     */
    @Insert
    suspend fun insertPinRelation(pinPerPinGroup: PinPerPinGroup)

    /**
     * Inserts a list of [PinPerPinGroup] objects into the cross group relation table with [Pin] and [PinGroup]
     * @param pinsPerPinGroups The list of [PinPerPinGroup] relations that you want inserted.
     */
    @Insert
    suspend fun insertPinRelation(pinsPerPinGroups: List<PinPerPinGroup>)

    /**
     * Deletes a specific [PinGroup] based on [PinGroup.id]
     * @param key The id of the pingroup that you want deleted
     */
    @Query("DELETE FROM pin_groups WHERE id = :key")
    suspend fun delete(key: Long)

    /**
     * Selects and returns the row that matches the supplied [PinGroup.id], which is our key, as flow data.
     * @param key The [PinGroup.id] of the target [PinGroup].
     */
    @Query("SELECT * from pin_groups WHERE id = :key")
    fun get(key: Long): Flow<PinGroup>

    /**
     * Returns all entries in the [pin_groups][PinGroup] table.
     */
    @Query("SELECT * from pin_groups")
    fun getAll(): Flow<List<PinGroup>>

    /**
     * Returns all [pinGroups][PinGroup] that match one of the given keys in the pin_groups table.
     */
    @Transaction
    @Query("SELECT * FROM pin_groups WHERE id IN (:keys)")
    fun getPinGroupsWithPins(vararg keys : Long): Flow<List<PinGroupWithPins>>

    /**
     * Deletes all entries in the pin_groups table.
     */
    @Query("DELETE FROM pin_groups")
    suspend fun deleteAll()
}
