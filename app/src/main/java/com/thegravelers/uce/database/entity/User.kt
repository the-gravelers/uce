/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

/**
 * The user entity in our database.
 * @param[id] The id of this user, should be from our server
 * @param[name] The name of this user
 * @param[userGroupId] The [UserGroup] that this user belongs to
 */
@Entity (tableName = "users",
        foreignKeys = [ForeignKey(
                onDelete = ForeignKey.CASCADE,
                entity = UserGroup::class,
                parentColumns = ["id"],
                childColumns = ["user_group_id"])])
data class User(
        @PrimaryKey
        var id: Long,

        @ColumnInfo(name = "name")
        var name: String = "",

        @ColumnInfo(name = "user_group_id")
        val userGroupId: Long = 0L
)

/**
 * The UserGroup entity in our database.
 * @param[id] The id of this UserGroup, should be synced with our server
 * @param[name] The name of this UserGroup
 * @param[pinGroupId] The [PinGroup] that belongs to this UserGroup.
 */
@Entity (tableName = "user_groups")
data class UserGroup(
        @PrimaryKey
        var id: Long,

        @ColumnInfo(name = "name")
        var name: String = "",

        @ColumnInfo(name = "pin_group_id")
        var pinGroupId: Long = 0L,
)