/*
 *  This program has been developed by students from the bachelor Computer
 *  Science at Utrecht University within the Software Project course. © Copyright
 *  Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.thegravelers.uce.util.getCurrentTimeSeconds

enum class PinStatus{
        Saved,
        Deleted,
        Nothing
}

/**
 * The Pin entity used in our database.
 * @param[title] The title of the pin
 * @param[lat] The latitude coordinates of the pin
 * @param[lon] The longitude coordinates of the pin
 * @param[text] The main block of text this pin displays
 * @param[status] If the pin is permanently saved in our local database
 * @param[serverId] The id that this pin has on our server
 * @param[lastEdited] The timestamp of the last time that we know this pin has changed (used to sync with server)
 * @param[userId] The id of the creator of this pin
 */
@Entity(tableName = "pins")
data class Pin(
        @ColumnInfo(name = "title")
        var title: String = "",

        @ColumnInfo(name = "latitude")
        var lat: Double = 0.0,

        @ColumnInfo(name = "longitude")
        var lon: Double = 0.0,

        @ColumnInfo(name = "text")
        var text: String = "",

        @ColumnInfo(name = "status")
        var status: PinStatus = PinStatus.Nothing,

        @ColumnInfo(name = "server_id")
        var serverId: Long = -1,

        @ColumnInfo(name = "last_edited")
        var lastEdited: Long = 0L,

        @ColumnInfo(name = "user_id")
        var userId: Long = 0L
){
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0L
}

enum class PinContentType(var type: String) {
        Image("image"),
        Video("video"),
        Quiz("quiz");

        override fun toString(): String {
                return type // working!
        }
}

/**
 * Abstract class for a generic pincontent entity.
 * Holds a uri to the filesystem, allowing pictures and/or videos to be referenced.
 * @param[pinId] The pin that this content belongs to
 * @param[uri] The uri that holds the location to the content in the filesystem
 * @param[contentIndex] Determines in which order the content should be displayed inside a pin
 * @param[contentType] Tells us which type of content this is, using the [PinContentType] enum
 */
@Entity(tableName = "pin_contents",
        foreignKeys = [ForeignKey(
                onDelete = ForeignKey.CASCADE,
                entity = Pin::class,
                parentColumns = ["id"],
                childColumns = ["pin_id"])])
data class PinContent(
        @ColumnInfo(name = "pin_id")
        var pinId: Long = 0L,

        @ColumnInfo(name = "uri")
        var uri: String? = null,

        @ColumnInfo(name = "content_index")
        var contentIndex: Int = 0,

        @ColumnInfo(name = "content_type")
        var contentType: String = "image"
){
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0L
}

/**
 * Represents a pin group entity in our database.
 * This entity is used in the relation [PinPerPinGroup].
 * @param[id] The id of the pinGroup.
 */
@Entity (tableName = "pin_groups")
data class PinGroup(
        @PrimaryKey
        var id: Long
)

/**
 * This entity is used to keep track of which pins we have recently accessed.
 * Once we have too many entries in this table, we should delete the oldest ones.
 * @param[pinId] The [Pin] that this cachedPin references
 * @param[lastAccessed] The timestamp at which this pin was last accessed, in seconds.
 */
@Entity (tableName = "cached_pins",
        foreignKeys = [ForeignKey(
                onDelete = ForeignKey.CASCADE,
                entity = Pin::class,
                parentColumns = ["id"],
                childColumns = ["pin_id"])])
data class CachedPin(
        @PrimaryKey
        @ColumnInfo(name = "pin_id")
        var pinId: Long,

        @ColumnInfo(name = "last_accessed")
        var lastAccessed : Long = 0L
)
