/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.entity

import androidx.room.*


/**
 * Specifies the relation between a [Pin] and its [contents][PinContent].
 * This relation is a one-to-many relation, where a pin can have many different contents.
 * A piece of content can belong to only one pin.
 */
data class PinWithContents(
        @Embedded val pin: Pin,
        @Relation(
                parentColumn = "id",
                entity = PinContent::class,
                entityColumn = "pin_id"
        )
        val contents: List<PinContent>
)

/**
 * Specifies the relation between a [Pin] and its [PinGroup].
 * This relation is a many-to-many relation, where a pin can belong to multiple PinGroup.
 * A PinGroup can have multiple pins.
  */
@Entity(tableName = "pins_per_pin_group", primaryKeys = ["pin_id", "pin_group_id"],
        foreignKeys = [ForeignKey(
                                onDelete = ForeignKey.CASCADE,
                                entity = Pin::class,
                                parentColumns = arrayOf("id"),
                                childColumns = arrayOf("pin_id")),
                       ForeignKey(
                                onDelete = ForeignKey.CASCADE,
                                entity = PinGroup::class,
                                parentColumns = ["id"],
                                childColumns = ["pin_group_id"])])
data class PinPerPinGroup(
        @ColumnInfo(name = "pin_id")
        val pinId: Long,
        @ColumnInfo(name = "pin_group_id")
        val pinGroupId: Long
)

/**
 * A separate data object to retrieve the relation from the [PinPerPinGroup] crossref table.
 * It includes a [PinGroup] with a list of it's [pins][Pin].
 */
data class PinGroupWithPins(
        @Embedded val pinGroup: PinGroup,
        @Relation(
                parentColumn = "id",
                entity = Pin::class,
                entityColumn = "id",
                associateBy = Junction(
                        value = PinPerPinGroup::class,
                        parentColumn = "pin_group_id",
                        entityColumn = "pin_id"
                )
        )
        val pins: List<Pin>
)

/**
 * A separate data object to retrieve the relation from the [PinPerPinGroup] crossref table.
 * It includes a [Pin] with a list of it's [PinGroups][PinGroup].
 */
data class PinWithPinGroups(
        @Embedded val pin: Pin,
        @Relation(
                parentColumn = "id",
                entity = PinGroup::class,
                entityColumn = "id",
                associateBy = Junction(
                        value = PinPerPinGroup::class,
                        parentColumn = "pin_id",
                        entityColumn = "pin_group_id"
                )
        )
        val pinGroups: List<PinGroup>
)

/**
 * Specifies the relation between a [UserGroup] and a [PinGroup].
 * This relation is a one-to-zero relation, where a UserGroup always has a PinGroup.
 * A PinGroup does not always have a UserGroup.
 */
data class UserGroupAndPinGroup(
        @Embedded val pinGroup: PinGroup,
        @Relation(
                parentColumn = "id",
                entity = UserGroup::class,
                entityColumn = "pin_group_id"
        )
        val userGroup: UserGroup
)

/**
 * Specifies the relation between a [UserGroup] and its [users][User].
 * This is a one-to-many relation, where a UserGroup has many users.
 * A user has only one UserGroup.
 */
data class UserGroupWithUsers(
        @Embedded val userGroup: UserGroup,
        @Relation(
                parentColumn = "id",
                entity = User::class,
                entityColumn = "user_group_id"
        )
        val users: List<User>
)

/**
 * Specifies the relation between a [CachedPin] and the [Pin] it keeps track of.
 * This is a one-to-zero relation, where a CachedPin always has a pin.
 * A pin is not always referenced by a CachedPin.
 */
data class CachedPinWithInfo(
        @Embedded val cachedPin: CachedPin,
        @Relation(
                parentColumn = "pin_id",
                entity = Pin::class,
                entityColumn = "id"
        )
        val pin: Pin
)

/**
 * Specifies the relation between a [User] and its [Pin].
 * This is a one-to-many relation, where a user can have many pins.
 * A pin can belong to only one user.
 */
data class UserWithPins(
        @Embedded val user: User,
        @Relation(
                parentColumn = "id",
                entity = Pin::class,
                entityColumn = "user_id"
        )
        val pins: List<Pin>
)

/**
 * Specifies the relation between a [Book] and its [contents][BookSection].
 * This relation is a nested one-to-many relation, where a book can have many different sections, which have many different contents.
 * A piece of content or section can belong to only one book.
 */
data class BookWithContents(
        @Embedded val book: Book,
        @Relation(
                parentColumn = "id",
                entity = BookSection::class,
                entityColumn = "book_id"
        )
        val sections: List<BookSectionWithContents>
)

/**
 * Specifies the relation between a [BookSection] and its [contents][BookContent].
 * This relation is a one-to-many relation, where a section can have many different contents.
 * A piece of content can belong to only one section.
 */
data class BookSectionWithContents(
        @Embedded val section: BookSection,
        @Relation(
                parentColumn = "id",
                entity = BookContent::class,
                entityColumn = "section_id"
        )
        val contents: List<BookContent>
)