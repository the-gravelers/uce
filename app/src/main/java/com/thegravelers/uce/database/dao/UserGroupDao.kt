/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.*
import com.thegravelers.uce.database.entity.UserGroup
import com.thegravelers.uce.database.entity.User
import com.thegravelers.uce.database.entity.PinGroup
import com.thegravelers.uce.database.entity.UserGroupAndPinGroup
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import kotlinx.coroutines.flow.Flow

/**
 * Defines methods for using the [UserGroup] class with Room.
 */
@Dao
interface UserGroupDao : BaseDao<UserGroup> {

    /**
     * Deletes a [UserGroup] from our user_groups table with an id that matches the given key.
     * @param key The id of the userGroup that we want deleted.
     */
    @Query("DELETE FROM user_groups WHERE id = :key")
    suspend fun delete(key: Long)

    /**
     * Selects and returns the row that matches the supplied UserGroupId, which is our key,
     * as live data.
     * @param key Id to match
     */
    @Query("SELECT * FROM user_groups WHERE id = :key")
    fun get(key: Long): Flow<UserGroup>

    /**
     * @return all [user groups][UserGroup]
     */
    @Query("SELECT * FROM user_groups")
    fun getAll(): Flow<List<UserGroup>>

    /**
     * Selects the [UserGroup] that matches the given key and returns it with all of it's users
     * @param key The [UserGroup.id] of the [UserGroup] that we want returned
     * @return An object with [UserGroup] bundled with a list of [users][User]
     */
    @Transaction
    @Query("SELECT * FROM user_groups WHERE id = :key")
    fun getUserGroupWithUsers(key: Long): UserGroupWithUsers

    /**
     * Selects all the usergroups and returns it with all of it's users
     * @return A Flow object with List<[UserGroup]> bundled with a list of [users][User]
     */
    @Transaction
    @Query("SELECT * FROM user_groups")
    fun getUserGroupsWithUsers(): Flow<List<UserGroupWithUsers>>

    /**
     * Selects the [UserGroup] that matches the given key and returns it combined with its [PinGroup].
     * @param key The [UserGroup.id] of the [UserGroup] that we want returned
     * @return A Flow object with [UserGroup] bundled with [PinGroup]
     */
    @Transaction
    @Query("SELECT * FROM user_groups WHERE id = :key")
    fun getUserGroupWithPinGroup(key: Long): Flow<UserGroupAndPinGroup>

    /**
     * Deletes all entries from the user_groups table.
     */
    @Query("DELETE FROM user_groups")
    suspend fun deleteAll()
}