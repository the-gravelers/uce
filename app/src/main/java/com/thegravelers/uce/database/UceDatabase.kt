package com.thegravelers.uce.database

/* This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.thegravelers.uce.database.dao.*
import com.thegravelers.uce.database.entity.*

/**
 * Our UCE database that contains all local Pin and User information.
 * Includes methods to access our DAO's and a global getter for our database.
 * WARNING: You should not get the database with this local getter. Use Hilt for dependencies.
 */
@Database(entities = [Pin::class, PinContent::class, CachedPin::class, PinGroup::class,
                      Book::class, User::class, UserGroup::class, PinPerPinGroup::class,
                      BookSection::class, BookContent::class],
          version = 15, exportSchema = false)
abstract class UceDatabase: RoomDatabase() {
    abstract fun pinDao(): PinDao
    abstract fun bookDao(): BookDao
    abstract fun pinContentDao(): PinContentDao
    abstract fun pinGroupDao(): PinGroupDao
    abstract fun userDao(): UserDao
    abstract fun userGroupDao(): UserGroupDao
    abstract fun cachedPinDao(): CachedPinDao
    abstract fun bookSectionDao(): BookSectionDao
    abstract fun bookContentDao(): BookContentDao

    companion object {
        /**
         * INSTANCE will keep a reference to any database returned via getInstance.
         *
         * This will help us avoid repeatedly initializing the database, which is expensive.
         *
         *  The value of a volatile variable will never be cached, and all writes and
         *  reads will be done to and from the main memory. It means that changes made by one
         *  thread to shared data are visible to other threads.
         */
        @Volatile
        private var INSTANCE: UceDatabase? = null

        /**
         * Helper function to get the database.
         *
         * If a database has already been retrieved, the previous database will be returned.
         * Otherwise, create a new database.
         *
         * This function is threadsafe, and callers should cache the result for multiple database
         * calls to avoid overhead.
         *
         * @param context The application context Singleton, used to get access to the filesystem.
         */
        fun getInstance(context: Context): UceDatabase {
            // Multiple threads can ask for the database at the same time, ensure we only initialize
            // it once by using synchronized. Only one thread may enter a synchronized block at a
            // time.
            synchronized(this) {
                // Copy the current value of INSTANCE to a local variable so Kotlin can smart cast.
                // Smart cast is only available to local variables.
                var instance = INSTANCE
                // If instance is `null` make a new database instance.
                if (instance == null) {
                    instance = Room.databaseBuilder(
                            context.applicationContext,
                            UceDatabase::class.java,
                            "uce_database"
                    )
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            .fallbackToDestructiveMigration()
                            .build()
                    // Assign INSTANCE to the newly created database.
                    INSTANCE = instance
                }
                // Return instance; smart cast to be non-null.
                return instance
            }
        }
    }
}
