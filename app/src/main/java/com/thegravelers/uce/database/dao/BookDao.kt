/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.room.*
import com.thegravelers.uce.database.entity.Book
import com.thegravelers.uce.database.entity.BookContent
import com.thegravelers.uce.database.entity.BookSection
import com.thegravelers.uce.database.entity.BookWithContents
import kotlinx.coroutines.flow.Flow

/**
 * Defines methods for using the [Book] class with Room.
 */
@Dao
interface BookDao : BaseDao<Book> {

    /**
     * Delete a specific [Book] from the books table based on it's key
     */
    @Query("DELETE FROM books WHERE id = :key")
    suspend fun delete(key: Long)

    /**
     * Selects and returns the row that matches the supplied [Book.id], which is our key, as flow data.
     * @param key The [Book.id] of the target [Book].
     */
    @Query("SELECT * from books WHERE id = :key")
    fun getFlow(key: Long): Flow<Book>

    @Query("SELECT * from books WHERE id = :key")
    fun get(key: Long): Book?

    /**
     * Selects and returns all rows in the table, sorted by time uploaded in descending order,
     * as live data.
     */
    @Query("SELECT * FROM books")
    fun getAll(): Flow<List<Book>>

    /**
     * Returns the info of a single [Book] in our [books][Book] table combined with its [BookSection] and [BookContent].
     * @param[key] The [id][Book.id] of the [Book].
     */
    @Transaction
    @Query("SELECT * FROM books WHERE id = :key")
    suspend fun getBookWithContents(key: Long): BookWithContents

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSection(section: BookSection)

    @Query("SELECT * FROM book_sections WHERE id = :sectionId")
    suspend fun getSection(sectionId: Long): BookSection?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertContent(content: BookContent)

    @Query("SELECT * FROM book_contents WHERE id = :contentId")
    suspend fun getContent(contentId: Long): BookContent?
}