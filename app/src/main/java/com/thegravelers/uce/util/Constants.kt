/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.util

import com.thegravelers.uce.BuildConfig

object Version {
    /**
     * Anything equal or below [LEGACY] is considered legacy
     */
    const val LEGACY = 28
}

object MapConstants {
    const val MAP_MAX_ZOOM = 16.0
    const val MAP_MIN_ZOOM = 7.0
    const val MAP_MAX_LOD_AT_ZOOM = 11
    const val MAP_ROTATE_ENABLED = false
    const val DegreeEqualsRadians = 0.017453292519943
    const val EarthsRadius = 6378137
    const val RadiansPerDegree = Math.PI / 180
    const val PIN_ICON = "ic_pin_on_map"
    const val GROUND_TYPE_ICON = "profile_background_card"
}

object MediaTypes {
    const val PLAINTEXT = "text/plain"
    const val JPEG = "image/jpeg"
    const val PNG = "image/png"
}

object ImageValues {
    const val DISPLAY_NAME = "uce_image"
}

object Animation {
    const val FAST_MILLIS = 50L
    const val SLOW_MILLIS = 100L
}

object WorkerConstants {
    const val REFRESH_TOKEN_NAME = BuildConfig.APPLICATION_ID + ":refresh-token-work"
    const val REFRESH_TOKEN_TAG = "REFRESH-TOKEN-WORK"
    const val REFRESH_TOKEN_INTERVAL = 60L
    const val KEY_REFRESH_TOKEN_FIRST = "KEY-REFRESH-TOKEN-FIRST"

    const val UPLOAD_PINS_NAME = BuildConfig.APPLICATION_ID + ":upload-pins-work"
    const val UPLOAD_PINS_TAG = "UPLOAD-PINS-WORK"
    const val KEY_UPLOAD_PINS_IDS = "KEY-UPLOAD-PINS-IDS"

    const val SYNC_PINS_NAME = BuildConfig.APPLICATION_ID + ":sync-pins-work"
    const val SYNC_PINS_TAG = "SYNC-PINS-WORK"
}

object Caching {
    const val MAX_NR_OF_PINS = 3
}