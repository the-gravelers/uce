/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.util.extensions

import com.mapbox.mapboxsdk.geometry.LatLng
import java.lang.Math.*
import kotlin.math.ln
import kotlin.math.pow
import kotlin.math.roundToInt

/**
 * Converts a LatLng object to UTM coordinates in a string.
 * This code was taken from the app from previous' year.
 */
fun LatLng.toUtm(): String {
    var easting: Double
    var northing: Double

    val lat = this.latitude
    val lon = this.longitude

    val zone = floor(lon / 6 + 31).toInt()
    val letter = latToUTMLetter(lat)

    val deg = PI / 180
    val latdeg = lat * deg
    val londeg = lon * deg
    val prezone = 6 * zone - 183
    val coslatdeg = cos(latdeg)
    val sinlondeg = sin(londeg - (prezone) * deg)

    easting = 0.5 * ln((1 + coslatdeg * sinlondeg) /
                    (1 - coslatdeg * sinlondeg))* 
            0.9996 * 6399593.62 / (1 + 0.0820944379.pow(2.0) * coslatdeg.pow(2.0)).pow(0.5) * 
            (1 + 0.0820944379.pow(2.0) / 2 * 
                    (0.5 * ln((1 + coslatdeg * sinlondeg) / 
                            (1 - coslatdeg * sinlondeg)))
                            .pow(2.0) * coslatdeg.pow(2.0) / 3) + 500000

    easting = (easting * 100).roundToInt() * 0.01

    northing = (atan(tan(latdeg) / cos(londeg - (prezone) * deg)) - latdeg) * 
            0.9996 * 6399593.625 / sqrt(1 + 0.006739496742 * coslatdeg.pow(2.0)) * 
            (1 + 0.006739496742 / 2 * (0.5 * ln((1 + coslatdeg * 
                    sinlondeg) / (1 - coslatdeg * 
                    sinlondeg))).pow(2.0) * 
                    coslatdeg.pow(2.0)) + 0.9996 * 6399593.625 * 
            (latdeg - 0.005054622556 * (latdeg + sin(2 * latdeg) / 2) + 4.258201531e-05 * 
                    (3 * (latdeg + sin(2 * latdeg) / 2) + sin(2 * latdeg) * 
                            coslatdeg.pow(2.0)) / 4 - 1.674057895e-07 * 
                    (5 * (3 * (latdeg + sin(2 * latdeg) / 2) + sin(2 * latdeg) * 
                            coslatdeg.pow(2.0)) / 4 + sin(2 * latdeg) * coslatdeg
                            .pow(2.0) * coslatdeg.pow(2.0)) / 3)
    
    if (letter < 'M') northing += 10000000
    northing = (northing * 100).roundToInt() * 0.01

    return "$zone" +
            "$letter " +
            "${easting.toFloat().toInt()}" +
            "E " +
            "${northing.toFloat().toInt()}" +
            if (letter < 'M') 'S' else 'N'
}

/**
 * Calculates a utm zone letter based on the latitude of a lat-long coordinate.
 * @param[lat] the latitude for which a utm letter is to be calculated.
 * @return a utm letter.
 */
fun latToUTMLetter(lat: Double): Char {
    val letters = listOf('C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M',
            'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W')
    var counter = -72
    for (l in letters) {
        if (lat < counter)
            return l
        counter += 8
    }
    return 'X'
}