/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.util

import android.annotation.SuppressLint
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.DiffUtil
import kotlinx.coroutines.CoroutineScope
import timber.log.Timber
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Take the Long seconds returned by the system and stored in Room,
 * and convert it to a nicely formatted string for display.
 *
 * EEEE - Display the long letter version of the weekday
 * MMM - Display the letter abbreviation of the month
 * dd-yyyy - day in month and full year numerically
 * HH:mm - Hours and minutes in 24hr format
 */
@SuppressLint("SimpleDateFormat")
fun convertLongToDateString(time: Long): String {
    return SimpleDateFormat("dd/MMM/yyyy").format(time * 1000).toString()
}

@SuppressLint("SimpleDateFormat")
fun convertLongToTimeString(time: Long) : String {
    return SimpleDateFormat("HH:mm:ss").format(time* 1000).toString()
}
// SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(time_uploaded)

@SuppressLint("SimpleDateFormat")
fun getCurrentTime(): String {
    return SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS").format(System.currentTimeMillis()).toString()
}

fun getCurrentTimeSeconds(): Long{
    return System.currentTimeMillis() / 1000
}

fun convertLongToDate(long: Long): Date {
    return Date(long)
}
//TODO make it really convert to UTM
fun latlonToUTM(lat: Double, lon: Double): String {
    return (lat * lon).toString()
}

@Suppress("FunctionName")
fun UriCallback() = object : DiffUtil.ItemCallback<Uri>() {
    override fun areItemsTheSame(oldItem: Uri, newItem: Uri) =
            oldItem.path == newItem.path

    override fun areContentsTheSame(oldItem: Uri, newItem: Uri) =
            oldItem == newItem
}

/**
 * Rounds a double up to the nth decimal.
 * @param d The double that needs to be rounded
 * @param n The double will be rounded to the nth decimal
 * @return The rounded double
 */
fun roundDoubleTo(d: Double, n: Int): Double{
    val df = DecimalFormat("#." + "#".repeat(n)).apply{
        roundingMode = RoundingMode.HALF_EVEN
    }
    return df.format(d).replace(',','.').toDouble()
}
