/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository.sessionmanager

import android.content.SharedPreferences
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@JsonClass(generateAdapter = true)
data class AuthInfo(
        @Json(name = "session_id")
        val sessionId: Long = 0L,
        @Json(name = "auth_token")
        val authToken: String = "",
        @Json(name = "refresh_token")
        val refreshToken: String = "",
        @Json(name = "expiry_date")
        val expiryDate: Long = 0L,
        val email: String = "",
        val password: String = "",
)


@JsonClass(generateAdapter = true)
data class UserInfo(
        @Json(name = "user_id")
        val userId: Long = 0L,

        val university: String = "",
        val course: String = "",

        @Json(name = "course_id")
        val classId: Long? = null,

        @Json(name = "pin_group_id")
        val pinGroupId: Long = 0L,
)

/**
 * This class managed all the SharedPreference interactions
 */
@Singleton
class SessionManager @Inject constructor(private val prefs: SharedPreferences, private val moshi: Moshi) {
    companion object {
        private const val AUTH_INFO = "auth_info"
        private const val USER_INFO = "user_info"
        private const val GHOST_TOKEN = "ghost_token"
        private const val DARK_TOKEN = "dark_token"
    }

    /**
     * Saves a bundle of authentication info to the [SharedPreferences]
     * @param[info] The bundle of [AuthInfo], containing the auth_token, refresh_token and expiry_date of our session.
     */
    fun saveAuthInfo(info: AuthInfo) {
        val adapter = moshi.adapter(AuthInfo::class.java)
        val json = adapter.toJson(info)

        val editor = prefs.edit()
        editor.putString(AUTH_INFO, json)
        editor.apply()
    }

    fun clearAuthInfo() {
        val editor = prefs.edit()
        editor.remove(AUTH_INFO)
        editor.apply()
    }

    /**
     * Gets the bundle of authentication info from [SharedPreferences]
     */
    fun fetchAuthInfo(): AuthInfo? {
        val info = prefs.getString(AUTH_INFO, null)
        if (info != null) {
            val adapter = moshi.adapter(AuthInfo::class.java)
            return adapter.fromJson(info)
        }
        return null
    }

    /**
     * This function saves a token to a specific key to SharedPreferences
     */
    fun saveDarkMode(token: String) {
        val editor = prefs.edit()
        editor.putString(DARK_TOKEN, token)
        editor.apply()
    }

    /**
     * This function gets the token from a specific key from SharedPreferences
     */
    fun fetchDarkMode(): String? {
        return prefs.getString(DARK_TOKEN, null)
    }

    /**
     * This function saves a token to a specific key to SharedPreferences
     */
    fun saveGhostMode(token: String) {
        val editor = prefs.edit()
        editor.putString(GHOST_TOKEN, token)
        editor.apply()
    }

    /**
     * This function gets the token from a specific key from SharedPreferences
     */
    fun fetchGhostMode(): String? {
        return prefs.getString(GHOST_TOKEN, null)
    }

    /**
     * Saves a [UserInfo] object that holds information specific for the logged in user.
     */
    fun saveUserInfo(info: UserInfo) {
        val adapter = moshi.adapter(UserInfo::class.java)
        val json = adapter.toJson(info)
        Timber.i(json)
        val editor = prefs.edit()
        editor.putString(USER_INFO, json)
        editor.apply()
    }

    fun clearUserInfo() {
        val editor = prefs.edit()
        editor.remove(USER_INFO)
        editor.apply()
    }

    /**
     * Retrieves the [UserInfo] object from the [SharedPreferences].
     */
    fun fetchUserInfo(): UserInfo? {
        val info = prefs.getString(USER_INFO, null)
        if (info != null) {
            val adapter = moshi.adapter(UserInfo::class.java)
            return adapter.fromJson(info)
        }
        return null
    }

    /**
     * Returns the [UserInfo.userId] from the user that is currently logged into the application.
     * @throws[Exception] If no userId has been saved, for example when no one is logged in.
     */
    fun fetchUserId(): Long {
        fetchUserInfo()?.let { return it.userId }
        throw(Exception("The userId has not been set yet"))
    }

    /**
     * Returns the [UserInfo.university] of the university from the user that is currently logged into the application.
     */
    fun fetchUniversity(): String? {
        fetchUserInfo()?.let { return it.university }
        return null
    }

    /**
     * Returns the [UserInfo.course] of the course from the user that is currently logged into the application.
     */
    fun fetchCourse(): String? {
        fetchUserInfo()?.let { return it.course }
        return null
    }

    fun fetchClassId(): Long? {
        fetchUserInfo()?.let {
            return it.classId
        }
        return null
    }

    /**
     * Returns the [UserInfo.pinGroupId] from the user that is currently logged into the application.
     */
    fun fetchPinGroupId(): Long? {
        fetchUserInfo()?.let { return it.pinGroupId }
        return null
    }
}