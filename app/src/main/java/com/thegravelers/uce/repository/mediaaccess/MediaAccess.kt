/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository.mediaaccess

import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import com.thegravelers.uce.domain.Image

interface MediaAccess {
    suspend fun deleteImage(uri: Uri): Boolean

    suspend fun storeImage(fileName: String, folderName: String, bitmap: Bitmap, format: Bitmap.CompressFormat): Uri?

    suspend fun exists(uri: Uri): Boolean
}