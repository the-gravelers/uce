/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository.mediaaccess

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.net.toUri
import com.thegravelers.uce.domain.Image
import com.thegravelers.uce.util.MediaTypes
import com.thegravelers.uce.util.Version
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MediaAccessImpl
@Inject
constructor(
        private val contentResolver: ContentResolver,
        @ApplicationContext private val context: Context
) : MediaAccess {
    override suspend fun deleteImage(uri: Uri): Boolean {
        if(uri == Uri.EMPTY) return false
        if(!exists(uri)) return false

        if (Build.VERSION.SDK_INT > Version.LEGACY) {
            return withContext(Dispatchers.IO) {
                val rowsDeleted = contentResolver.delete(uri, null, null)
                return@withContext rowsDeleted == 1
            }
        } else {
            return deleteImageLegacy(uri)
        }
    }

    private suspend fun deleteImageLegacy(uri: Uri): Boolean {
        return withContext(Dispatchers.IO) {
            uri.path?.let {
                val file = File(it)
                if (file.exists()) {
                    file.delete()
                    return@withContext true
                }
            }
            return@withContext false
        }
    }

    override suspend fun storeImage(fileName: String, folderName: String, bitmap: Bitmap, format: Bitmap.CompressFormat): Uri? {
        if (Build.VERSION.SDK_INT > Version.LEGACY) {
            return withContext(Dispatchers.IO) {
                val collection = MediaStore.Images.Media.EXTERNAL_CONTENT_URI

                val contentValues = Image.getContentValues(
                        displayName = fileName,
                        relativePath = Environment.DIRECTORY_PICTURES + File.separator + "Uce" + File.separator + folderName)
                contentValues.put(MediaStore.Images.Media.IS_PENDING, 1)

                val extension = when (format) {
                    Bitmap.CompressFormat.PNG -> MediaTypes.PNG
                    else -> MediaTypes.JPEG
                }
                contentValues.put(MediaStore.Images.Media.MIME_TYPE, extension)
                val uri = contentResolver.insert(collection, contentValues)
                if (uri != null) {
                    try {
                        // Since we are in a IO thread we can call a blocking call like openOutputStream so ignore the warning
                        @Suppress("BlockingMethodInNonBlockingContext")
                        contentResolver.openOutputStream(uri).use {
                            bitmap.compress(format, 100, it)
                        }
                        // Release pending status of the file
                        contentValues.clear()
                        contentValues.put(MediaStore.Images.Media.IS_PENDING, 0)
                        contentResolver.update(uri, contentValues, null, null)
                    } catch (e: Exception) {
                        Timber.i("Could store image: %s", e.stackTrace)
                    }
                }
                return@withContext uri
            }
        } else {
            return storeImageLegacy(fileName, folderName, bitmap, format)
        }
    }

    private suspend fun storeImageLegacy(fileName: String, folder: String, bitmap: Bitmap, format: Bitmap.CompressFormat): Uri {
        return withContext(Dispatchers.IO) {
            val filePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            val directory = File(filePath, folder)

            if (!directory.exists()) {
                directory.mkdirs()
            }

            val extension = when (format) {
                Bitmap.CompressFormat.PNG -> "png"
                else -> "jpg"
            }

            val file = File(directory, "${fileName}.$extension")

            try {
                // Since we are in a IO thread we can call a blocking call like FileOutputStream so ignore the warning
                @Suppress("BlockingMethodInNonBlockingContext")
                FileOutputStream(file).use {
                    bitmap.compress(format, 100, it)
                }
            } catch (e: Exception) {
                Timber.i("Could not store image in legacy mode: %s", e.stackTrace)
            }
            return@withContext file.toUri()
        }
    }

    /**
     * Checks if a files is still stored on the system. If this returns true, but the file can't be
     * retrieved, than the file wasn't properly deleted.
     */
    override suspend fun exists(uri: Uri): Boolean {
        if (Build.VERSION.SDK_INT > Version.LEGACY) {
            return withContext(Dispatchers.IO) {
                var exists = false
                contentResolver.query(uri, Image.getProjection(),
                        null, null, null)?.use { cursor ->
                    exists = cursor.moveToFirst()
                }
                return@withContext exists
            }
        } else {
            return existsLegacy(uri)
        }
    }

    private suspend fun existsLegacy(uri: Uri): Boolean {
        return withContext(Dispatchers.IO) {
            uri.path?.let {
                val file = File(it)
                return@withContext file.exists()
            }
            return@withContext false
        }
    }
}