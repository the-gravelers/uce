/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository

import androidx.work.*
import com.thegravelers.uce.database.dao.PinGroupDao
import com.thegravelers.uce.database.dao.UserDao
import com.thegravelers.uce.database.dao.UserGroupDao
import com.thegravelers.uce.database.entity.User
import com.thegravelers.uce.database.entity.UserGroup
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.network.uceserver.AuthRequest
import com.thegravelers.uce.network.uceserver.UceServerApiService
import com.thegravelers.uce.network.worker.RefreshTokenWorker
import com.thegravelers.uce.repository.sessionmanager.AuthInfo
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.repository.sessionmanager.UserInfo
import com.thegravelers.uce.util.WorkerConstants
import com.thegravelers.uce.util.getCurrentTimeSeconds
import kotlinx.coroutines.flow.Flow
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * The [UserRepositoryImpl] handles network calls that are relevant to a user account.
 */

@Singleton
class UserRepositoryImpl
@Inject
constructor(
        private val uceServerApiService: UceServerApiService,
        private val sessionManager: SessionManager,
        private val userDao: UserDao,
        private val userGroupDao: UserGroupDao,
        private val pinGroupDao: PinGroupDao,
        private val workManager: WorkManager
) : UserRepository {

    override suspend fun login(username: String, password: String): Boolean {
        val response = uceServerApiService.authenticate(
                AuthRequest(
                        email = username,
                        password = password
                ))

        if (response.isSuccessful) {
            response.body()?.let {
                sessionManager.saveAuthInfo(AuthInfo(
                        it.sessionId,
                        it.accessToken,
                        it.refreshToken,
                        it.expiresIn + getCurrentTimeSeconds(),
                        username,
                        password
                ))
            }
            //TODO: Reset db? Maybe only user stuff

            retrieveUserInfo()
            startRefreshTokenWorker(true)
            return true
        }
        return false
    }

    override suspend fun logout() {
        if (uceServerApiService.leaveUserGroup().isSuccessful) {
            sessionManager.clearAuthInfo()
            sessionManager.clearUserInfo()
            stopRefreshTokenWorker()
        }
    }

    override suspend fun getTeamIdFromUser(): Long? {
        val userId = sessionManager.fetchUserId()
        refreshTeams()
        val user = userDao.get(userId)
        user?.let {
            return user.userGroupId
        }
        return null
    }

    override fun getAuthInfo(): AuthInfo? {
        return sessionManager.fetchAuthInfo()
    }

    override suspend fun getTeam(teamId: Long): UserGroupWithUsers {
        return userGroupDao.getUserGroupWithUsers(teamId)
    }

    override fun getTeams(): Flow<List<UserGroupWithUsers>> {
        return userGroupDao.getUserGroupsWithUsers()
    }

    override suspend fun refreshTeams() {
        sessionManager.fetchClassId()?.let { classId ->
            val response = uceServerApiService.getUserGroups(classId)
            if (response.isSuccessful)
                response.body()?.let { userGroupList ->
                    userDao.deleteAll()
                    userGroupDao.deleteAll()
                    for (userGroup in userGroupList) {
                        val userGroupId = userGroupDao.insert(UserGroup(
                                userGroup.userGroupId,
                                userGroup.name,
                                0L))

                        for (member in userGroup.members) {
                            userDao.insert(User(member.userId, "Temp name", userGroupId))
                        }
                    }
                }
        }
    }

    override suspend fun joinTeam(teamId: Long): Boolean {
        return if (uceServerApiService.joinUserGroup(teamId).isSuccessful) {
            refreshTeams()
            true
        } else {
            false
        }
    }

    override suspend fun leaveTeam() {
        uceServerApiService.leaveUserGroup()
        refreshTeams()
    }

    /**
     * Creates a [UserInfo] by querying the server api and bundling certain info together.
     * Saves it in our [SessionManager].
     */
    private suspend fun retrieveUserInfo() {
        val response = uceServerApiService.getUserData()

        if (response.isSuccessful) {
            response.body()?.let {
                val userInfo = UserInfo(
                        it.id,
                        "Utrecht University",
                        "",
                        it.classId,
                        0L
                )
                //TODO: Add university and course names
                sessionManager.saveUserInfo(userInfo)
            }
        }
    }

    override fun startRefreshTokenWorker(skip: Boolean) {
        val workConstraints = Constraints
                .Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

        // We set our initial input data to true so that it skips the first check based on our parameter.
        // This way our first refresh is in [WorkerConstants.REFRESH_TOKEN_INTERVAL] minutes instead of directly after this function.
        val workData = Data.Builder()
                .putBoolean(WorkerConstants.KEY_REFRESH_TOKEN_FIRST, skip)
                .build()

        val periodicTokenRefreshWork = PeriodicWorkRequestBuilder<RefreshTokenWorker>(
                WorkerConstants.REFRESH_TOKEN_INTERVAL, TimeUnit.MINUTES)
                .setConstraints(workConstraints)
                .setInputData(workData)
                .addTag(WorkerConstants.REFRESH_TOKEN_TAG)
                .build()

        workManager.enqueueUniquePeriodicWork(
                WorkerConstants.REFRESH_TOKEN_NAME,
                ExistingPeriodicWorkPolicy.REPLACE,
                periodicTokenRefreshWork
        )
    }

    /**
     * Stops our active [RefreshTokenWorker].
     * Should be called when we logout.
     */
    private fun stopRefreshTokenWorker() {
        workManager.cancelUniqueWork(WorkerConstants.REFRESH_TOKEN_NAME)
    }
}
