/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository

import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.network.worker.RefreshTokenWorker
import com.thegravelers.uce.repository.sessionmanager.AuthInfo
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    suspend fun login(username: String, password: String): Boolean
    suspend fun logout()

    /**
     * Returns a specific team
     */
    suspend fun getTeam(teamId: Long): UserGroupWithUsers

    /**
     * Returns all usergroups
     */
    fun getTeams(): Flow<List<UserGroupWithUsers>>

    /**
     * Checks if the api call is successful.
     * Inserts the user in the room database, calls refreshTeam() and return true if the join is successful
     */
    suspend fun joinTeam(teamId: Long): Boolean

    /**
     * Gets the userId from the sessionManager and the groupId from the userDao
     * Server call to leave the group and refreshes team
     */
    suspend fun leaveTeam()

    /**
     * Refreshes our current usergroups with new ones from the server.
     * Deletes all usergroups and users in our room db before inserting the new ones.
     */
    suspend fun refreshTeams()

    /**
     * Starts a [RefreshTokenWorker] who will refresh our AUTH_TOKEN on a periodic basis.
     * Should be called when we login.
     * @param[skip] True if we want to skip the first cycle. False if we want a refresh immediately.
     */
    fun startRefreshTokenWorker(skip: Boolean)

    /**
     * Returns the userGroup from a specific user,
     * if the user exists,
     * if the user is in a usergroup.
     * @return The id of the userGroup
     */
    suspend fun getTeamIdFromUser(): Long?

    /**
     * Returns the authInfo if any user is logged in
     */
    fun getAuthInfo(): AuthInfo?
}