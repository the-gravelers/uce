/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository

import android.graphics.Bitmap
import android.net.Uri
import com.thegravelers.uce.database.entity.*
import kotlinx.coroutines.flow.Flow

enum class ContentType{
    PIN, BOOK
}

/**
 * Represents a single item in a content feed list.
 * Examples of content feed lists are PinFeedFragment, LibraryFeedFragment, SearchFeedFragment, MyPinsFeedFragment, SavedPinsFeedFragment.
 * This object is used by ContentFeedFragment to turn it into an entry in the RecyclerView.
 * @param[id] The unique identifier of this item.
 * @param[type] The [ContentType] of this content item.
 * @param[title] Title that the card view should have in the recyclerview.
 * @param[date] Timestamp of this content entry.
 * @param[previewText] The first paragraph of text that this content has, if it has any.
 * @param[location] The location of the [ContentType.PIN] in (for now) lat long coordinates. This is empty for [ContentType.BOOK].
 * @param[drawable] The resource id of the icon this content entry should have (example: R.drawable.avatar).
 */
data class ContentFeedItem(
        val id : Long,
        val type : ContentType,
        val title : String,
        val drawable : Int,
        val date : String = "",
        val previewText : String = "There is no preview available.",
        val location : String = ""
)

interface ContentRepository{
    /**
     * Inserts a freshly-created pin into our [Pin] and [PinContent] tables.
     * If we have access to the server, start a background service to sync all of our pins with the server.
     * @param[pin] The newly created [Pin] from the PinEditorFragment. Assumes that the [Pin.userId] has not been set.
     * @param[contents] The [Uri] of each content converted to string, in order of displaying.
     */
    suspend fun insertNewPin(pin: Pin, contents: List<PinContent>?)

    /**
     * Retrieves all [Pin] objects in our [pins][Pin] table.
     * Calls [refreshPins] if we have a connection to the server.
     * @return A list of [Pins][Pin] converted to [ContentFeedItems][ContentFeedItem] inside of a [Flow] object.
     */
    suspend fun getAllPinsFeed(): Flow<List<ContentFeedItem>>

    /**
     * Retrieves all [Pin] objects in our [pins][Pin] table.
     * Calls [refreshPins] if we have a connection to the server.
     * @return A list of [Pins][Pin] inside of a [Flow] object.
     */
    fun getAllPins(): Flow<List<Pin>>

    /**
     * Retrieves all [Pin] objects in our [pins][Pin] table where we are the creator.
     * @return A list of [Pins][Pin] converted to [ContentFeedItems][ContentFeedItem] inside of a [Flow] object.
     */
    suspend fun getMyPinsFeed(): Flow<List<ContentFeedItem>>

    /**
     * Retrieves all [Pin] objects in our [pins][Pin] table which are saved
     * @return A list of [Pins][Pin] converted to [ContentFeedItems][ContentFeedItem] inside of a [Flow] object.
     */
    suspend fun getSavedPinsFeed(): Flow<List<ContentFeedItem>>

    /**
     * Retrieves all [Book] objects in our [books][Book] table.
     * Calls [refreshBooks] if we have a connection to the server.
     * @return A list of [Books][Book] converted to [ContentFeedItems][ContentFeedItem] inside of a [Flow] object.
     */
    suspend fun getAllBooksFeed(): Flow<List<ContentFeedItem>>

    /**
     * Retrieves all the [BookContent] entries in the book_contents table that correspond to the [Book.id] through [BookSection]
     * @param[id] The [Book.id] from the [Book] that you want the content from.
     */
    suspend fun getBookWithContent(id: Long): BookWithContents
    /**
     * Deletes a pin which the user has created.
     * The actual pin will not be deleted until the server knows that it needs to be deleted.
     * It will instantly delete all pinContent (images) from the pin.
     */
    suspend fun deleteUserPin(id: Long)

    /**
     * Syncs the server pins with the local pins.
     * Checks for deleted pins on the server.
     * Checks for newer or new pins on the server.
     * Deletes locally deleted pins on the server (and then permanently deletes them locally).
     */
    suspend fun refreshPins()

    /**
     * Starts a background service which tries to upload our [pins][Pin] to the server.
     */
    fun uploadPins(vararg pinIds: Long)

    /**
     * Returns the pin object which matches the pinId from the pin database.
     */
     suspend fun getPin(pinId: Long): Pin

     suspend fun uploadAllPins()


    /**
     * Returns all pinContent for the pin which matches the pinId.
     * If necessary, it will download the pinContent from the server.
     */
    suspend fun getPinContent(pinId: Long): List<PinContent>

    /**
     * Caches a pin and deletes the oldest pin if there are too many cached pins.
     */
    suspend fun cachePin(pinId: Long, currentTime: Long)

    /**
     * Saves a pin and deletes the CachedPin if there is any.
     */
    suspend fun savePin(pinId: Long)

    /**
     * Reverts the effects of [savePin]. Also caches the pin again.
     */
    suspend fun unsavePin(pinId: Long)

    /**
     * Stores an image on the device
     */
    suspend fun storeImage(fileName: String, folderName: String, bitmap: Bitmap, format: Bitmap.CompressFormat)

    /**
     * Deletes an image at the end of the uri on the device (if it exists)
     */
    suspend fun deleteImage(uri: Uri)

    /**
     * Updates a pin (including pincontent) to the given content.
     * @param[pin] The new pin information. The pin.id is used to identify which pin needs updating.
     * @param[newImages] The list of new pincontent for this pin.
     */
    suspend fun updatePin(pin: Pin, newImages: List<Uri>?)

    /**
     * Downloads all books that we do not have yet from the server.
     * Also downloads all content from each section (only if it is an image or text)
     */
    suspend fun downloadAllBooks()
}
