/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import androidx.core.net.toUri
import androidx.work.*
import com.mapbox.mapboxsdk.geometry.LatLng
import com.thegravelers.uce.R
import com.thegravelers.uce.database.dao.*
import com.thegravelers.uce.database.entity.*
import com.thegravelers.uce.network.uceserver.*
import com.thegravelers.uce.network.worker.UploadPinsWorker
import com.thegravelers.uce.repository.mediaaccess.MediaAccess
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.util.*
import com.thegravelers.uce.util.extensions.toUtm
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject
import kotlin.io.path.ExperimentalPathApi

class ContentRepositoryImpl @Inject constructor(
        private val pinDao: PinDao,
        private val pinContentDao: PinContentDao,
        private val cachedPinDao: CachedPinDao,
        private val bookDao: BookDao,
        private val pinGroupDao: PinGroupDao,

        private val sessionManager: SessionManager,
        private val uceServerApi: UceServerApiService,
        private val mediaAccess: MediaAccess,
        private val workManager: WorkManager
) : ContentRepository {


    override suspend fun insertNewPin(pin: Pin, contents: List<PinContent>?) {
        pin.apply {
            userId = sessionManager.fetchUserId()
            status = PinStatus.Saved
            lastEdited = getCurrentTimeSeconds()
        }
        val id = pinDao.insert(pin)
        for (content in contents.orEmpty()) {
            content.pinId = id
            pinContentDao.insert(content)
        }
        uploadAllPins()
    }

    override suspend fun updatePin(pin: Pin, newImages: List<Uri>?) {
        if (pin.userId != sessionManager.fetchUserId()) {
            return
        }

        pinDao.update(pin)
        newImages ?: return
        val oldContent = pinContentDao.getAll(pin.id)
        // Delete physical content that is not in the new list
        for (oc in oldContent) {
            if (!(newImages.contains(oc.uri?.toUri()))) {
                deletePinContent(oc)
            }
        }
        // Insert new images
        newImages.forEachIndexed { index, uri ->
            val pinContent = PinContent(pin.id, uri.toString(), index)
            pinContentDao.insert(pinContent)
        }
        uploadPins(pin.id)
    }

    override suspend fun getAllPinsFeed(): Flow<List<ContentFeedItem>> {
        return pinDao.getAllFlow()
                .map { pins ->
                    pins
                            .filter { pin ->
                                pin.status != PinStatus.Deleted
                            }
                            .map { pin ->
                                pinToContentItem(pin)
                            }
                }
    }

    override fun getAllPins(): Flow<List<Pin>> {
        return pinDao.getAllFlow()
    }

    override suspend fun getMyPinsFeed(): Flow<List<ContentFeedItem>> {
        return pinDao.getAllFromUser(sessionManager.fetchUserId())
                .map { pins ->
                    pins
                            .filter { pin ->
                                pin.status != PinStatus.Deleted
                            }
                            .map { pin ->
                                pinToContentItem(pin)
                            }
                }
    }

    override suspend fun getSavedPinsFeed(): Flow<List<ContentFeedItem>> {
        return pinDao.getAllSaved()
                .map { pins ->
                    pins
                            .filter { pin ->
                                pin.status != PinStatus.Deleted
                            }
                            .map { pin ->
                                pinToContentItem(pin)
                            }
                }
    }

    override suspend fun getAllBooksFeed(): Flow<List<ContentFeedItem>> {
        return bookDao.getAll()
                .map { books ->
                    books.map { book ->
                        bookToContentItem(book)
                    }
                }
    }

    override suspend fun getBookWithContent(id: Long): BookWithContents {
        return bookDao.getBookWithContents(id)
    }

    override suspend fun deleteUserPin(id: Long) {
        val pin = pinDao.get(id)
        pin.apply {
            status = PinStatus.Deleted
        }
        pinDao.update(pin)
        deleteAllPinContent(id)
    }

    override suspend fun refreshPins() {
        val serverPins = mutableListOf<GetPinsResponse>()

        fun handleResponse(response: Response<List<GetPinsResponse>>) {
            if (response.isSuccessful) {
                response.body()?.let {
                    serverPins.addAll(it)
                }
            }
        }

        // We grab all pins that we have access to (class pins, group pins and user pins)
        sessionManager.fetchClassId()?.let { handleResponse(uceServerApi.getPins(classId = it)) }
        handleResponse(uceServerApi.getPins(userId = sessionManager.fetchUserId()))

        val localPins = pinDao.getAll()
        val serverIds = serverPins.map { serverPin -> serverPin.id }
        localPins.forEach { localPin ->
            // Foreach pin that we have local, check if it is deleted on the server
            if (localPin.serverId >= 0 && !serverIds.contains(localPin.serverId)) {
                // The pin has been deleted on the server, let's delete it here as well
                deleteAllPinContent(localPin.id)
                pinDao.delete(localPin)
            }
        }

        serverPins.forEach { serverPin ->
            // Foreach pin on the server, check if we have an older local version.
            // Also check if we don't have this pin yet.
            val localPin = pinDao.getWithServerId(serverPin.id)

            if (localPin != null) {
                if (localPin.lastEdited < serverPin.lastEdited) {
                    // Local is an older version, let's update it with the server pin
                    localPin.apply {
                        title = serverPin.title
                        text = serverPin.text
                        lastEdited = serverPin.lastEdited
                    }
                    pinDao.update(localPin)
                }
            } else {
                // We don't have this pin yet, let's add it to our local database
                pinDao.insert(Pin(
                        serverPin.title,
                        serverPin.latitude,
                        serverPin.longitude,
                        serverPin.text,
                        PinStatus.Nothing,
                        serverPin.id,
                        serverPin.lastEdited,
                        serverPin.userId))
            }
        }

        // Delete all pins that the user wanted to delete
        val deletedPins = pinDao.getDeletedPins()
        deletedPins.forEach { deletedPin ->
            if (uceServerApi.deletePin(deletedPin.serverId).isSuccessful) {
                pinDao.delete(deletedPin)
            }
        }
    }

    override suspend fun uploadAllPins() {
        val pinIds = pinDao.getAllLocalWithContents().map { pinWithContents ->
            pinWithContents.pin.id
        }
        uploadPins(pinIds = pinIds.toLongArray())
    }

    @OptIn(ExperimentalPathApi::class)
    override fun uploadPins(vararg pinIds: Long) {
        val workConstraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

        val workData = Data.Builder()
                .putLongArray(WorkerConstants.KEY_UPLOAD_PINS_IDS, pinIds)
                .build()

        val uploadPinsWork = OneTimeWorkRequestBuilder<UploadPinsWorker>()
                .setConstraints(workConstraints)
                .setInputData(workData)
                .addTag(WorkerConstants.UPLOAD_PINS_TAG)
                .build()

        workManager.cancelUniqueWork(WorkerConstants.UPLOAD_PINS_NAME)

        workManager.enqueueUniqueWork(
                WorkerConstants.UPLOAD_PINS_NAME,
                ExistingWorkPolicy.APPEND_OR_REPLACE,
                uploadPinsWork
        )
    }

    override suspend fun getPin(pinId: Long): Pin {
        return pinDao.get(pinId)
    }

    override suspend fun getPinContent(pinId: Long): List<PinContent> {
        val pinWithContents = pinDao.getPinWithContents(pinId)
        val pinContentResponse = uceServerApi.getPin(pinWithContents.pin.serverId)
        if (pinContentResponse.code() != 200) {
            Timber.w("Request failed ${pinContentResponse.code()}")
            return pinWithContents.contents
        }
        val getPinResponse = pinContentResponse.body()!!
        val nContents = getPinResponse.contents.size

        if(pinWithContents.pin.status != PinStatus.Saved)
            cachePin(pinId, getCurrentTimeSeconds())

        // First check if we already downloaded pin content
        return if (pinWithContents.pin.lastEdited >= getPinResponse.lastEdited && pinWithContents.contents.size == nContents) {
            pinWithContents.contents
        } else {
            // If the pin is updated, delete the already downloaded images. Otherwise the there will
            // be more of the same image
            deleteAllPinContent(pinId)
            retrieveRemoteContent(getPinResponse.contents, pinWithContents.pin)
        }
    }

    private suspend fun retrieveRemoteContent(contents: List<PinContentInfo>, pin: Pin): List<PinContent> {
        val returnList = mutableListOf<PinContent>()
        for (contentInfo in contents) {
            val pinContent = downloadPinContent(pin.id, pin.serverId, pin.title, contentInfo)
                    ?: continue
            returnList.add(pinContent)
            Timber.i("DOWNLOADING: ${pinContent.uri}")
        }
        return returnList
    }

    private suspend fun deleteAllPinContent(pinId: Long) {
        // First delete the physical images
        pinContentDao.getAll(pinId).also { pinContentList ->
            for (pinContent in pinContentList) {
                pinContent.uri?.toUri()?.let { mediaAccess.deleteImage(it) }
            }
        }
        // Then delete the references in the database
        pinContentDao.deletePinContents(pinId)
    }

    private suspend fun deletePinContent(content: PinContent) {
        content.uri?.toUri()?.let {
            mediaAccess.deleteImage(it)
            pinContentDao.delete(content.id)
        }
    }

    private suspend fun downloadPinContent(
            pinId: Long,
            serverId: Long,
            pinTitle: String,
            contentInfo: PinContentInfo)
            : PinContent? {
        val contentResponse = uceServerApi.getPinContent(serverId, contentInfo.id)
        Timber.i("Downloading content: [$pinId] index:[${contentInfo.contentNr}]")
        if (contentResponse.code() != 200) {
            Timber.w("Request failed ${contentResponse.code()}")
            return null
        }
        val content = contentResponse.body() ?: return null

        val bytes = content.byteStream()
        val bitmap = BitmapFactory.decodeStream(bytes)

        val contentTitle = pinTitle + " " + contentInfo.contentNr + " " + getCurrentTime()
        val uri = mediaAccess.storeImage(contentTitle, "uce_pin_content", bitmap, Bitmap.CompressFormat.JPEG)

        val pinContent = PinContent(pinId, uri.toString(), contentInfo.contentNr)
        pinContentDao.insert(pinContent)

        return pinContent
    }

    override suspend fun cachePin(pinId: Long, currentTime: Long) {
        val cachedPin = CachedPin(pinId, currentTime)
        cachedPinDao.insert(cachedPin)
        val pins = cachedPinDao.getCachedPins()
        for (i in 0 until (pins.size - Caching.MAX_NR_OF_PINS)) {
            deleteAllPinContent(pins[i].pin.id)
            cachedPinDao.delete(pins[i].cachedPin)
        }
    }

    override suspend fun savePin(pinId: Long) {
        val pin = pinDao.get(pinId)
        if (pin.status == PinStatus.Nothing)
        {
            pin.apply {
                status = PinStatus.Saved
            }
            pinDao.update(pin)
            cachedPinDao.delete(pinId)
        }
    }

    override suspend fun unsavePin(pinId: Long) {
        val pin = pinDao.get(pinId)
        if (pin.status == PinStatus.Saved) {
            pin.apply {
                status = PinStatus.Nothing
            }
            pinDao.update(pin)
            cachedPinDao.insert(CachedPin(pinId, getCurrentTimeSeconds()))
        }
    }

    override suspend fun downloadAllBooks() {
        val response = uceServerApi.getBooks()
        if (!response.isSuccessful)
            return
        response.body()?.forEach { bookInfo ->
            val bookResponse = uceServerApi.getBook(bookInfo.bookId)
            if (bookResponse.isSuccessful) {
                bookResponse.body()?.let { book ->
                    val localBook = bookDao.get(book.id)
                    if(localBook == null){
                        downloadBook(book)
                    }
                    else if(localBook.lastEdited < book.lastEdited){
                        deleteBook(book.id)
                        downloadBook(book)
                    }
                }
            }
        }
    }

    /**
     * Completely deletes everything from a book, including images and text.
     */
    private suspend fun deleteBook(bookId: Long){
        val bookWithContents = bookDao.getBookWithContents(bookId)
        bookWithContents.sections.forEach{ section ->
            section.contents.forEach{ content ->
                if(content.contentType == BookContentType.IMAGE){
                    deleteImage(content.content.toUri())
                }
            }
        }
        bookDao.delete(bookWithContents.book)
    }

    /**
     * Gets all book information (book, sections and contents) from the server and inserts it in our room database.
     */
    private suspend fun downloadBook(book: GetBookResponse){
        bookDao.insert(Book(
                book.id,
                book.title,
                "Preview",
                book.lastEdited
        ))
        book.sections.forEach { section ->
            bookDao.insertSection(BookSection(
                    section.bookSectionId,
                    section.bookId,
                    section.title,
                    section.sectionNr
            ))
            section.contents.forEach { content ->
                downloadBookContent(book.title, section.title, content)?.let {
                    bookDao.insertContent(it)
                }
            }
        }
    }

    /**
     * Downloads specific content for a book.
     * Can only download images or text, it will ignore content that specifies video or quiz.
     */
    private suspend fun downloadBookContent(bookTitle: String,
                                            sectionTitle: String,
                                            contentInfo: GetBookContentResponse): BookContent? {
        val contentResponse = uceServerApi.getBookContent(contentInfo.bookContentId)
        if (contentResponse.code() != 200) {
            Timber.w("Request failed ${contentResponse.code()}")
            return null
        }
        val content = contentResponse.body() ?: return null
        var bookContent: BookContent? = null
        when (contentInfo.contentType) {
            "text" -> {
                val text = content.string()
                bookContent = BookContent(
                        contentInfo.bookContentId,
                        contentInfo.bookSectionId,
                        text,
                        contentType = BookContentType.TEXT,
                        contentInfo.contentNr
                )
            }
            "image" -> {
                val bytes = content.byteStream()
                val bitmap = BitmapFactory.decodeStream(bytes)
                val contentTitle = "$bookTitle$sectionTitle${contentInfo.contentNr}"
                val uri = mediaAccess.storeImage(contentTitle, "uce_pin_content", bitmap, Bitmap.CompressFormat.JPEG)
                bookContent = BookContent(
                        contentInfo.bookContentId,
                        contentInfo.bookSectionId,
                        uri.toString(),
                        contentType = BookContentType.IMAGE,
                        contentInfo.contentNr
                )
            }
        }
        return bookContent
    }

    override suspend fun storeImage(fileName: String, folderName: String, bitmap: Bitmap, format: Bitmap.CompressFormat) {
        mediaAccess.storeImage(fileName, folderName, bitmap, format)
    }

    override suspend fun deleteImage(uri: Uri) {
        mediaAccess.deleteImage(uri)
    }

    /**
     * Used to convert a pin to a feed-viable item.
     */
    private fun pinToContentItem(pin: Pin): ContentFeedItem {
        return ContentFeedItem(
                pin.id,
                ContentType.PIN,
                pin.title,
                R.drawable.profile_my_pins,
                convertLongToDateString(pin.lastEdited),
                pin.text,
                LatLng(pin.lat, pin.lon).toUtm()
        )
    }

    /**
     * Used to convert a book to a feed-viable item.
     */
    private fun bookToContentItem(book: Book): ContentFeedItem {
        return ContentFeedItem(
                book.id,
                ContentType.BOOK,
                book.title,
                R.drawable.ic_library_feed_item_icon,
                convertLongToDateString(book.lastEdited),
                book.previewText
        )
    }
}
