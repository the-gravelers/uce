/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.di

import android.content.Context
import com.thegravelers.uce.database.UceDatabase
import com.thegravelers.uce.database.dao.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideUceDatabase(@ApplicationContext context: Context): UceDatabase {
        return UceDatabase.getInstance(context)
    }

    @Provides
    fun providePinDao(database: UceDatabase): PinDao {
        return database.pinDao()
    }

    @Provides
    fun providePinContentDao(database: UceDatabase): PinContentDao {
        return database.pinContentDao()
    }

    @Provides
    fun providePinGroupDao(database: UceDatabase): PinGroupDao {
        return database.pinGroupDao()
    }

    @Provides
    fun provideUserDao(database: UceDatabase): UserDao {
        return database.userDao()
    }

    @Provides
    fun provideUserGroupDao(database: UceDatabase): UserGroupDao {
        return database.userGroupDao()
    }

    @Provides
    fun provideBookDao(database: UceDatabase): BookDao {
        return database.bookDao()
    }

    @Provides
    fun provideCachedPinDao(database: UceDatabase): CachedPinDao{
        return database.cachedPinDao()
    }

    @Provides
    fun provideBookSectionDao(database: UceDatabase): BookSectionDao{
        return database.bookSectionDao()
    }

    @Provides
    fun provideBookContentDao(database: UceDatabase): BookContentDao{
        return database.bookContentDao()
    }
}