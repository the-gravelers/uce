/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Qualifier

data class DispatcherWrapper(
        val value: CoroutineDispatcher
)

@Module
@InstallIn(SingletonComponent::class)
object CoroutineModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class DefaultDispatcher

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class IoDispatcher

    @DefaultDispatcher
    @Provides
    fun provideDispatcherUnconfined(): CoroutineDispatcher = Dispatchers.Unconfined


    @IoDispatcher
    @Provides
    fun provideDispatcherIO(): CoroutineDispatcher = Dispatchers.IO

}