/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.thegravelers.uce.network.ServiceGenerator
import com.thegravelers.uce.network.uceserver.UceServerApiService
import com.thegravelers.uce.network.uceserver.UceServerPaths
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }

    @Provides
    fun provideRetrofitBuilder(moshi: Moshi) : Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl(UceServerPaths.BASE_URL)
                // Call asLenient() to accept Strings as responses,
                // otherwise you will get a MalformedJsonException
                .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
    }

    @Provides
    fun provideUceServerApiService(serviceGenerator: ServiceGenerator): UceServerApiService {
        return serviceGenerator.createService(UceServerApiService::class.java)
    }
}
