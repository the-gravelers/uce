/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.di

import com.thegravelers.uce.repository.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    /**
     * Binds an instance of UserRepository
     */
    @Binds
    abstract fun bindUserRepository(impl: UserRepositoryImpl) : UserRepository

    /**
     * Binds an instance of ContentRepository
     */
    @Binds
    abstract fun bindContentRepository(impl: ContentRepositoryImpl):  ContentRepository

}