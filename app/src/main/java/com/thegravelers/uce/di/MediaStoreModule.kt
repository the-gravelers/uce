/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.di

import com.thegravelers.uce.repository.mediaaccess.MediaAccess
import com.thegravelers.uce.repository.mediaaccess.MediaAccessImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class MediaStoreModule {

    @Binds
    abstract fun bindMediaStoreAccess(impl: MediaAccessImpl): MediaAccess
}