/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.di

import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import android.content.res.AssetManager
import android.content.res.Resources
import androidx.work.WorkManager

import com.squareup.moshi.Moshi
import com.thegravelers.uce.R
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object AppModule {
    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context) : SharedPreferences{
        return context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideSessionManager(preferences: SharedPreferences, moshi: Moshi) : SessionManager {
        return SessionManager(preferences, moshi)
    }

    @Provides
    fun provideContentResolver(@ApplicationContext context: Context): ContentResolver {
        return context.contentResolver
    }

    @Singleton
    @Provides
    fun provideResources(@ApplicationContext context: Context) : Resources {
        return context.resources
    }

    @Singleton
    @Provides
    fun provideAssets(@ApplicationContext context: Context) : AssetManager{
        return context.assets
    }

    @Singleton
    @Provides
    fun provideWorkManager(@ApplicationContext context: Context) : WorkManager {
        return WorkManager.getInstance(context)
    }
}
