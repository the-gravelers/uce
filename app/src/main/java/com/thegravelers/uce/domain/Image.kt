/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.domain

import android.content.ContentResolver
import android.content.ContentUris
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.recyclerview.widget.DiffUtil
import com.thegravelers.uce.util.Version
import com.thegravelers.uce.util.convertLongToDate
import java.util.*

/**
 * These are the [MediaStore.Images.ImageColumns] used when the [ContentResolver] is queried.
 */
data class ImageColumns(
        val idColumn: Int,
        val displayNameColumn: Int,
        val widthColumn: Int,
        val heightColumn: Int,
        val dateAddedColumn: Int,
        val relativePathColumn: Int)

/**
 * This data class is a representation of an image within our app.
 * It contains the functionality when querying images using the [MediaStore]
 */
data class Image(
        val id: Long,
        val name: String,
        val width: Int,
        val height: Int,
        val dateAdded: Date,
        val relativePath: String,
        val uri: Uri) {
    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<Image>() {
            override fun areItemsTheSame(oldItem: Image, newItem: Image) =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Image, newItem: Image) =
                    oldItem == newItem
        }

        // TODO: Internal or external? Probably with SessionManager
        fun getCollection(): Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI

        /**
         * @return A list of which columns to return.
         */
        fun getProjection(): Array<String> {
            val projection = arrayListOf(
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.WIDTH,
                    MediaStore.Images.Media.HEIGHT,
                    MediaStore.Images.Media.DATE_ADDED)
            if (Build.VERSION.SDK_INT > Version.LEGACY)
                projection.add(MediaStore.Images.Media.RELATIVE_PATH)
            return projection.toTypedArray()
        }

        /**
         * Cache the column indices so that you don't need to call getColumnIndex each time you process a row from the query result.
         */
        fun getImageColumns(cursor: Cursor): ImageColumns {
            val idColumn = cursor.getColumnIndex(MediaStore.Images.Media._ID)
            val displayNameColumn = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME)
            val widthColumn = cursor.getColumnIndex(MediaStore.Images.Media.WIDTH)
            val heightColumn = cursor.getColumnIndex(MediaStore.Images.Media.HEIGHT)
            val dateAddedColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED)
            val relativePathColumn =
                    if (Build.VERSION.SDK_INT > Version.LEGACY)
                        cursor.getColumnIndex(MediaStore.Images.Media.RELATIVE_PATH)
                    else -1

            return ImageColumns(
                    idColumn = idColumn,
                    displayNameColumn = displayNameColumn,
                    widthColumn = widthColumn,
                    heightColumn = heightColumn,
                    dateAddedColumn = dateAddedColumn,
                    relativePathColumn = relativePathColumn
            )
        }

        /**
         * Get the columnvalues to create an [Image]
         */
        fun getColumnValues(cursor: Cursor, imageColumns: ImageColumns, collection: Uri): Image {
            val id = cursor.getLong(imageColumns.idColumn)
            val displayName = cursor.getString(imageColumns.displayNameColumn)
            val width = cursor.getInt(imageColumns.widthColumn)
            val height = cursor.getInt(imageColumns.heightColumn)
            val dateAdded = convertLongToDate(cursor.getLong(imageColumns.dateAddedColumn))
            val relativePath =
                    if (Build.VERSION.SDK_INT > Version.LEGACY)
                        cursor.getString(imageColumns.relativePathColumn)
                    else ""

            val contentUri = ContentUris.withAppendedId(collection, id)

            return Image(id, displayName, width, height, dateAdded, relativePath, contentUri)
        }

        fun getContentValues(
                displayName: String? = null,
                relativePath: String? = null,
                mimeType: String? = null
        ): ContentValues {
            return ContentValues().apply {
                displayName?.let {
                    put(MediaStore.Images.Media.DISPLAY_NAME, displayName)
                }
                if (Build.VERSION.SDK_INT > Version.LEGACY) {
                    relativePath?.let {
                        put(MediaStore.Images.Media.RELATIVE_PATH, relativePath)
                    }
                }
                mimeType?.let {
                    put(MediaStore.Images.Media.MIME_TYPE, mimeType)
                }
            }
        }
    }
}