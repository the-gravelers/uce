/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.book

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.thegravelers.uce.databinding.BookFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookFragment : Fragment() {

    lateinit var binding: BookFragmentBinding
    val viewModel: BookViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = BookFragmentBinding.inflate(inflater)
        val adapter = BookSectionAdapter()
        binding.bookSectionList.adapter = adapter

        viewModel.setBookInFocus(BookFragmentArgs.fromBundle(requireArguments()).bookId)

        viewModel.bookInFocus.observe(viewLifecycleOwner, {
            it.let {
                binding.bookTitle.text = it.book.title
                adapter.submitList(it.sections)
            }
        })

        return binding.root
    }
}