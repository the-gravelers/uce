/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion

interface OnBackPressed {
    /**
     * @return Whether or not the super should be called
     */
    fun onBackPressed(): Boolean
}