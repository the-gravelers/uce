package com.thegravelers.uce.screens.start.teamcode

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.databinding.StartTeamMembersListItemBinding
import com.thegravelers.uce.databinding.StartingTeamMembersBinding


class StartStudentAdapter(private val context: Context, private val userGroupWithUsers: UserGroupWithUsers) : BaseAdapter() {
    private lateinit var binding: StartTeamMembersListItemBinding

    /**
     * sets the textview of the team_code_list_item to studentName
     */
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        binding = if (convertView != null){
            convertView.tag as StartTeamMembersListItemBinding
        }
        else {
            val layoutInflater = LayoutInflater.from(context)
            StartTeamMembersListItemBinding.inflate(layoutInflater)
        }
        binding.studentName.text = userGroupWithUsers.users[position].name
        binding.root.tag = binding
        return binding.root
    }

    override fun getItem(position: Int): Any {
        return userGroupWithUsers.users[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return userGroupWithUsers.users.size
    }
}
