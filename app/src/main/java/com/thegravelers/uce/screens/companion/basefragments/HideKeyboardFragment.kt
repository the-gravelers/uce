/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.basefragments

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.thegravelers.uce.screens.companion.CompanionActivity

open class HideKeyboardFragment : Fragment() {
    private class HideKeyboard(lifecycle: Lifecycle, private val companionActivity: CompanionActivity) : LifecycleObserver {
        init {
            lifecycle.addObserver(this)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun hideKeyboardOnDestroy() {
            companionActivity.hideKeyboard()
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun hideKeyboardOnCreate() {
            companionActivity.hideKeyboard()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(requireActivity() is CompanionActivity){
            HideKeyboard(this.lifecycle, requireActivity() as CompanionActivity)
        }
    }
}