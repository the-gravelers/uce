/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map.geologic

import android.content.res.AssetManager
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import java.io.InputStream

@AssistedFactory
interface GeologicInfoParserFactory {
    fun create(path: String): GeologicInfoParser
}

class GeologicInfoParser @AssistedInject constructor(assetManager: AssetManager, @Assisted path: String) {

    var geologicInfoDB: HashMap<String, List<String>>
        private set

    init {
        geologicInfoDB = createDB(assetManager.open("$path/GeologicInfo.txt"))
    }

    private fun createDB(inputStream: InputStream): HashMap<String, List<String>> {
        val hashMap: HashMap<String, List<String>> = hashMapOf()
        var codeIndex: Int
        var takeCount: Int
        var firstLine: List<String>
        inputStream.bufferedReader().readLine().let {
            firstLine = it.split("\t")
            codeIndex = firstLine.indexOf("Code")
            takeCount = firstLine.count() - codeIndex - 1
            firstLine = firstLine.takeLast(takeCount)
        }
        inputStream.bufferedReader().forEachLine {
            val cells = it.split("\t")
            if(cells.count() > codeIndex){
                val code = cells[codeIndex]
                if(!hashMap.containsKey(code)){
                    hashMap[code] = cells.takeLast(takeCount).mapIndexed{ index, s -> firstLine[index] + ": " + s }
                }
            }
        }
        return hashMap
    }
}