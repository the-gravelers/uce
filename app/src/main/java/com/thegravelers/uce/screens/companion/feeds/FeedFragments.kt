/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.thegravelers.uce.repository.ContentFeedItem
import com.thegravelers.uce.R
import com.thegravelers.uce.repository.ContentType
import com.thegravelers.uce.util.extensions.observeOnce
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class PinFeedFragment : ContentFeedFragment() {
    private val pinFeedViewModel by activityViewModels<PinFeedViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = pinFeedViewModel
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.contentFeedTitle.text = getString(R.string.pins)
        viewModel.navigateToPinFragment.observe(viewLifecycleOwner, { contentId ->
            contentId?.let {
                findNavController().navigate(PinFeedFragmentDirections.actionPinFeedFragmentToPinFragment(it))
                viewModel.onContentFragmentNavigated()
            }
        })
        pinFeedViewModel.refreshPins()
        return binding.root
    }
}

@AndroidEntryPoint
class SearchFeedFragment : ContentFeedFragment() {
    private val searchFeedViewModel by activityViewModels<SearchFeedViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = searchFeedViewModel
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.contentFeedTitle.text = getString(R.string.search_pins)
        viewModel.navigateToPinFragment.observe(viewLifecycleOwner, { contentId ->
            contentId?.let {
                val value = (viewModel as SearchFeedViewModel).navigateWithType.value
                if (value == ContentType.PIN) {
                    findNavController().navigate(SearchFeedFragmentDirections.actionSearchFeedFragmentToPinFragment(it))
                    viewModel.onContentFragmentNavigated()
                }
                else if (value == ContentType.BOOK) {
                    findNavController().navigate(SearchFeedFragmentDirections.actionSearchFeedFragmentToBookFragment(it))
                    viewModel.onContentFragmentNavigated()
                }
            }
        })
        return binding.root
    }
}

@AndroidEntryPoint
class BookFeedFragment : ContentFeedFragment() {
    private val bookFeedViewModel by activityViewModels<BookFeedViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = bookFeedViewModel
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.contentFeedTitle.text = getString(R.string.library)
        viewModel.navigateToPinFragment.observe(viewLifecycleOwner, { contentId ->
            contentId?.let {
                findNavController().navigate(BookFeedFragmentDirections.actionBookFeedFragmentToBookFragment(it))
                viewModel.onContentFragmentNavigated()
            }
        })
        bookFeedViewModel.downloadBooks()
        return binding.root
    }
}

@AndroidEntryPoint
class MyPinsFeedFragment : ContentFeedFragment() {
    private val myPinsFeedViewModel by activityViewModels<MyPinsFeedViewModel>()
    private lateinit var itemTouchHelperCallback:SwipeToDeleteCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = myPinsFeedViewModel
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.contentFeedTitle.text = getString(R.string.my_pins)
        itemTouchHelperCallback = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle("Deleting pin")
                builder.setMessage("Are you sure you want to delete this pin?")
                        .setPositiveButton("Yes"
                        ) { dialog, _ ->
                            viewModel.feedItemList.observeOnce(viewLifecycleOwner, Observer {
                                myPinsFeedViewModel.deleteFeedItem(it[viewHolder.adapterPosition].id)
                            })
                            binding.contentFeedList.adapter?.notifyItemChanged(viewHolder.adapterPosition)
                            dialog.cancel()
                        }
                        .setNegativeButton("No"
                        ) { dialog, _ ->
                            binding.contentFeedList.adapter?.notifyItemChanged(viewHolder.adapterPosition)
                            dialog.cancel()
                        }
                builder.setCancelable(false)
                builder.show()
            }
        }

        viewModel.navigateToPinFragment.observe(viewLifecycleOwner, { contentId ->
            contentId?.let {
                findNavController().navigate(MyPinsFeedFragmentDirections.actionMyPinsFeedFragmentToPinFragment(it))
                viewModel.onContentFragmentNavigated()
            }
        })

        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(binding.contentFeedList)
        return binding.root
    }
}

@AndroidEntryPoint
class SavedPinsFeedFragment : ContentFeedFragment() {
    private val savedPinsFeedViewModel by activityViewModels<SavedPinsFeedViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = savedPinsFeedViewModel
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.contentFeedTitle.text = getString(R.string.saved_pins)

        viewModel.navigateToPinFragment.observe(viewLifecycleOwner, { contentId ->
            contentId?.let {
                findNavController().navigate(SavedPinsFeedFragmentDirections.actionSavedPinsFeedFragmentToPinFragment(it))
                viewModel.onContentFragmentNavigated()
            }
        })
        return binding.root
    }
}
