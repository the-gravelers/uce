/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.book

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.thegravelers.uce.database.entity.BookSectionWithContents
import com.thegravelers.uce.databinding.BookSectionListItemBinding

class BookSectionAdapter : ListAdapter<BookSectionWithContents, BookSectionAdapter.ViewHolder>(BookSectionItemCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    /**
     * A ViewHolder holds a view for the [RecyclerView] as well as providing additional information
     * to the RecyclerView such as where on the screen it was last drawn during scrolling.
     */
    class ViewHolder private constructor(val binding: BookSectionListItemBinding) : RecyclerView.ViewHolder(binding.root){
        companion object{
            fun from(parent : ViewGroup) : ViewHolder{
                val inflater = LayoutInflater.from(parent.context)
                val binding = BookSectionListItemBinding.inflate(inflater, parent, false)
                return ViewHolder(binding)
            }
        }

        //sets all the values for the items for the feed item in xml to that of the feed item data
        fun bind(item: BookSectionWithContents){
            binding.sectionTitle.text = item.section.title
            binding.sectionTitle.setOnClickListener {
                if(binding.bookContentList.visibility == View.VISIBLE){
                    binding.bookContentList.visibility = View.GONE
                    binding.isCollapsed = true

                }
                else if (binding.bookContentList.visibility == View.GONE){
                    binding.bookContentList.visibility = View.VISIBLE
                    binding.isCollapsed = false
                    }
            }
            val adapter = BookContentAdapter()
            binding.bookContentList.adapter = adapter

            adapter.submitList(item.contents)
            binding.executePendingBindings()
        }
    }

    /**
     * [BookSectionItemCallback] extends [DiffUtil] to calculate the changes between content when the list changes
     * and update the list accordingly.
     */
    class BookSectionItemCallback : DiffUtil.ItemCallback<BookSectionWithContents>(){
        override fun areItemsTheSame(oldItem: BookSectionWithContents, newItem: BookSectionWithContents): Boolean {
            return oldItem.section.id == newItem.section.id
        }

        override fun areContentsTheSame(oldItem: BookSectionWithContents, newItem: BookSectionWithContents): Boolean {
            // ContentFeedItem is a data class that contains methods for comparing items
            return oldItem == newItem
        }
    }
}