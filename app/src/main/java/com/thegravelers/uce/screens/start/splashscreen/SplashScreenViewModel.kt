/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.splashscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thegravelers.uce.repository.UserRepository
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashScreenViewModel @Inject constructor(
        private val userRepository: UserRepository
) : ViewModel() {

    private val _loggedIn = MutableLiveData<Boolean>()
    val loggedIn: LiveData<Boolean> get() = _loggedIn

    private val _inTeam = MutableLiveData<Boolean>()
    val inTeam: LiveData<Boolean> get() = _inTeam

    fun checkIfUserLoggedIn() {
        if (userRepository.getAuthInfo() != null) {
            // We have an active user, so let's log him/her in.
            userRepository.startRefreshTokenWorker(false)
            _loggedIn.value = true
        } else {
            _loggedIn.value = false
        }
    }

    /**
     * Checks in our database if the userId from the sessionManager is in UserGroup.
     * @return True if the user is already in a group. False otherwise.
     */
    fun checkIfInGroup() {
        viewModelScope.launch {
            _inTeam.value = userRepository.getTeamIdFromUser() != null
        }
    }
}