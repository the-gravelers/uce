/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.pin

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.mapbox.mapboxsdk.geometry.LatLng
import com.thegravelers.uce.R
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.PinStatus
import com.thegravelers.uce.databinding.PinFragmentBinding
import com.thegravelers.uce.screens.companion.CompanionActivity
import com.thegravelers.uce.screens.companion.OnBackPressed
import com.thegravelers.uce.screens.companion.gallery.GalleryAdapter
import com.thegravelers.uce.screens.companion.gallery.SharedGalleryViewModel
import com.thegravelers.uce.util.convertLongToDateString
import com.thegravelers.uce.util.extensions.toUtm
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

/**
 * This fragment is used to view and to edit pins
 */
@AndroidEntryPoint
class PinFragment(var containerId: Int = 0) : Fragment(), OnBackPressed, PermissionListener {

    lateinit var binding: PinFragmentBinding
    lateinit var pinViewModel: PinViewModel
        private set
    private val galleryViewModel by activityViewModels<SharedGalleryViewModel>()
    private lateinit var viewPager: ViewPager2
    private lateinit var bottomSheetDialogPictures: BottomSheetDialog
    private lateinit var galleryActivity: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // A listener that checks if the local picture activity returns a picture and then adds it to the pin picture list
        galleryActivity = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            pinViewModel.navigateToGalleryActivity(false)
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                data?.data?.let { galleryViewModel.addImage(it) }
                viewPager.adapter?.notifyDataSetChanged()
            }
        }

        if (containerId == R.id.map_bottom_sheet_container) {
            val viewModel by activityViewModels<PinViewModel>()
            pinViewModel = viewModel
        } else {
            val viewModel by viewModels<PinViewModel>()
            pinViewModel = viewModel
            pinViewModel.enableFromPinFeed()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate layout
        binding = PinFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.pinViewModel = pinViewModel

        exitTransition = TransitionInflater.from(context)
                .inflateTransition(R.transition.grid_exit_transition)
        postponeEnterTransition()

        initBottomSheetDialog(container)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        startPostponedEnterTransition()
        initViewPager()
        initEditability()
        initGalleryViewModelObservers()
        initPinViewModelObservers()
        setupBottomSheetDialog()
        setupBookmark()
        checkForArguments()
    }

    /**
     * This check is needed for when we navigate to this fragment.
     * Usually happens from a feed.
     */
    private fun checkForArguments() {
        if (arguments != null) {
            pinViewModel.enableFromPinFeed()
            val args = PinFragmentArgs.fromBundle(requireArguments())
            setPinFocused(args.pinId)
            containerId = R.id.nav_host_fragment
        }
    }

    private fun initViewPager() {
        viewPager = binding.photoViewPager
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                updateImageCount(position)
                super.onPageSelected(position)
            }
        })
        updateImageCount(0)
        viewPager.apply {
            offscreenPageLimit = 2
            this.adapter = GalleryAdapter()
        }
    }

    private fun initEditability() {
        binding.editButton.setOnClickListener {
            pinViewModel.enableEditability()
        }

        binding.cancelButton.setOnClickListener {
            undoChanges()
        }

        binding.applyButton.setOnClickListener {
            pinViewModel.disableEditability()
            if(activity is CompanionActivity) {
                (activity as CompanionActivity).hideKeyboard()
            }
            pinViewModel.getCurrentPin().let { pin ->
                if (pin != null) {
                    pinViewModel.updatePin(
                            pin,
                            binding.titleField.text.toString(),
                            binding.notesField.text.toString(),
                            galleryViewModel.getImages())
                } else {
                    pinViewModel.createPin(
                            binding.titleField.text.toString(),
                            binding.notesField.text.toString(),
                            galleryViewModel.getImages())
                }
            }
        }
    }

    private fun initBottomSheetDialog(container: ViewGroup?){
        // Make a instance of a BottomSheetDialog
        bottomSheetDialogPictures = BottomSheetDialog(requireContext())
        bottomSheetDialogPictures.setContentView(layoutInflater.inflate(R.layout.pin_photo_bottom_sheet, container, false))
    }

    private fun setupBottomSheetDialog() {
        if(!this::bottomSheetDialogPictures.isInitialized){
            Timber.w("BottomSheetDialogPictures is not initialized, did you forget to call initBottomSheetdialog()")
            return
        }
        binding.backButton.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.openPictureModalButton.setOnClickListener {
            bottomSheetDialogPictures.show()
        }

        // Button for opening the camera to make a picture for the pin
        val buttonCamera: ImageButton = bottomSheetDialogPictures.findViewById(R.id.image_camera_button)!!
        buttonCamera.setOnClickListener {
            findNavController().navigate(R.id.action_global_cameraFragment)
            bottomSheetDialogPictures.dismiss()
        }

        // Button for selection local picture for a pin (first asks for permission to access local storage)
        val buttonLocalPictures: ImageButton = bottomSheetDialogPictures.findViewById(R.id.image_gallery_button)!!
        buttonLocalPictures.setOnClickListener {
            pinViewModel.navigateToGalleryActivity(true)
            Dexter.withContext(context)
                    .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .withListener(this)
                    .check()
            bottomSheetDialogPictures.dismiss()
        }
    }

    private fun setupBookmark(){
        binding.bookmark.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                pinViewModel.savePinInFocus()
            } else {
                pinViewModel.unsavePinInFocus()
            }
        }
    }

    private fun initGalleryViewModelObservers() {
        galleryViewModel.images.observe(viewLifecycleOwner, {
            val adapter = binding.photoViewPager.adapter as GalleryAdapter
            adapter.submitList(listOf())
            adapter.submitList(it)
            adapter.notifyDataSetChanged()
        })
        galleryViewModel.notifyItemRemoved.observe(viewLifecycleOwner, {
            if (it != null) {
                (binding.photoViewPager.adapter as GalleryAdapter).notifyItemRemoved(it)
                galleryViewModel.itemRemoved()
            }
        })
    }

    private fun initPinViewModelObservers() {
        pinViewModel.pinInFocus.observe(viewLifecycleOwner, {
            if (it != null) {
                loadPin(it)
            } else {
                clearPin()
            }
        })
        pinViewModel.newPinContent.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                for (content in it) {
                    content.uri?.let { uri ->
                        galleryViewModel.addImage(uri.toUri())
                    }
                }
                pinViewModel.clearPinContent()
            }
        })
        pinViewModel.isEditable.observe(viewLifecycleOwner, {
            val visibility = if (!it) View.VISIBLE else View.INVISIBLE
            binding.editButton.visibility = visibility
            binding.backButton.visibility = visibility
            (binding.photoViewPager.adapter as GalleryAdapter).isClickable = !it
        })
        pinViewModel.revert.observe(viewLifecycleOwner, {
            if (it) {
                undoChanges()
                pinViewModel.performedRevert()
                pinViewModel.getCurrentPin()?.let { it1 -> loadPin(it1) }
            }
        })
    }

    /**
     * Sets a pin into focus in the viewmodel
     * @param[pinId] The pin id
     */
    fun setPinFocused(pinId: Long) {
        pinViewModel.retrievePin(pinId)
    }

    /**
     * Loads a pin into the fragment
     * @param [pin] The pin that is loaded
     */
    private fun loadPin(pin: Pin) {
        binding.titleField.setText(pin.title)
        binding.notesField.setText(pin.text)
        if (galleryViewModel.getImages().orEmpty().count() == 0) {
            pinViewModel.getPinContent(pin.id)
            binding.titleFieldView.text = pin.title
            binding.notesFieldView.text = pin.text
            binding.dateFieldView.text = convertLongToDateString(pin.lastEdited)
            binding.locationText.text = LatLng(pin.lat, pin.lon).toUtm()
            //binding.madeByText.setText("Made by: " + pinContents.pin.userId)
            binding.bookmark.isChecked = pin.status == PinStatus.Saved
        }
    }

    /**
     * Clears the current fields from the fragment
     */
    private fun clearFields() {
        val adapter = viewPager.adapter as GalleryAdapter
        adapter.submitList(listOf())
        binding.titleField.clear(true)
        binding.notesField.clear(true)
        binding.titleFieldView.text = ""
        binding.notesFieldView.text = ""
    }

    /**
     * Resets the changes made to the original pin or empty if there was none
     */
    private fun undoChanges() {
        pinViewModel.disableEditability()
        clearPin()
        pinViewModel.getCurrentPin()?.let { loadPin(it) }
    }

    private fun clearPin() {
        clearFields()
        galleryViewModel.flushImages()
    }

    /**
     * Updates the displayed image count in the pin
     * @param[position] The index of the current image
     */
    private fun updateImageCount(position: Int) {
        val imageCount = viewPager.adapter?.itemCount
        if (imageCount == null || imageCount == 0 || imageCount == 1) {
            binding.imageCounterText.visibility = View.INVISIBLE
        } else {
            binding.imageCounterText.visibility = View.VISIBLE
        }
        val current = imageCount?.let { minOf(position + 1, it) }
        val text = "$current/$imageCount"
        binding.imageCounterText.text = text
    }

    /**
     * Sets up the viewmodel for a clean pin to be created
     * @param[latLng] The position of the pin
     */
    fun beginPinCreation(latLng: LatLng) {
        pinViewModel.clearPinInFocus()
        pinViewModel.setPinLocation(latLng)
        pinViewModel.enableEditability()
    }

    // Go to local pictures and add the selected picture
    override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        galleryActivity.launch(intent)
    }

    // Make AlertDialog for when local storage is denied
    override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Local storage permission")
        builder.setMessage("Accessing your local storage is needed to save pictures for pins by denying you will only be able to add pictures to pins by taking pictures with your camera")
        builder.setIcon(R.drawable.ic_image)
        builder.setNeutralButton("Okay") { dialog, _ ->
            dialog.cancel()
        }
        builder.show()
    }

    override fun onPermissionRationaleShouldBeShown(p0: PermissionRequest?, token: PermissionToken?) {
        token?.continuePermissionRequest()
    }

    override fun onResume() {
//        galleryViewModel.images.value.let {
//            (binding.photoViewPager.adapter as GalleryAdapter).submitList(it)
//            (binding.photoViewPager.adapter as GalleryAdapter).notifyDataSetChanged()
//        }
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearPin()
    }

    override fun onBackPressed(): Boolean {
        undoChanges()
        return true
    }
}
