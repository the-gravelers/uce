/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map.geologic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.thegravelers.uce.databinding.GeologicInfoFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GeologicInfoFragment: Fragment() {

    lateinit var binding: GeologicInfoFragmentBinding
    val viewModel : GeologicInfoViewModel by activityViewModels()
    private lateinit var listView: ListView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = GeologicInfoFragmentBinding.inflate(inflater)
        listView = binding.listView
        val args = GeologicInfoFragmentArgs.fromBundle(requireArguments())
        binding.textView.text = args.id
        viewModel.getInfo(args.id)
        listView.divider = null
        listView.adapter = GeologicInfoAdapter(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        viewModel.geologicInfo.observe(viewLifecycleOwner, {
            (listView.adapter as GeologicInfoAdapter).apply {
                submitList(it)
                notifyDataSetChanged()
            }
            //listener for the back button
            binding.backButtonGeologicInfo.setOnClickListener {
                findNavController().navigateUp()
            }

        })
    }
}