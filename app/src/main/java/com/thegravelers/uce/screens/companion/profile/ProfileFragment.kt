/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import com.thegravelers.uce.R
import com.thegravelers.uce.databinding.ProfileFragmentBinding
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.screens.start.StartActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * The [ProfileFragment] subclass.
 */
@AndroidEntryPoint
class ProfileFragment : Fragment() {
    lateinit var binding: ProfileFragmentBinding
    val viewModel by activityViewModels<ProfileViewModel>()

    @Inject
    lateinit var sessionManager: SessionManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        // Setting up the basic fragments needed for the fragment
        binding = ProfileFragmentBinding.inflate(inflater)

        binding.profileViewModel = viewModel
        binding.lifecycleOwner = this

        // A listener that checks when the switch changes and then saves it to the SharedPreferences
        switchListener(binding.switchGhost)
        switchListener(binding.switchDark)

        // When the fragment is created it makes sure that the correct button states are shown
        getPreferencesSwitch(binding.switchGhost)
        getPreferencesSwitch(binding.switchDark)

        // Functions that check if observers for navigation is called
        navigationObserver(viewModel.navigateToLogin)
        navigationObserver(viewModel.navigateToMyPins)
        navigationObserver(viewModel.navigateToSavedPins)

        return binding.root
    }

    /**
     * This function observes when navigation is needed
     * You have to give it a livedata that will change when navigation is called
     */
    private fun navigationObserver(liveData: LiveData<Boolean>) {
        liveData.observe(viewLifecycleOwner, { doNavigate ->
            if (doNavigate) {
                when (liveData) {
                    viewModel.navigateToLogin -> {
                        startLoginActivity()
                        viewModel.doneNavigatingToLogin()
                    }
                    viewModel.navigateToMyPins -> {
                        findNavController().navigate(R.id.action_profileFragment_to_myPinsFeedFragment)
                        viewModel.doneNavigatingToMyPins()
                    }
                    viewModel.navigateToSavedPins -> {
                        findNavController().navigate(R.id.action_profileFragment_to_savedPinsFeedFragment)
                        viewModel.doneNavigatingToSavedPins()
                    }
                }
            }
        })
    }

    /**
     * when the fragment is destroyed this function is called
     * saves the buttons states to shared preferences
     */
    override fun onDestroyView() {
        super.onDestroyView()
        savePreferencesSwitch(binding.switchGhost)
        savePreferencesSwitch(binding.switchDark)
    }

    private fun switchListener(switch: SwitchCompat) {
        switch.setOnCheckedChangeListener { _, isChecked ->
            when (switch.id) {
                binding.switchGhost.id -> sessionManager.saveGhostMode(isChecked.toString())
                binding.switchDark.id -> {
                    sessionManager.saveDarkMode(isChecked.toString())
                    if (isChecked) {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                    } else {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    }
                }
            }

        }
    }

    /**
     * Retrieves the buttons states from shared preferences and the makes that the state for the buttons in the fragment
     */
    private fun getPreferencesSwitch(switch: SwitchCompat) {
        try {
            val switchState: Boolean
            when (switch.id) {
                binding.switchGhost.id -> {
                    switchState = (sessionManager.fetchGhostMode() == "true")
                    switch.isChecked = switchState
                }
                binding.switchDark.id -> {
                    switchState = (sessionManager.fetchDarkMode() == "true")
                    switch.isChecked = switchState
                }
            }
        } catch (ex: Exception) {
            Toast.makeText(requireContext(), ex.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * saves all the states of the buttons to shared preferences
     */
    private fun savePreferencesSwitch(switch: SwitchCompat) {
        when (switch.id) {
            binding.switchGhost.id -> sessionManager.saveGhostMode(switch.isChecked.toString())
            binding.switchDark.id -> sessionManager.saveDarkMode(switch.isChecked.toString())
        }
    }

    private fun startLoginActivity() {
        val intent = Intent(activity, StartActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }
}
