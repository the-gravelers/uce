/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.thegravelers.uce.repository.ContentFeedItem
import com.thegravelers.uce.repository.ContentRepository
import java.util.*

abstract class ContentFeedViewModel constructor(protected val contentRepository: ContentRepository) : ViewModel(){

    // Contains ALL of the valid content items
    protected var _feedItemList = updateFeedItemList() as MutableLiveData<List<ContentFeedItem>>

    // Filters out all of the content items in _feedItemList who do not conform to the filter.
    val feedItemList: LiveData<List<ContentFeedItem>> get() {
        return Transformations.switchMap(_feedItemList){ contents ->
            val filteredContents = MutableLiveData<List<ContentFeedItem>>()
            Transformations.switchMap(contentFilter){ filter ->
                filteredContents.value = contents.filter{content ->
                    content.title.toLowerCase(Locale.ROOT).contains(filter.toLowerCase(Locale.ROOT))
                }
                filteredContents
            }

        }
    }

    // The filter set by the search bar in the fragment.
    // Filter will be used as case-insensitive
    val contentFilter = MutableLiveData<String>("")

    protected val _navigateToPinFragment = MutableLiveData<Long?>()
    val navigateToPinFragment: LiveData<Long?> get() = _navigateToPinFragment

    fun onContentFeedItemClicked(id: Long) {
        _navigateToPinFragment.value = id
    }

    /**
     * Sets the [_feedItemList] with the correct LiveData from the [ContentRepository]
     * Default returns an empty livedata, other viewmodels need to override this method and provide their own livedata.
     */
    open fun updateFeedItemList(): LiveData<List<ContentFeedItem>>{
        return MutableLiveData<List<ContentFeedItem>>()
    }

    fun onContentFragmentNavigated() {
        _navigateToPinFragment.value = null
    }

    /**
     * Clears the current filter for our content items
     */
    fun clearFilter(){
        contentFilter.value = ""
    }
}
