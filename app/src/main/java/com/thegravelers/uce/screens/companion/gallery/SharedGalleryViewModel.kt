/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.gallery

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import timber.log.Timber
import javax.inject.Inject

/**
 * ViewModel shared between fragments to save pictures the user took
 */
@HiltViewModel
class SharedGalleryViewModel @Inject constructor() : ViewModel() {

    private val _images = MutableLiveData<MutableList<Uri>>(mutableListOf())
    val images: LiveData<MutableList<Uri>> get() = _images

    private val _notifyItemRemoved = MutableLiveData<Int?>()
    val notifyItemRemoved: LiveData<Int?> get() = _notifyItemRemoved

    fun deleteImage(index: Int) {
        _images.value?.let {
            it.removeAt(index)
            _notifyItemRemoved.value = index
            // When the list is empty, set an new empty list so that observers get notified that the list is empty
            if (it.isEmpty()) {
                _images.value = mutableListOf()
            }
            else{
                _images.postValue(it)
            }
        }
    }

    fun addImage(uri: Uri) {
        _images.value?.let {
            it.add(uri)
            _images.value = it
            Timber.i("ADDED IMAGE: $uri")
        }
    }

    fun itemRemoved() {
        _notifyItemRemoved.value = null
    }

    fun flushImages(): MutableList<Uri>? {
        val images = _images.value
        _images.value = mutableListOf()
        return images
    }

    fun getImages(): MutableList<Uri>? {
        return _images.value
    }
}
