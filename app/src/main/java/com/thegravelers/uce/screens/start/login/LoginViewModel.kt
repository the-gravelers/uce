/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.login

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thegravelers.uce.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
        private val userRepository: UserRepository
) : ViewModel() {

    private val _loginSuccessful = MutableLiveData<Boolean>()
    val loginSuccessful: LiveData<Boolean> get() = _loginSuccessful

    private val _inTeam = MutableLiveData<Boolean>()
    val inTeam: LiveData<Boolean> get() = _inTeam

    // Observers for the username and password text fields
    // These are being set directly from within the layout
    var mUserId = ObservableField("kort@uce.nl")
    var mPassword = ObservableField("asdf")

    /**
     * Checks if the user name and password are valid and goes to the next fragment.
     * If that is the case else make the fields empty and let the fragment controller give a toast message
     */
    fun checkLoginCredentials() {
        val userId = mUserId.get()
        val password = mPassword.get()

        viewModelScope.launch {
            if (
                    !userId.isNullOrEmpty()
                    && !password.isNullOrEmpty()
                    && userRepository.login(userId, password)
            ) {
                _loginSuccessful.value = true
            } else {
                _loginSuccessful.value = false
                mPassword.set(null)
            }
        }
    }

    /**
     * Checks in our database if the userId from the sessionManager is in UserGroup.
     * @return True if the user is already in a group. False otherwise.
     */
    fun checkIfInGroup() {
        viewModelScope.launch {
            _inTeam.value = userRepository.getTeamIdFromUser() != null
        }
    }
}