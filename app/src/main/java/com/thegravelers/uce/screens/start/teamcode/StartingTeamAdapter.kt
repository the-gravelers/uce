package com.thegravelers.uce.screens.start.teamcode

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.thegravelers.uce.R
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.databinding.StartingTeamMembersBinding

class StartingTeamAdapter(val context: Context, private val teams: UserGroupWithUsers) : BaseAdapter() {
    private lateinit var binding: StartingTeamMembersBinding


    override fun getCount(): Int {
        return teams.users.size
    }

    override fun getItem(position: Int): Any {
        return teams.users[position]
    }

    override fun getItemId(position: Int): Long {
        return teams.users[position].id
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        binding = if(convertView != null){
            convertView.tag as StartingTeamMembersBinding
        }
        else {
            val layoutInflater = LayoutInflater.from(context)
            StartingTeamMembersBinding.inflate(layoutInflater)
        }

        binding.teamName.text = teams.userGroup.name
        binding.studentList.adapter = StartStudentAdapter(context, teams)
        binding.root.tag = binding
        return binding.root
    }


}
