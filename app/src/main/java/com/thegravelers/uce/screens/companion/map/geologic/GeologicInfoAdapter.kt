/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map.geologic

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.thegravelers.uce.R
import com.thegravelers.uce.databinding.GeologicInfoListItemBinding

class GeologicInfoAdapter(private val layoutInflater: LayoutInflater): BaseAdapter() {
    private var geologicInfo: List<String> = listOf()

    override fun getCount(): Int {
        return this.geologicInfo.count()
    }

    override fun getItem(position: Int): Any {
        return geologicInfo[position]
    }

    override fun getItemId(position: Int): Long {
        return 0L
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return if (convertView == null){
            val binding = GeologicInfoListItemBinding.inflate(layoutInflater)
            binding.geologicInfoListItemTextView.text = geologicInfo[position]
            binding.root
        }
        else {
            convertView.findViewById<TextView>(R.id.geologic_info_list_item_text_view).text = geologicInfo[position]
            convertView
        }
    }

    fun submitList(list: List<String>){
        geologicInfo = list
    }
}