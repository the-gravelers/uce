/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.thegravelers.uce.R
import com.thegravelers.uce.databinding.LoginFragmentBinding
import com.thegravelers.uce.screens.companion.CompanionActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment: Fragment() {

    private lateinit var binding : LoginFragmentBinding
    val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = LoginFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.loginViewModel = viewModel

        // Observer to see if a login attempt was done
        viewModel.loginSuccessful.observe(viewLifecycleOwner, {
            if(it){
                viewModel.checkIfInGroup()
            }
            else {
                showError()
            }
        })

        viewModel.inTeam.observe(viewLifecycleOwner, {
            if(it){
                startCompanionActivity()
            }
            else{
                findNavController().navigate(R.id.action_loginFragment_to_teamListFragment)
            }
        })

        return binding.root
    }

    private fun startCompanionActivity(){
        val intent = Intent(activity, CompanionActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

    private fun showError(){
        val shake = AnimationUtils.loadAnimation(requireContext(), R.anim.shake)
        binding.usernameField.startAnimation(shake)
        binding.passwordField.startAnimation(shake)
        binding.usernameField.error = getString(R.string.login_fail_warning)
    }
}