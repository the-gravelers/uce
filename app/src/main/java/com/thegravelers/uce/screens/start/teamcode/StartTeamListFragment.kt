package com.thegravelers.uce.screens.start.teamcode

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.thegravelers.uce.R
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.databinding.StartTeamListFragmentBinding
import com.thegravelers.uce.screens.companion.CompanionActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint

class StartTeamListFragment : Fragment() {
    private lateinit var binding: StartTeamListFragmentBinding
    val viewModel: StartTeamListViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = StartTeamListFragmentBinding.inflate(inflater)
        binding.startTeamListViewModel = viewModel

        // might need to move this after initializing the listeners
        viewModel.refreshTeamList()

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.refreshTeamList()
        }
        viewModel.stopRefreshingTeams.observe(viewLifecycleOwner, {
            if(it){
                binding.swipeRefresh.isRefreshing = false
            }
        })

        /**
         * navigate to startTeamFragment after successfully joining a team
         */
        viewModel.joinTeamSuccessful.observe(viewLifecycleOwner, {
            if (it) {
                findNavController().navigate(R.id.action_teamListFragment_to_startTeamFragment)
                viewModel.doneNavigatingToStartTeamFragment()
            }
        })

        viewModel.teamsList.observe(viewLifecycleOwner, { teamsList ->
            teamsList?.let {
                binding.TeamList.adapter = context?.let {
                    StartTeamListAdapter(it, teamsList as MutableList<UserGroupWithUsers>, viewModel)
                }
            }
        })

        return binding.root
    }
}