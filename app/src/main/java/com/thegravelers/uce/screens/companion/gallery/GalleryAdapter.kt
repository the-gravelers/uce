/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.gallery

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.thegravelers.uce.databinding.GalleryItemBinding
import com.thegravelers.uce.screens.companion.pin.ImagePagerFragment
import com.thegravelers.uce.screens.companion.pin.PinFragment
import com.thegravelers.uce.util.UriCallback

class GalleryAdapter : ListAdapter<Uri, GalleryAdapter.PhotoViewHolder>(UriCallback()) {

    var isClickable: Boolean = true

    inner class PhotoViewHolder(var binding: GalleryItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(uri: Uri){
            Glide.with(binding.imageView.context).load(uri).into(binding.imageView)
            binding.imageView.transitionName = uri.toString()
            binding.imageView.setOnClickListener {
                onItemClicked()
            }
        }

        /**
         * Switches between showing the image full size in the [ImagePagerFragment] and
         * small in the [PinFragment] by clicking on the image
         */
        private fun onItemClicked(){
            if(!isClickable){
                return
            }
            val transitioningView = binding.imageView
            val transitionName = transitioningView.transitionName
            val fragment = transitioningView.findFragment<Fragment>()
            val fragments = fragment.parentFragmentManager.fragments
            if(fragment is ImagePagerFragment){
                fragment.parentFragmentManager
                        .beginTransaction()
                        .setReorderingAllowed(true) // Optimize for shared element transition
                        .addSharedElement(transitioningView, transitionName)
                        .remove(fragment)
                        .commit()
                val pinFragment = fragments.first()
                if(pinFragment is PinFragment){
                    pinFragment.binding.photoViewPager.setCurrentItem(adapterPosition, false)
                }
            }
            else if (fragment is PinFragment){
                fragment.parentFragmentManager
                        .beginTransaction()
                        .setReorderingAllowed(true) // Optimize for shared element transition
                        .addSharedElement(transitioningView, transitionName)
                        .add(fragment.containerId, ImagePagerFragment(adapterPosition), ImagePagerFragment::class.java
                                .simpleName)
                        .addToBackStack(null)
                        .commit()
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PhotoViewHolder(GalleryItemBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val image = getItem(position)
        holder.bind(image)
    }


}