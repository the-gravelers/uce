package com.thegravelers.uce.screens.start.teamcode

import android.content.Context
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.thegravelers.uce.R
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.databinding.StartTeamListItemBinding
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class StartTeamListAdapter @Inject constructor(val context: Context, private val teams: MutableList<UserGroupWithUsers>,
                                               viewModel: StartTeamListViewModel, ) : BaseAdapter() {

    val mViewModel = viewModel
    val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return teams.size
    }

    override fun getItem(position: Int): Any {
        return teams[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = StartTeamListItemBinding.inflate(layoutInflater)
        rowView.teamName.text = teams[position].userGroup.name
        rowView.studentList.adapter = StartStudentAdapter(context, teams[position])
        rowView.ExpandButton.setOnClickListener {
            if (rowView.studentList.visibility == View.VISIBLE) {
                TransitionManager.beginDelayedTransition(parent, AutoTransition())
                rowView.studentList.visibility = View.GONE
                rowView.ExpandButton.setImageResource(R.drawable.team_arrow_down)
                rowView.JoinTeamButton.visibility = View.GONE
            } else {
                TransitionManager.beginDelayedTransition(parent, AutoTransition())
                rowView.studentList.visibility = View.VISIBLE
                rowView.ExpandButton.setImageResource(R.drawable.team_arrow_up)
                rowView.JoinTeamButton.visibility = View.VISIBLE
            }
        }
        rowView.JoinTeamButton.setOnClickListener {
            val teamID = teams[position].userGroup.id
            mViewModel.joinTeam(teamID)
            //mViewModel.navigateToStartTeamFragment()
        }

        return rowView.root
    }
}