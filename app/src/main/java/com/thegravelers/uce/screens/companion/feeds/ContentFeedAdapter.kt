/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.thegravelers.uce.repository.ContentFeedItem
import com.thegravelers.uce.databinding.ContentFeedListItemBinding
import com.thegravelers.uce.repository.ContentType
import com.thegravelers.uce.util.convertLongToDateString

class ContentFeedAdapter(private val clickListener: ContentFeedItemListener) : ListAdapter<ContentFeedItem, ContentFeedAdapter.ViewHolder>(ContentFeedItemCallback()) {

    /**
     * Called when [RecyclerView] needs a new [ViewHolder]
     * A ViewHolder holds a view for the RecyclerView as well as providing additional information
     * to the RecyclerView such as where on the screen it was last drawn during scrolling.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    /**
     * Called when [RecyclerView] needs to show an item.
     * The [ViewHolder] passed may be recycled, so make sure that this sets any properties that
     * may have been set previously.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    /**
     * A ViewHolder holds a view for the [RecyclerView] as well as providing additional information
     * to the RecyclerView such as where on the screen it was last drawn during scrolling.
     */
    class ViewHolder private constructor(val binding: ContentFeedListItemBinding) : RecyclerView.ViewHolder(binding.root){
        companion object{
            fun from(parent : ViewGroup) : ViewHolder{
                val inflater = LayoutInflater.from(parent.context)
                val binding = ContentFeedListItemBinding.inflate(inflater, parent, false)
                return ViewHolder(binding)
            }
        }

        //sets all the values for the items for the feed item in xml to that of the feed item data
        fun bind(item: ContentFeedItem, clickListener: ContentFeedItemListener){
            binding.content = item
            binding.clickListener = clickListener
            binding.contentFeedItemTitle.text = item.title
            binding.contentFeedItemPreview.text = item.previewText
            binding.contentFeedItemIcon.setImageResource(item.drawable)
            binding.contentFeedItemDate.text = item.date
            if(item.type == ContentType.PIN) {
                binding.contentFeedItemLocation.text = item.location
            }
            binding.executePendingBindings()
        }
    }
}

/**
 * [ContentFeedItemCallback] extends [DiffUtil] to calculate the changes between content when the list changes
 * and update the list accordingly.
 */
class ContentFeedItemCallback : DiffUtil.ItemCallback<ContentFeedItem>(){
    override fun areItemsTheSame(oldItem: ContentFeedItem, newItem: ContentFeedItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ContentFeedItem, newItem: ContentFeedItem): Boolean {
        // ContentFeedItem is a data class that contains methods for comparing items
        return oldItem == newItem
    }
}

/**
 * [ContentFeedItemListener]
 */
class ContentFeedItemListener(val clickListener: (contentId: Long, contentType: ContentType) -> Unit){
    fun onClick(content: ContentFeedItem) = clickListener(content.id, content.type)
}
