/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.gallery

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.thegravelers.uce.databinding.GalleryFragmentBinding
import com.thegravelers.uce.screens.companion.basefragments.FullscreenFragment
import com.thegravelers.uce.util.extensions.padWithDisplayCutout
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment used to present the user with a gallery of photos taken
 */
@AndroidEntryPoint
class GalleryFragment : FullscreenFragment() {

    private lateinit var binding: GalleryFragmentBinding
    private lateinit var viewPager: ViewPager2
    val sharedViewModel by activityViewModels<SharedGalleryViewModel>()
    private lateinit var args: GalleryFragmentArgs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(arguments != null){
            args = GalleryFragmentArgs.fromBundle(requireArguments())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = GalleryFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = this
        // Initialise the ViewPager and implement a cache of two media items
        viewPager = binding.photoViewPager
        viewPager.apply {
            offscreenPageLimit = 2
            // Adapter class used to present a fragment containing one photo or video as a page
            adapter = GalleryAdapter()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Make sure that the cutout "safe area" avoids the screen notch if any
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // Use extension method to pad "inside" view containing UI using display cutout's bounds
            binding.cutoutSafeArea.padWithDisplayCutout()
        }

        binding.backButton.setOnClickListener {
            navigateBack()
        }

        binding.deleteButton.setOnClickListener {
            deleteImage()
        }

        // Observe the images to submit to the recyclerview
        sharedViewModel.images.observe(viewLifecycleOwner, {
            if(it.size == 0){
                navigateBack()
            }
            // Populate the ViewPager
            val adapter = viewPager.adapter as GalleryAdapter
            adapter.submitList(it)
            adapter.notifyDataSetChanged()
        })
        // Wait for the ViewPager to be initialised before setting the current item
        if(this::args.isInitialized){
            viewPager.post { viewPager.setCurrentItem(args.position, false)}
        }

        // Update the recyclerview on item removed
        sharedViewModel.notifyItemRemoved.observe(viewLifecycleOwner, {
            if (it != null) {
                viewPager.adapter?.notifyItemRemoved(it)
                sharedViewModel.itemRemoved()
            }
        })
    }

    private fun deleteImage() {
        AlertDialog.Builder(requireContext(), android.R.style.Theme_Material_Dialog)
                .setTitle("Confirm")
                .setMessage("Delete current photo?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes") { _, _ ->
                    // Delete current image
                    sharedViewModel.deleteImage(viewPager.currentItem)
                }
                .setNegativeButton("No", null)
                .show()
    }

    private fun navigateBack(){
        findNavController().navigateUp()
    }
}