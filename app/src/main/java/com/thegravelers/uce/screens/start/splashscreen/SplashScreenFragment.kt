/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.splashscreen

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.thegravelers.uce.R
import com.thegravelers.uce.screens.companion.CompanionActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class SplashScreenFragment : Fragment() {

    val viewModel by viewModels<SplashScreenViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewModel.loggedIn.observe(viewLifecycleOwner, {
            if (it) {
                // Check if we are already in a team
                viewModel.checkIfInGroup()
            } else {
                // Navigate to login fragment
                findNavController().navigate(SplashScreenFragmentDirections.actionSplashScreenFragmentToLoginFragment())
            }
        })

        viewModel.inTeam.observe(viewLifecycleOwner, {
            if (it) {
                // Start the main activity (we are logged in and in a team)
                startCompanionActivity()
            } else {
                // Navigate to teamlist fragment
                findNavController().navigate(SplashScreenFragmentDirections.actionSplashScreenFragmentToTeamListFragment())
            }
        })

        viewModel.checkIfUserLoggedIn()

        return inflater.inflate(R.layout.splash_screen_fragment, container, false)
    }

    private fun startCompanionActivity() {
        val intent = Intent(activity, CompanionActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }
}