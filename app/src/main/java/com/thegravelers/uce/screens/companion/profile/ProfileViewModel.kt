/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.thegravelers.uce.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(private val userRepository: UserRepository): ViewModel(){

    private val _navigateToLogin = MutableLiveData<Boolean>()
    val navigateToLogin : LiveData<Boolean> get() = _navigateToLogin

    private val _navigateToMyPins = MutableLiveData<Boolean>()
    val navigateToMyPins : LiveData<Boolean> get() = _navigateToMyPins

    private val _navigateToSavedPins = MutableLiveData<Boolean>()
    val navigateToSavedPins : LiveData<Boolean> get() = _navigateToSavedPins

    //functions used for navigating to the Login fragment
    fun navigateToLogin(){
        _navigateToLogin.value = true
    }

    fun doneNavigatingToLogin(){
        _navigateToLogin.value = false
    }

    //functions used for navigating to the MyPins fragment
    fun navigateToMyPins(){
        _navigateToMyPins.value = true
    }

    fun doneNavigatingToMyPins(){
        _navigateToMyPins.value = false
    }

    //functions used for navigating to the MyPins fragment
    fun navigateToSavedPins(){
        _navigateToSavedPins.value = true
    }

    fun doneNavigatingToSavedPins(){
        _navigateToSavedPins.value = false
    }
    fun logout(){
        runBlocking{userRepository.logout()}
        navigateToLogin()
    }
}