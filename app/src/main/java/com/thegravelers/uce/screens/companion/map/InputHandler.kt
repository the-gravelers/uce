/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import com.mapbox.mapboxsdk.geometry.LatLng
import com.thegravelers.uce.util.MapConstants.EarthsRadius
import kotlin.math.ln
import kotlin.math.tan

/**
 * Handles the input by the user to obtain a description about the MapBox map
 * @param[polygonDB] the database of polygons
 * @param[gridData] the list of which polygons are contained per image index on the deepest layer
 */
class InputHandler(private val polygonDB: HashMap<String, Pair<String, Polygon>>, private val gridData: List<List<String>>) {

    /**
     * Public function to return the Codi_Cas (ground type) of the clicked polygon
     * @param[latLng] the coordinates of an input
     * @param[index] the image index on the deepest layer
     */
    fun handleInput(latLng: LatLng, index: Int) : String {
        val convertedLatLng = convertToWebMercator(latLng)
        return findClickedPolygon(convertedLatLng, index)
    }

    /**
     * Convert the latLng user input to Web Mercator the same way as in the preprocessor
     * @param[latLng] the user input to convert
     */
    private fun convertToWebMercator(latLng: LatLng):Point{
        val x: Double = latLng.longitude * (2 * Math.PI * EarthsRadius / 2) / 180
        var y: Double = ln(tan((90 + latLng.latitude) * Math.PI / 360)) / (Math.PI / 180)
        y = y * (2 * Math.PI * EarthsRadius / 2) / 180
        return Point.fromLngLat(x, y)
    }

    /**
     * Search all the polygons in the given grid tile to see how many times they intersect with
     * a straight line drawn from an input to the right. (PiP algorithm)
     * @param[latLng] the coordinates of an input
     * @param[index] the image index on the deepest layer
     */
    private fun findClickedPolygon(latLng: Point, index: Int):String{
        for (polygonID in gridData[index]) {
            polygonDB[polygonID]?.let {
                if (intersects(latLng, it.second)) {
                    return it.first
                }
            }
        }
        return ""
    }

    /**
     * Goes over all boundaries in a polygon to see the amount of intersections, even or odd
     * @param[latLng] the coordinates of an input
     * @param[polygon] the current polygon to be checked for intersections
     */
    private fun intersects(latLng: Point, polygon: Polygon):Boolean {
        var oddNodes = false
        for (points in polygon.coordinates()){
            if(oddInBoundary(latLng, points)){
                oddNodes = !oddNodes
            }
        }
        return oddNodes
    }

    /**
     * Calculation from worked out Point in Polygon algorithm http://alienryderflex.com/polygon/
     * for each point in the given boundary
     * @param[latLng] the coordinates of an input
     * @param[points] a boundary within a polygon to check for intersections
     */
    private fun oddInBoundary(latLng: Point, points: List<Point>):Boolean{
        var oddNodes = false
        var prev = points.first()
        for (point in points) {

            if (point == prev) {
                continue
            }
            if ((point.latitude() < latLng.latitude() && prev.latitude() >= latLng.latitude()
                            || prev.latitude() < latLng.latitude() && point.latitude() >= latLng.latitude())
                    && (point.longitude() <= latLng.longitude() || prev.longitude() <= latLng.longitude())) {
                if (point.longitude() + (latLng.latitude() - point.latitude()) /
                        (prev.latitude() - point.latitude()) * (prev.longitude() - point.longitude()) < latLng.longitude()) {
                    oddNodes = !oddNodes
                }
            }
            prev = point
        }
        return oddNodes
    }
}