/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.pin

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mapbox.mapboxsdk.geometry.LatLng
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.PinContent
import com.thegravelers.uce.di.CoroutineModule
import com.thegravelers.uce.di.DispatcherWrapper
import com.thegravelers.uce.repository.ContentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PinViewModel @Inject constructor(
        @CoroutineModule.IoDispatcher private val dispatcher: CoroutineDispatcher,
        private val contentRepository: ContentRepository
) : ViewModel() {
    /**
     * Used for databinding to toggle editablity.
     */
    private val _isEditable = MutableLiveData(false)
    val isEditable: LiveData<Boolean> get() = _isEditable

    private val _fromPinFeed = MutableLiveData<Boolean>()
    val fromPinFeed: LiveData<Boolean> get() = _fromPinFeed

    /**
     * Pin editability
     */
    private val _revert = MutableLiveData(false)
    val revert: LiveData<Boolean> get() = _revert

    private val _pinCreated = MutableLiveData(false)
    val pinCreated: LiveData<Boolean> get() = _pinCreated

    private val _navigateToGalleryActivity = MutableLiveData(false)
    val navigateToGalleryActivity: LiveData<Boolean> get() = _navigateToGalleryActivity

    // Pin
    private var _pinLocation: LatLng? = null

    private val _pinInFocus = MutableLiveData<Pin?>()
    val pinInFocus: LiveData<Pin?> get() = _pinInFocus

    private var _newPinContent = MutableLiveData<List<PinContent>>()
    val newPinContent: LiveData<List<PinContent>> get() = _newPinContent

    fun getCurrentPin(): Pin? = _pinInFocus.value

    /**
     * Retrieves a [Pin] from the repository
     * @param[pinId] The pin id
     */
    fun retrievePin(pinId: Long) {
        viewModelScope.launch(dispatcher) {
            _pinInFocus.postValue(contentRepository.getPin(pinId))
        }
    }

    /**
     * Retrieves the pin content asynchronously
     */
    fun getPinContent(pinId: Long) {
        viewModelScope.launch(dispatcher) {
            _newPinContent.postValue(contentRepository.getPinContent(pinId))
        }
    }

    fun clearPinContent() {
        _newPinContent.value = listOf()
    }

    /**
     * Toggle the editability of a pin
     */
    fun enableEditability() {
        _isEditable.value = true
    }

    /**
     * Disable the editability of a pin
     */
    fun disableEditability() {
        if (_isEditable.value == true) {
            _isEditable.postValue(false)
        }
    }

    /**
     * Enable the property that let's us know the [PinFragment] came from the pin feed
     */
    fun enableFromPinFeed() {
        _fromPinFeed.postValue(true)
    }

    /**
     * Let the viewmodel know that we have left, or returned to, the activity
     */
    fun navigateToGalleryActivity(value: Boolean) {
        _navigateToGalleryActivity.value = value
    }

    /**
     * Clear the current pin from the viewmodel
     */
    fun clearPinInFocus() {
        _pinInFocus.postValue(null)
    }

    /**
     * Set the location for a new pin
     * @param[latLng] The position of the location
     */
    fun setPinLocation(latLng: LatLng) {
        _pinLocation = latLng
    }

    /**
     * Creates a new pin with the current user input using the repository
     * @param[title] The text from the title field
     * @param[images] The current list of images
     * @param[notes] The text from the notes field
     */
    fun createPin(title: String, notes: String, images: List<Uri>?) {
        viewModelScope.launch(dispatcher) {
            _pinLocation?.let { latLng ->
                val pin = Pin(lat = latLng.latitude, lon = latLng.longitude, title = title, text = notes)
                val contents: List<PinContent>? = images?.let {
                    it.withIndex().map { (index, image) ->
                        PinContent(uri = image.toString(), contentIndex = index)
                    }
                }
                contentRepository.insertNewPin(pin, contents)
                _pinCreated.postValue(true)
            }
        }
    }

    /**
     * Updates a existing pin with the current user input using the repository
     * @param[pin] The pin to be updated along with its old content
     * @param[title] The text from the title field
     * @param[notes] The text from the notes field
     * @param[images] The current list of images
     */
    fun updatePin(pin: Pin, title: String, notes: String, images: List<Uri>?) {
        pin.title = title
        pin.text = notes

        viewModelScope.launch(dispatcher) {
            contentRepository.updatePin(pin, images)
            _pinInFocus.postValue(contentRepository.getPin(pin.id))
        }
    }

    /**
     * Call if pin fragment reverted
     */
    fun performedRevert() {
        _revert.value = false
    }

    /**
     * Call if the pin fragment should revert
     */
    fun performRevert() {
        _revert.value = true
    }

    fun savePinInFocus() {
        viewModelScope.launch(dispatcher) {
            _pinInFocus.value?.let { pin ->
                contentRepository.savePin(pin.id)
            }
        }
    }

    fun unsavePinInFocus() {
        viewModelScope.launch(dispatcher) {
            _pinInFocus.value?.let { pin ->
                contentRepository.unsavePin(pin.id)
            }
        }
    }
}
