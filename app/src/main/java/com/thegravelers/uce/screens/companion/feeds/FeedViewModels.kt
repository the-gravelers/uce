/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.thegravelers.uce.repository.ContentFeedItem
import com.thegravelers.uce.repository.ContentRepository
import com.thegravelers.uce.repository.ContentType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class PinFeedViewModel @Inject constructor(repository : ContentRepository) : ContentFeedViewModel(repository){
    override fun updateFeedItemList(): LiveData<List<ContentFeedItem>> {
        return runBlocking(Dispatchers.IO) {
            contentRepository.getAllPinsFeed().asLiveData()
        }
    }

    fun refreshPins(){
        viewModelScope.launch(Dispatchers.IO){
            contentRepository.refreshPins()
        }
    }
}

@HiltViewModel
class SearchFeedViewModel @Inject constructor(repository : ContentRepository) : ContentFeedViewModel(repository){

    private val _navigateWithType = MutableLiveData<ContentType?>()
    val navigateWithType: LiveData<ContentType?> get() = _navigateWithType

    override fun updateFeedItemList(): LiveData<List<ContentFeedItem>> {
        return runBlocking(Dispatchers.IO) {
            contentRepository.getAllPinsFeed().asLiveData()
        }
    }

    fun onContentFeedItemClicked(id: Long, contentType: ContentType){
        _navigateWithType.value = contentType
        _navigateToPinFragment.value = id
    }
}

@HiltViewModel
class BookFeedViewModel @Inject constructor(repository : ContentRepository) : ContentFeedViewModel(repository){
    override fun updateFeedItemList(): LiveData<List<ContentFeedItem>> {
        return runBlocking(Dispatchers.IO) {
            contentRepository.getAllBooksFeed().asLiveData()
        }
    }

    /**
     * Downloads all books that we do not have in a coroutine
     */
    fun downloadBooks(){
        viewModelScope.launch(Dispatchers.IO){
            contentRepository.downloadAllBooks()
        }
    }
}

@HiltViewModel
class MyPinsFeedViewModel @Inject constructor(repository : ContentRepository) : ContentFeedViewModel(repository){
    override fun updateFeedItemList(): LiveData<List<ContentFeedItem>> {
        return runBlocking(Dispatchers.IO) {
            contentRepository.getMyPinsFeed().asLiveData()
        }
    }

    fun deleteFeedItem(index: Long?) {
        index?.let{
            viewModelScope.launch(Dispatchers.IO) {
                contentRepository.deleteUserPin(it)
            }
        }
    }
}

@HiltViewModel
class SavedPinsFeedViewModel @Inject constructor(repository : ContentRepository) : ContentFeedViewModel(repository){
    override fun updateFeedItemList(): LiveData<List<ContentFeedItem>> {
        return runBlocking(Dispatchers.IO) {
            contentRepository.getSavedPinsFeed().asLiveData()
        }
    }
}
