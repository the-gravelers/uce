/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.book

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thegravelers.uce.database.entity.BookWithContents
import com.thegravelers.uce.repository.ContentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(private val contentRepository: ContentRepository): ViewModel() {
    private val _bookInFocus = MutableLiveData<BookWithContents>()
    val bookInFocus: LiveData<BookWithContents> get() = _bookInFocus

    fun setBookInFocus(id: Long){
        viewModelScope.launch {
            _bookInFocus.value = contentRepository.getBookWithContent(id)
        }
    }

}