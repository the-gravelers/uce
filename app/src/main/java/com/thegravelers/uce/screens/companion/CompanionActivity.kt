/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion

import android.app.Activity
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.thegravelers.uce.R
import com.thegravelers.uce.databinding.CompanionActivityBinding
import com.thegravelers.uce.screens.companion.camera.CameraFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CompanionActivity : AppCompatActivity() {

    private lateinit var binding: CompanionActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.companion_activity)

        val navController = findNavController(R.id.nav_host_fragment)
        binding.bottomNavigationView.setupWithNavController(navController)
    }

    fun showBottomNav() {
        binding.bottomNavigationView.visibility = VISIBLE
    }

    fun hideBottomNav() {
        binding.bottomNavigationView.visibility = GONE
    }

    fun hideKeyboard() {
        this.currentFocus?.let { view ->
            val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onBackPressed() {
        if (notifyFragments()) super.onBackPressed()
    }

    /**
     * A [Fragment] that has the interface [OnBackPressed] implemented can be told when the back button is pressed
     * before the back press goes through
     * Currently used in the [CameraFragment] to work around visual rendering issues (showing the map in between navigation)
     */
    private fun notifyFragments(): Boolean {
        val fragments: List<Fragment> = supportFragmentManager.fragments
        var callSuper = true
        for (f in fragments) {
            if (f is OnBackPressed) {
                callSuper = f.onBackPressed()
            }
            if (f is NavHostFragment){
                val navFragments = f.childFragmentManager.fragments
                for (navF in navFragments){
                    if(navF is OnBackPressed) {
                        callSuper = navF.onBackPressed()
                    }
                }
            }
        }
        return callSuper
    }
}
