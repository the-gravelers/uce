package com.thegravelers.uce.screens.start.teamcode

import androidx.lifecycle.*
import com.thegravelers.uce.database.dao.UserDao
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.repository.UserRepository
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.internal.notify
import javax.inject.Inject

@HiltViewModel
class StartingTeamViewModel @Inject constructor(private val userRepository: UserRepository,
                                                private val sessionManager: SessionManager) : ViewModel() {

    private var _teamList : MutableLiveData<UserGroupWithUsers> = MutableLiveData()
    val teamList: LiveData<UserGroupWithUsers> get() = _teamList

    private val _backToTeamList = MutableLiveData(false)
    val backToTeamList: LiveData<Boolean> get() = _backToTeamList

    fun getTeam() {
        val id = sessionManager.fetchUserId()
        viewModelScope.launch(Dispatchers.IO) {
            val groupId = userRepository.getTeamIdFromUser()
            groupId?.let{
                _teamList.postValue(userRepository.getTeam(it))
            }
        }
    }

    /**
     * Tells the [UserRepository] to leave the current team of the user.
     * Then updates the [backToTeamList] livedata to let the fragment know we are ready to go back
     */
    fun leaveTeam() {
        viewModelScope.launch {
            userRepository.leaveTeam()
            _backToTeamList.value = true
        }
    }

    /**
     * Should be called by the fragment after we have navigated back to the teamlist fragment.
     */
    fun leftTeam(){
        _backToTeamList.value = false
    }
}