package com.thegravelers.uce.screens.start.teamcode

import androidx.lifecycle.*
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.repository.UserRepository
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class StartTeamListViewModel @Inject constructor(
        private val userRepository: UserRepository
        ) : ViewModel() {
    val teamsList = userRepository.getTeams().asLiveData()

    private val _joinTeamSuccessful = MutableLiveData<Boolean>()
    val joinTeamSuccessful: LiveData<Boolean> get() = _joinTeamSuccessful

    private val _stopRefreshingTeams = MutableLiveData<Boolean>()
    val stopRefreshingTeams: LiveData<Boolean> get() = _stopRefreshingTeams

    /**
     * gives the current team from the repository
     */
    fun refreshTeamList() {
        _stopRefreshingTeams.value = false
        viewModelScope.launch {
            userRepository.refreshTeams()
            _stopRefreshingTeams.value = true
        }
    }

    /**
     * when navigated to the startTeamFragment the navigation is done and joinTeamSuccessful is set on false
     */
    fun doneNavigatingToStartTeamFragment() {
        _joinTeamSuccessful.value = false
    }

    /**
     * joinTeam returns True when successfully joining a team
     */
    fun joinTeam(teamId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            _joinTeamSuccessful.postValue(userRepository.joinTeam(teamId))
        }
    }
}