/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.pin

import android.os.Bundle
import android.transition.Transition
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.thegravelers.uce.R
import com.thegravelers.uce.databinding.FragmentPagerBinding
import com.thegravelers.uce.screens.companion.gallery.GalleryAdapter
import com.thegravelers.uce.screens.companion.gallery.SharedGalleryViewModel

class ImagePagerFragment(private val position: Int): Fragment() {

    private lateinit var viewPager: ViewPager2
    private lateinit var binding: FragmentPagerBinding
    private val galleryViewModel by activityViewModels<SharedGalleryViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        postponeEnterTransition()
        val transition: Transition = TransitionInflater.from(context)
                .inflateTransition(R.transition.grid_exit_transition)
        exitTransition = transition
        val entTransition: Transition = TransitionInflater.from(context)
                .inflateTransition(R.transition.image_shared_element_transition)
        enterTransition = entTransition
        binding = FragmentPagerBinding.inflate(inflater)
        viewPager = binding.viewPager
        viewPager.apply {
            offscreenPageLimit = 2
            // Adapter class used to present a fragment containing one photo or video as a page
            adapter = GalleryAdapter()
        }
        galleryViewModel.images.observe(viewLifecycleOwner, { images ->
            val adapter = (viewPager.adapter as GalleryAdapter)
            adapter.submitList(images)
            adapter.notifyDataSetChanged()
        })

        return viewPager

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewPager.post {
            viewPager.setCurrentItem(position, false)
            startPostponedEnterTransition()
        }
    }}