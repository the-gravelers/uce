/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.pluginscalebar.ScaleBarOptions
import com.mapbox.pluginscalebar.ScaleBarPlugin
import com.thegravelers.uce.CompanionNavigationDirections
import com.thegravelers.uce.R
import com.thegravelers.uce.databinding.MapFragmentBinding
import com.thegravelers.uce.screens.companion.BottomSheetContainer
import com.thegravelers.uce.screens.companion.OnBackPressed
import com.thegravelers.uce.screens.companion.gallery.GalleryAdapter
import com.thegravelers.uce.screens.companion.pin.PinFragment
import com.thegravelers.uce.screens.companion.pin.PinViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

/**
 * The [MapFragment] subclass.
 */
@AndroidEntryPoint
class MapFragment : Fragment(), OnMapReadyCallback, MultiplePermissionsListener, OnBackPressed {
    companion object {
        const val TAG = "BOTTOM_SHEET_FRAGMENT"
    }

    private lateinit var mapBinding: MapFragmentBinding
    private lateinit var mapView: MapView
    val mapViewModel by activityViewModels<MapViewModel>()
    private lateinit var map: MapboxMap

    lateinit var bottomSheetBehavior: BottomSheetContainer<FragmentContainerView>
        private set
    private lateinit var pinFragment: PinFragment
    private val pinViewModel by activityViewModels<PinViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate map
        mapBinding = MapFragmentBinding.inflate(inflater)
        mapBinding.mapViewModel = mapViewModel
        mapBinding.lifecycleOwner = viewLifecycleOwner

        mapView = mapBinding.mapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        // Setup observers
        mapViewModel.groundTypeId.observe(viewLifecycleOwner, {
            findNavController().navigate(CompanionNavigationDirections.actionGlobalGeologicInfoFragment(it))
        })

        mapViewModel.isMapFocused.observe(viewLifecycleOwner, {
            mapBinding.mapConstraintLayout.isMapFocused = it
            mapBinding.mapView.visibility = if (it) View.VISIBLE else View.INVISIBLE
            if (!it && findNavController().currentDestination?.id != R.id.cameraFragment) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                childFragmentManager.popBackStack()
            }
        })

        // Add pin fragment to bottom sheet
        childFragmentManager.beginTransaction()
                .add(R.id.map_bottom_sheet_container, PinFragment(R.id.map_bottom_sheet_container), TAG)
                .commitNow()

        return mapBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBottomSheetBehavior()
    }

    private fun setupBottomSheetBehavior() {
        // Init bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(mapBinding.mapBottomSheetContainer) as BottomSheetContainer<FragmentContainerView>
        bottomSheetBehavior.isHideable = true
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.halfExpandedRatio = 0.3f
        val screenSize = Point()
        this.activity?.display?.getRealSize(screenSize)
        bottomSheetBehavior.peekHeight = (screenSize.y * 0.34f).toInt()
    }

    /**
     * Call when the getMapAsync is finished. Starts listeners and calls the [MapViewModel].
     * @param[mapboxMap] The [MapboxMap] instance from the Mapbox servers
     */
    override fun onMapReady(mapboxMap: MapboxMap) {
        map = mapboxMap
        map.setStyle(Style.OUTDOORS) { style ->
            Dexter.withContext(requireContext())
                    .withPermissions(
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(this)
                    .check()

            val symbolManager = SymbolManager(mapView, map, style)

            // Set listeners for markers
            symbolManager.addClickListener { symbol ->
                mapViewModel.onMarkerClick(symbol)
            }

            mapViewModel.onMapReady(map, symbolManager)

            map.addOnCameraIdleListener {
                mapViewModel.onMapIdle()
            }
            map.addOnMapClickListener {
                mapViewModel.onMapClick(it)
                true
            }
            map.addOnMapLongClickListener { latLng ->
                mapViewModel.onMapLongClick(latLng)
                true
            }

            //setting up and displaying the scale bar
            map.uiSettings.logoGravity = 2
            map.uiSettings.attributionGravity = 2

            val mapHeight = mapBinding.mapView.height.toFloat()
            val scaleBarPlugin = ScaleBarPlugin(mapView, mapboxMap)
            val scaleBarOptions = ScaleBarOptions(requireContext())
            scaleBarOptions
                    .setTextColor(R.color.textButtonColor)
                    .setMarginTop(mapHeight - (mapHeight * 0.05f))
                    .setMetricUnit(true)
            scaleBarPlugin.create(scaleBarOptions)
        }
        mapViewModel.pinLiveData.observe(viewLifecycleOwner, { pinList ->
            mapViewModel.updatePinsOnMap(pinList)
        })
    }

    // This function is called to see if permissions are accepted or denied
    // And does action depending on if it is denied or accepted
    @SuppressLint("MissingPermission")
    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
        // Permission is accepted
        if (report?.areAllPermissionsGranted() == true) {
            val customLocationComponentOptions = LocationComponentOptions.builder(requireContext())
                    .accuracyColor(ContextCompat.getColor(requireContext(), R.color.buttonColor))
                    .build()

            val locationComponentActivationOptions = LocationComponentActivationOptions
                    .builder(requireContext(), requireNotNull(map.style))
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()

            // Get an instance of the LocationComponent and then adjust its settings
            map.locationComponent.apply {
                // Activate the LocationComponent with options
                activateLocationComponent(locationComponentActivationOptions)
                // Enable to make the LocationComponent visible
                isLocationComponentEnabled = true
                // Set the LocationComponent's render mode
                renderMode = RenderMode.COMPASS
            }
        }
        // When permission is denied
        else {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Location permission")
            builder.setMessage("The location is needed to automatically put pins down on the map with your specific location else you have to manually place pins down which will give a less accurate placement")
            builder.setIcon(R.drawable.ic_map)
            builder.setNeutralButton("okay") { dialog, _ ->
                dialog.cancel()
            }
            builder.show()
        }

    }

    // Keeps asking for permission on till it is accepted or user says never to show it again
    override fun onPermissionRationaleShouldBeShown(p0: MutableList<PermissionRequest>?, token: PermissionToken?) {
        token?.continuePermissionRequest()
    }

    override fun onStart() {
        super.onStart()
        populateBottomSheet()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapBinding.mapConstraintLayout.removeView(mapView)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onBackPressed(): Boolean {
        if (mapViewModel.isMapFocused.value == false) {
            return true
        }
        return when (bottomSheetBehavior.state) {
            BottomSheetBehavior.STATE_HIDDEN -> {
                true
            }
            BottomSheetBehavior.STATE_COLLAPSED -> {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                false
            }
            else -> {
                if (pinViewModel.getCurrentPin() == null) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                } else {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                }
                childFragmentManager.popBackStack()
                false
            }
        }
    }

    /**
     * Sets up the bottomsheet with the [PinFragment]
     * Adds callbacks to handle UI elements when the user slides the bottomsheet up and down
     */
    private fun populateBottomSheet() {
        if (this::pinFragment.isInitialized) {
            return
        }
        pinFragment = childFragmentManager.findFragmentByTag(TAG) as PinFragment
        val pinFragmentBinding = pinFragment.binding

        if (!this::bottomSheetBehavior.isInitialized) {
            Timber.e("bottomSheetbehavior not initialized, maybe forgot to call setupBottomSheetBehavior")
        }

        // Callback for slide animation
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // If this has a value that means we have left the activity temporarily
                if (mapViewModel.bottomSheetState.value == null) {
                    val position = if (slideOffset <= 0f) 0f else slideOffset
                    val maxHeight = (pinFragmentBinding.pinLinearLayout.measuredHeight * 0.4).toInt()
                    val minHeight = (maxHeight * 0.4).toInt()
                    val newHeight = ((position) * (maxHeight - minHeight) + minHeight).toInt()
                    val maxMargin = pinFragmentBinding.topButtonLayout.measuredHeight
                    val minMargin = maxMargin / 4
                    val params = pinFragmentBinding.imageCard.layoutParams as ViewGroup.MarginLayoutParams
                    params.updateMargins(top = minMargin + ((maxMargin - minMargin) * (position)).toInt())
                    pinFragmentBinding.photoViewPager.updateLayoutParams { height = newHeight }
                }
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // If this has a value that means we have left the activity temporarily
                if (mapViewModel.bottomSheetState.value == null) {
                    if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        (pinFragmentBinding.photoViewPager.adapter as GalleryAdapter)
                                .isClickable = true
                        (pinFragmentBinding.imageCard.layoutParams as ViewGroup.MarginLayoutParams)
                                .updateMargins(top = 0)
                        pinFragmentBinding.topButtonLayout.visibility = View.VISIBLE
                        if (pinViewModel.isEditable.value != true && pinViewModel.getCurrentPin() == null) {
                            pinFragmentBinding.topButtonLayout.performClick()
                        }
                    }
                    else {
                        if((newState == BottomSheetBehavior.STATE_HIDDEN || newState == BottomSheetBehavior.STATE_COLLAPSED) && pinViewModel.isEditable.value == true) {
                            pinViewModel.performRevert()
                        }
                        if(newState == BottomSheetBehavior.STATE_HIDDEN){
                            pinViewModel.clearPinInFocus()
                        }
                        (pinFragmentBinding.photoViewPager.adapter as GalleryAdapter).isClickable = false
                        if (pinFragmentBinding.topButtonLayout.visibility == View.VISIBLE) {
                            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                                (pinFragmentBinding.imageCard.layoutParams as ViewGroup.MarginLayoutParams)
                                        .updateMargins(top = pinFragmentBinding.topButtonLayout.measuredHeight)
                            }
                            pinFragmentBinding.topButtonLayout.visibility = View.GONE
                        }
                    }

                }
                // Here we have returned to our activity
                else {
                    if (newState == mapViewModel.bottomSheetState.value) {
                        if (mapViewModel.isMapFocused.value == true) {
                            (pinFragmentBinding.imageCard.layoutParams as ViewGroup.MarginLayoutParams)
                                    .updateMargins(top = 0)
                        }
                        mapViewModel.setBottomSheetState(null)
                    }
                }
            }
        })

        setupBottomSheetCommunication()
        mapViewModel.refreshPins()
    }

    private fun setupBottomSheetCommunication() {
        mapViewModel.pinInFocus.observe(viewLifecycleOwner, { focusedPin ->
            when (focusedPin) {
                is Long -> {
                    pinFragment.setPinFocused(focusedPin)
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                }
                is LatLng -> {
                    pinFragment.beginPinCreation(focusedPin)
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
                null -> {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                }
            }
        })

        pinViewModel.navigateToGalleryActivity.observe(viewLifecycleOwner, {
            if (it) {
                mapViewModel.setBottomSheetState(bottomSheetBehavior.state)
            } else {
                val state = mapViewModel.bottomSheetState.value
                if (state != null) {
                    bottomSheetBehavior.state = state
                }
            }
        })

        pinViewModel.isEditable.observe(viewLifecycleOwner, {
            bottomSheetBehavior.shouldInterceptTouchEvent = it
        })

        pinViewModel.pinCreated.observe(viewLifecycleOwner, {
            if (it) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })

    }
}
