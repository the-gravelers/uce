/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior

/**
 * Custom ConstraintLayout to disable user interaction when out of focus
 */
class MapConstraintLayout : ConstraintLayout {
    constructor(context: Context?) : super(context!!)
    constructor(context: Context?, attributeSet: AttributeSet?) : super(context!!, attributeSet)
    constructor(context: Context?, attributeSet: AttributeSet?, i: Int) : super(context!!, attributeSet, i)

    var isMapFocused = false

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (isMapFocused)
            super.dispatchTouchEvent(ev)
        else
            false
    }
}

class BottomSheetContainer<V : View> : BottomSheetBehavior<V> {
    constructor() : super()
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    var shouldInterceptTouchEvent: Boolean = false

    override fun onTouchEvent(parent: CoordinatorLayout, child: V, event: MotionEvent): Boolean {
        return if ((state == STATE_EXPANDED && (child as ViewGroup).childCount > 1) || shouldInterceptTouchEvent) {
            false
        }
        else {
            super.onTouchEvent(parent, child, event)
        }
    }

    override fun onInterceptTouchEvent(parent: CoordinatorLayout, child: V, event: MotionEvent): Boolean {
        return if ((state == STATE_EXPANDED && (child as ViewGroup).childCount > 1) || shouldInterceptTouchEvent) {
            false
        }
        else {
            super.onInterceptTouchEvent(parent, child, event)
        }
    }
}