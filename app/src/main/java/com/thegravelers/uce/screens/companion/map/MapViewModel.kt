/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import android.annotation.SuppressLint
import android.content.res.Resources
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.*
import com.google.gson.JsonPrimitive
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.utils.BitmapUtils
import com.thegravelers.uce.R
import com.thegravelers.uce.database.entity.*
import com.thegravelers.uce.repository.ContentRepository
import com.thegravelers.uce.util.MapConstants
import com.thegravelers.uce.util.MapConstants.GROUND_TYPE_ICON
import com.thegravelers.uce.util.MapConstants.PIN_ICON
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MapViewModel @Inject constructor(private val contentRepository: ContentRepository, val resources: Resources) : ViewModel() {

    var isInitialized: Boolean = false
        private set

    val pinLiveData: LiveData<List<Pin>> = contentRepository.getAllPins().asLiveData()

    private val _groundTypeId = MutableLiveData<String>()
    private val _isMapFocused = MutableLiveData<Boolean>()
    private val _map = MutableLiveData<MapboxMap>()
    private val _pinInFocus = MutableLiveData<Any>()
    private val _bottomSheetState = MutableLiveData<Int>()
    val groundTypeId: LiveData<String> get() = _groundTypeId
    val isMapFocused: LiveData<Boolean> get() = _isMapFocused
    val map: LiveData<MapboxMap> get() = _map
    val pinInFocus: LiveData<Any> get() = _pinInFocus
    val bottomSheetState: LiveData<Int> get() = _bottomSheetState

    private var hidden = false

    @Inject
    lateinit var overlayManagerFactory: OverlayManagerFactory
    lateinit var overlayManager: OverlayManager
    @Inject
    lateinit var parserFactory: ParserFactory
    private lateinit var inputHandler: InputHandler
    private lateinit var symbolManager: SymbolManager
    val createdGroundTypeSymbolList: MutableList<Symbol> = mutableListOf()

    @SuppressLint("NullSafeMutableLiveData")
    fun setBottomSheetState(value: Int?){
        _bottomSheetState.value = value
    }

    /**
     * The map fragment is not part of the bottom sheet and is always alive.
     * It is only visible when navigated to the blank fragment, then this function should be called.
     */
    @SuppressLint("NullSafeMutableLiveData")
    fun navigateToMap() {
        _isMapFocused.value = true
    }
    /**
     * The map fragment is not part of the bottom sheet and is always alive.
     * It is only visible when navigated to the blank fragment.
     * When navigating away, this function should be called.
     */
    fun navigateFromMap() {
        _isMapFocused.value = false
    }

    /**
     * Toggles the [OverlayManager] to either turn everything on or off.
     */
    fun toggleMapLayer() {
        Timber.i("User toggled the Map Layer")
        hidden = !hidden
        if(hidden) {
            _map.value?.style?.let {
                overlayManager.hideLayers(it, true)
            }
        }
        else {
            _map.value?.let {
                val bounds = OverlayQuad(
                        it.projection.visibleRegion.latLngBounds.southWest,
                        it.projection.visibleRegion.latLngBounds.northEast)
                val zoom = it.cameraPosition.zoom.toInt()
                overlayManager.updateMap(zoom, bounds)
            }
        }
    }

    /**
     * Animates the camera of the current [MapboxMap] so that it moves towards the user's position.
     */
    fun centerLocation() {
        _map.value?.let { mapbox ->
            if(!mapbox.locationComponent.isLocationComponentActivated)
                return
            val location = mapbox.locationComponent.lastKnownLocation
            location?.let {
                mapbox.animateCamera(CameraUpdateFactory
                        .newLatLngZoom(LatLng(
                                location.latitude, location.longitude),
                                mapbox.cameraPosition.zoom))
            }
        }
    }

    /**
     * Creates a pin on the user's location.
     * Requires that the user enabled location permissions.
     */
    fun createPinOnLocation(){
        _map.value?.let { mapbox ->
            if(!mapbox.locationComponent.isLocationComponentActivated)
                return
            val location = mapbox.locationComponent.lastKnownLocation
            location?.let {
                _pinInFocus.value = LatLng(location.latitude, location.longitude)
            }
        }
    }

    /**
     * Needs to be called when a [MapFragment] creates a new MapView.
     * It either initializes a [MapboxMap] or restores it to it's previous state.
     * @param[newMap] The new instance of the displayed base map.
     * @param[sManager] An instance of [SymbolManager], which handles the placement of markers.
     */
    fun onMapReady(newMap: MapboxMap, sManager: SymbolManager) {
        symbolManager = sManager

        // If we have an old map, reuse it's camera for the new map
        // If we do not have an old map, set our camera position to a hardcoded position
        // TODO: Change this hardcoded position to read from metadata file

        _map.value?.let {
            newMap.moveCamera(CameraUpdateFactory.newCameraPosition(it.cameraPosition))
        } ?: newMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(41.0, 0.0)))

        initialiseMap(newMap)

    }

    /**
     * Handles a normal click on a [MapboxMap] object.
     * Should remove any visible PinPopups or Map info overlays from previous MapLongClicks.
     * @param[pos] The [LatLng] position of the click on the [MapboxMap] object.
     */
    @SuppressLint("NullSafeMutableLiveData")
    fun onMapClick(pos: LatLng) {
        // TODO: implement it so that we save the PinID not the boolean
        Timber.i("Long click on MapboxMap registered at $pos")
        _pinInFocus.value?.let {
            _pinInFocus.value = null
            return
        }
        val index = overlayManager.gridIndexFromInput(pos)
        index?.let {
            createGroundTypeSymbol(inputHandler.handleInput(pos, it), pos)
        }
    }

    private fun createGroundTypeSymbol(groundType: String, latLng: LatLng){
        symbolManager.delete(createdGroundTypeSymbolList)
        createdGroundTypeSymbolList.add(symbolManager.create(SymbolOptions()
                .withIconImage(GROUND_TYPE_ICON)
                .withLatLng(latLng)
                .withTextField(groundType)
                .withIconSize(.25f)
                .withIconOffset(arrayOf(0f,-50f))
                .withTextOffset(arrayOf(0f,-1f))
                .withData(JsonPrimitive(groundType))))

    }

    /**
     * Handles a long click on a [MapboxMap] object.
     * Starts the process of creating a pin with the information from the geological map.
     * @param[pos] The [LatLng] position of the click on the [MapboxMap] object.
     */
    @SuppressLint("NullSafeMutableLiveData")
    fun onMapLongClick(pos: LatLng) {
        _pinInFocus.value = pos
    }

    /**
     * Handles a click on a [Symbol] object.
     * Starts the process of creating a popup with the information of the Marker.
     * Animates the camera so that the marker is in the centre of the view.
     * @param[marker] The clicked [Symbol] object in the map
     */
    fun onMarkerClick(marker: Symbol): Boolean {
        requireNotNull(map.value).animateCamera(CameraUpdateFactory
                .newLatLngZoom(marker.latLng, requireNotNull(map.value).cameraPosition.zoom))
        marker.data.let {
            if (it != null && it.isJsonPrimitive) {
                if ((it as JsonPrimitive).isNumber) {
                    _pinInFocus.value = it.asLong
                }
                if (it.isString){
                    _groundTypeId.value = it.asString
                    symbolManager.delete(marker)
                }
            }
        }
        return true
    }

    /**
     * Asks the [OverlayManager] if there are any new images to be loaded.
     * Updates the map by toggling visibility on the correct images.
     */
    fun onMapIdle() {
        if (hidden || !this::overlayManager.isInitialized)
            return
        _map.value?.let {
            val bounds = OverlayQuad(
                    it.projection.visibleRegion.latLngBounds.southWest,
                    it.projection.visibleRegion.latLngBounds.northEast)
            val zoom = it.cameraPosition.zoom.toInt()
            overlayManager.updateMap(zoom, bounds)
        }
    }

    /**
     * Initialises the map and creating needed objects
     * @param[newMap] the map to be initialised
     */
    private fun initialiseMap(newMap: MapboxMap) {
        val parser: Parser by lazy {
            parserFactory.create("data")
        }

        overlayManager = overlayManagerFactory.create("", parser.mapBounds, parser.maxLayers)
        newMap.style?.let {
            addImagesToStyle(it)
        }

        newMap.style?.let {
            overlayManager.initMap(it)
        }

        inputHandler = InputHandler(parser.polygonDB, parser.gridData)

        newMap.apply {
            uiSettings.isRotateGesturesEnabled = MapConstants.MAP_ROTATE_ENABLED
            uiSettings.isTiltGesturesEnabled = false
            uiSettings.isCompassEnabled = false
            setMinZoomPreference(MapConstants.MAP_MIN_ZOOM)
            setMaxZoomPreference(MapConstants.MAP_MAX_ZOOM)
        }

        _map.value = newMap
        isInitialized = true
    }

    /**
     * Updates the pins on the map
     * Gives the pins a mapId if they lack one which
     * corresponds with the symbolId they have when on the map
     * @param[pins] the list of pins to be added
     */
    fun updatePinsOnMap(pins: List<Pin>) {
        if(this::symbolManager.isInitialized) {
            for (pin in pins) {
                symbolManager.create(SymbolOptions()
                        .withIconImage(PIN_ICON)
                        .withLatLng(LatLng(pin.lat, pin.lon))
                        .withIconSize(2f)
                        .withData(JsonPrimitive(pin.id)))
            }
        }
    }

    /**
     * Syncs up the server pins with our local pins.
     */
    fun refreshPins(){
        viewModelScope.launch(Dispatchers.IO){
            contentRepository.refreshPins()
        }
    }

    private fun addImagesToStyle(style: Style) {
        if (style.isFullyLoaded) {
            var drawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_pin, null)
            var mBitmap = BitmapUtils.getBitmapFromDrawable(drawable)
            if (mBitmap != null) {
                style.addImage(PIN_ICON, mBitmap)
            }
            drawable = ResourcesCompat.getDrawable(resources, R.drawable.geologic_pin, null)
            mBitmap = BitmapUtils.getBitmapFromDrawable(drawable)
            if (mBitmap != null) {
                style.addImage(GROUND_TYPE_ICON, mBitmap)
            }
        }
    }
}
