package com.thegravelers.uce.screens.start.teamcode

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.thegravelers.uce.databinding.StartingTeamFragmentBinding
import com.thegravelers.uce.screens.companion.CompanionActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StartingTeamFragment : Fragment() {
    private lateinit var binding: StartingTeamFragmentBinding
    val viewModel: StartingTeamViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = StartingTeamFragmentBinding.inflate(inflater)
        binding.startTeamViewModel = viewModel

        viewModel.getTeam()
        viewModel.teamList.observe(viewLifecycleOwner, { team ->
            team?.let {
                binding.StudentList.adapter = context?.let {
                    StartingTeamAdapter(it, team)
                }
            }
        })

        binding.leaveTeamButton.setOnClickListener {
            viewModel.leaveTeam()
        }

        viewModel.backToTeamList.observe(viewLifecycleOwner, { back ->
            if(back){
                findNavController().navigateUp()
                viewModel.leftTeam()
            }
        })

        binding.toAppButton.setOnClickListener {
            startCompanionActivity()
        }

        return binding.root
    }

    private fun startCompanionActivity() {
        val intent = Intent(activity, CompanionActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

}
