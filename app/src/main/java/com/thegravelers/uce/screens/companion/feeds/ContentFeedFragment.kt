/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.thegravelers.uce.databinding.ContentFeedFragmentBinding


abstract class ContentFeedFragment : Fragment() {
    protected lateinit var binding: ContentFeedFragmentBinding

    /**
     * Every child fragment needs to set this viewmodel
     */
    protected lateinit var viewModel: ContentFeedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = ContentFeedFragmentBinding.inflate(inflater)
        binding.contentFeedViewModel = viewModel
        binding.lifecycleOwner = this

        val adapter = ContentFeedAdapter(ContentFeedItemListener { contentId, contentType ->
            if(viewModel is SearchFeedViewModel){
                (viewModel as SearchFeedViewModel).onContentFeedItemClicked(contentId, contentType)
            }
            else {
                viewModel.onContentFeedItemClicked(contentId)
            }
        })
        binding.contentFeedList.adapter = adapter

        //checks if list is updated then makes sure it is shown
        viewModel.feedItemList.observe(viewLifecycleOwner, { listItems ->
            adapter.submitList(listItems)
            adapter.notifyDataSetChanged()
        })

        // Keeps track of what the current search bar says.
        // If it is empty, then we do not need to see the 'clear search' button
        viewModel.contentFilter.observe(viewLifecycleOwner, { filter ->
            binding.contentFeedEmptySearchBtn.visibility =
                    if (filter != "")
                        View.VISIBLE
                    else
                        View.GONE
        })

        binding.contentFeedEmptySearchBtn.setOnClickListener {
            viewModel.clearFilter()
        }

        return binding.root
    }
}
