/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map.geologic

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class GeologicInfoViewModel @Inject constructor(): ViewModel() {

    @Inject
    lateinit var geologicInfoParserFactory: GeologicInfoParserFactory
    private val _geologicInfo = MutableLiveData<List<String>>()
    val geologicInfo: LiveData<List<String>> get() = _geologicInfo

    private val geologicInfoParser: GeologicInfoParser by lazy {
        geologicInfoParserFactory.create("data")
    }

    fun getInfo(key: String) {
        _geologicInfo.value = geologicInfoParser.geologicInfoDB[key]
    }

}