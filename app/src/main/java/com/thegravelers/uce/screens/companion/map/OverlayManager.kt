/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import android.content.res.AssetManager
import android.graphics.BitmapFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngQuad
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.sources.ImageSource
import com.mapbox.mapboxsdk.style.layers.*
import com.mapbox.mapboxsdk.style.layers.Property.*
import com.thegravelers.uce.util.MapConstants
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import kotlin.math.*

@AssistedFactory
interface OverlayManagerFactory {
    fun create(overlayName: String, maxBoundsList: List<Double>, maxLayer: Int): OverlayManager
}

/**
 * Manages a single overlay used by the MapboxMap.
 * Determines which images should be loaded or not, based on position and zoom.
 * @param[overlayName] the name for this map and where to look for metadata files
 */
class OverlayManager @AssistedInject constructor(private val assetManager : AssetManager,
                                                 @Assisted overlayName: String,
                                                 @Assisted maxBoundsList: List<Double>,
                                                 @Assisted private val maxLayer: Int) {

    private val maxBoundsSouthWest = LatLng(maxBoundsList[1], maxBoundsList[0])
    private val maxBoundsNorthEast = LatLng(maxBoundsList[3], maxBoundsList[2])

    val maxBounds = OverlayQuad(maxBoundsSouthWest, maxBoundsNorthEast)
    private val notInitialized : Int = -1
    private var prevZoom : Int = notInitialized
    private lateinit var prevTarget : OverlayQuad
    private var prevImages : Array<Int>
    private var grid = hashMapOf<Int, Array<OverlayQuad>>()

    lateinit var style : Style
        private set

    init {
        grid[0] = arrayOf(maxBounds)
        fillQuadGrid(1, maxBounds)
        prevImages = arrayOf()
    }

    /**
     * Adds all sources and layers to the style with visibility that can be toggled
     * @param[curStyle] the style to be updated
     */
    fun initMap(curStyle: Style){
        style = curStyle
        var symbolLayerId = ""
        style.layers.forEach { layer ->

            if (layer.id.startsWith("mapbox-android-symbol-layer")){
                symbolLayerId = layer.id
                return@forEach
            }
        }
        for (layer in 0..maxLayer) {
            grid[layer].orEmpty().forEachIndexed { index, bounds ->
                val affix = "$layer/$index"
                val path = "$affix.png"
                style.addSource(ImageSource(affix, LatLngQuad(
                        bounds.northWest,
                        bounds.northEast,
                        bounds.southEast,
                        bounds.southWest),
                        BitmapFactory.decodeStream(assetManager.open(path))))
                if (symbolLayerId != "") {
                    style.addLayerBelow(RasterLayer("Overlay/$affix", affix), symbolLayerId)
                }
                else {
                    style.addLayer(RasterLayer("Overlay/$affix", affix))
                }

            }
        }
        hideLayers(style)
    }

    /**
     * The public function to be called by the ViewModel when the camera is idle.
     * Checks if the imagesToLoad have changed since last time this was called.
     * Creates a new list of images to load if it did change.
     * @param[zoom] the current zoom level of the camera
     * @param[target] the current bounding box of the camera
     */
    fun updateMap(zoom:Int, target:OverlayQuad){
        if (prevZoom != zoom || prevTarget != target)
        {
            val layer = getLayerFromZoom(zoom)
            val newImages = imagesToLoad(layer, target)
            if(!prevImages.contentEquals(newImages)){
                updateValues(zoom, target, newImages)
                hideLayers(style)
                updateStyle(layer, newImages)
            }
        }
    }

    /**
     * Updates the private values to be compared next time updateMap() is called
     * @param[zoom] the current zoom level of the camera
     * @param[target] the current bounding box of the camera
     * @param[images] the array of indices of images to load
     */
    private fun updateValues(zoom:Int, target:OverlayQuad, images:Array<Int>) {
        prevZoom = zoom
        prevTarget = target
        prevImages = images
    }

    /**
     * Updates the style with the new images
     * @param[layer] the current layer of images that is requested
     * @param[images] the array of indices of images to load
     */
    private fun updateStyle(layer : Int, images : Array<Int>){
        val layerIDs = Array(images.size) { "Overlay/$layer/" }
        images.forEachIndexed{index, image ->
            layerIDs[index] =  layerIDs[index] + image
        }
        for (styleLayer in style.layers){
            if (layerIDs.contains(styleLayer.id)){
                styleLayer.setProperties(PropertyFactory.visibility(VISIBLE))
            }
        }
    }

    /**
     * Public function to hide all layers
     * @param[style] the style to be updated
     */
    fun hideLayers(style: Style, shouldFullyHide: Boolean = false){
        for (layer in style.layers){
            if (layer.id.startsWith("Overlay/")){
                if ((!shouldFullyHide && layer.id.startsWith("Overlay/0"))){
                    layer.setProperties(PropertyFactory.visibility(VISIBLE))
                }
                else {
                    layer.setProperties(PropertyFactory.visibility(NONE))
                }
            }
        }
        updateValues(notInitialized, OverlayQuad(LatLng(0.0,0.0), LatLng(0.0,0.0)), arrayOf())
    }

    /**
     * Public function to get a grid index on the deepest layer from an input
     * @param[latLng] the coordinates of an input
     */
    fun gridIndexFromInput(latLng: LatLng): Int? {
        grid[maxLayer].orEmpty().forEachIndexed{index, bounds ->
            if (pointInBounds(latLng, bounds)){
                return index
            }
        }
        return null
    }

    /**
     * Figures out which images should be obtained by the image indices.
     * Used to check if different images should be shown
     * @param[layer] the current layer, derived from the zoom level of the camera
     * @param[target] the current bounding box of the camera
     */
    private fun imagesToLoad(layer:Int, target:OverlayQuad) : Array<Int>{
        //map is fully contained within camera view
        if(boundsInBounds(maxBounds, target)){
            return fullLayer(layer)
        }
        //map is visible
        else if((
                                pointInBounds(target.southWest, maxBounds) ||
                                pointInBounds(target.southEast, maxBounds) ||
                                pointInBounds(target.northWest,maxBounds) ||
                                pointInBounds(target.northEast,maxBounds) ||
                                pointInBounds(maxBounds.southWest, target) ||
                                pointInBounds(maxBounds.southEast, target) ||
                                pointInBounds(maxBounds.northWest,target) ||
                                pointInBounds(maxBounds.northEast,target) ||
                                pointInBounds(target.center, maxBounds))) {
            if (layer == 0){
                return arrayOf(0)
            }
            var northEast : Int = -1
            var southWest : Int = -1
            var northWest : Int = -1
            var southEast : Int = -1
            //check which grid tiles contain the corners of the camera view
            grid[layer].orEmpty().forEachIndexed { index : Int, segment : OverlayQuad ->
                if (pointInBounds(target.southWest, segment)){
                    southWest = index
                }
                if (pointInBounds(target.northEast, segment)){
                    northEast = index
                }
                if (pointInBounds(target.northWest, segment)){
                    northWest = index
                }
                if (pointInBounds(target.southEast, segment)){
                    southEast = index
                }

            }
            return indicesFromGrid(layer, northWest, northEast, southEast, southWest)

        }
        //map is outside of the camera view
        else {
            return arrayOf()
        }
    }

    /**
     * Check if a bounds is fully contained within another
     * @param[target] the bounds to be contained
     * @param[bounds] the bounds which contains
     */
    private fun boundsInBounds(target: OverlayQuad, bounds: OverlayQuad):Boolean{
        return (
                pointInBounds(target.southWest, bounds) &&
                pointInBounds(target.northEast, bounds)
                )
    }

    /**
     * Check if a point is fully contained within bounds
     * @param[point] the point to be contained
     * @param[bounds] the bounds which contains
     */
    private fun pointInBounds(point: LatLng, bounds: OverlayQuad):Boolean{
        return (
                point.latitude >= bounds.southWest.latitude && point.latitude <= bounds.northEast.latitude &&
                point.longitude >= bounds.southWest.longitude && point.longitude <= bounds.northEast.longitude
                )
    }

    /**
     * Gets all the indices of a given layer.
     * @param[layer] the layer to retrieve the indices from
     */
    private fun fullLayer(layer: Int):Array<Int>{
        return if(layer == 0)
            arrayOf(0)
        else {
            val size = 2f.pow(layer)
            (0..(size*size).toInt()).toList().toTypedArray()
        }
    }

    /**
     * Fills the grid dictionary up until the maxLayer. Keys are layers.
     * The values are arrays of bounds at the correct index per column from left to right.
     * This is done recursively so that's why the calculations are needed to get the correct index.
     * @param[curLayer] the layer that is currently being added, starting with 1 ends with [maxLayer].
     * Layer 0 is added separately.
     * @param[bounds] the bounding box that needs to be split up in four quads
     * @param[fromIndex] the variable to determine the correct index on recursive calls
     */
    private fun fillQuadGrid(curLayer: Int, bounds:OverlayQuad, fromIndex:Int = 0) {
        if (curLayer > maxLayer)
            return

        val quadList = createQuads(bounds)

        val indexedQuads= Array(4f.pow(curLayer).toInt()) { quadList[0] }
        grid[curLayer].orEmpty().forEachIndexed{ index: Int, element ->
            indexedQuads[index] = element
        }

        val indices = createIndices(curLayer, fromIndex)

        quadList.forEachIndexed { index: Int, quad ->
            indexedQuads[indices[index]] = quad
            fillQuadGrid(curLayer + 1, quad, indices[index] * 2)
        }
        grid[curLayer] = indexedQuads
    }

    /**
     * Creates four OverlayQuads out of the input
     * @param[bounds] the bounding box to be split up into four quads
     */
    private fun createQuads(bounds: OverlayQuad):Array<OverlayQuad>{
        val center = bounds.center
        val topCenter = LatLng(bounds.northEast.latitude, center.longitude)
        val botCenter = LatLng(bounds.southWest.latitude, center.longitude)
        val leftCenter = LatLng(center.latitude, bounds.southWest.longitude)
        val rightCenter = LatLng(center.latitude, bounds.northEast.longitude)
        val swQuad = OverlayQuad(bounds.southWest, center)
        val nwQuad = OverlayQuad(leftCenter, topCenter)
        val neQuad = OverlayQuad(center, bounds.northEast)
        val seQuad = OverlayQuad(botCenter, rightCenter)

        return arrayOf(nwQuad, swQuad, neQuad, seQuad)
    }

    /**
     * Calculates the correct index the OverlayQuads should have
     * @param[curLayer] the current layer that is being added
     * @param[fromIndex] the variable to calculate the correct index on recursive calls
     */
    private fun createIndices(curLayer: Int, fromIndex: Int):Array<Int>{
        val columnLength = 2f.pow(curLayer)
        val fromColumn = floor((fromIndex / columnLength).toDouble()).toInt()
        val topLeft = (fromIndex + fromColumn * columnLength).toInt()
        val topRight = (columnLength + fromIndex + fromColumn * columnLength).toInt()

        return arrayOf(topLeft, topLeft+1, topRight, topRight+1)
    }


    /**
     * From the four corners that are contained within the camera view, create the complete
     * grid of indices that need to be loaded
     * @param[layer] the current layer that is being requested
     * @param[northWest] the tile index in which the north-west corner of the camera lies, or -1 if out of bounds
     * @param[northEast] the tile index in which the north-east corner of the camera lies, or -1 if out of bounds
     * @param[southEast] the tile index in which the south-east corner of the camera lies, or -1 if out of bounds
     * @param[southWest] the tile index in which the south-west corner of the camera lies, or -1 if out of bounds
     */
    private fun indicesFromGrid(layer:Int, northWest: Int, northEast:Int, southEast: Int, southWest:Int):Array<Int>{
        val columnLength: Int = 2f.pow(layer).toInt()

        val leftColumnIndex = max(0, max(northWest / columnLength, southWest / columnLength))
        val topRowIndex = max(0, max(northEast % columnLength, northWest % columnLength))

        val rightColumnIndex = if (northEast == -1 && southEast == -1){
            columnLength -1
        }
        else {
            max(northEast / columnLength, southEast / columnLength)
        }

        val botRowIndex = if (southWest == -1 && southEast == -1){
            columnLength -1
        }
        else {
            max(southWest % columnLength, southEast % columnLength)
        }

        var array : Array<Int> = arrayOf()
        for(columnIndex in leftColumnIndex..rightColumnIndex){
            array += (topRowIndex + columnIndex * columnLength..botRowIndex + columnIndex * columnLength).toList().toTypedArray()
        }
        return array
    }

    /**
     * Converts the camera zoom level to the correct layer of images that should be loaded
     * @param[zoom] the current zoom level of the camera
     */
    private fun getLayerFromZoom(zoom:Int):Int{
        return if(zoom >= MapConstants.MAP_MAX_LOD_AT_ZOOM){
            maxLayer
        }
        else {
            val diff = MapConstants.MAP_MAX_LOD_AT_ZOOM - maxLayer
            max(0,(zoom - diff))
        }
    }
}
