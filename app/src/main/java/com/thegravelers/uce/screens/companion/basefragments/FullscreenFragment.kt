/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.basefragments

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.thegravelers.uce.screens.companion.CompanionActivity
import timber.log.Timber

/**
 * This fragment hides the bottom navigation bar automatically when inherited from
 */
open class FullscreenFragment : HideKeyboardFragment() {
    private class Fullscreen(private val lifecycle: Lifecycle, private val companionActivity: CompanionActivity) : LifecycleObserver {

        init {
            lifecycle.addObserver(this)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun hideBottomNavigationOnCreate(){
            companionActivity.hideBottomNav()
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun hideBottomNavigationOnResume(){
            companionActivity.hideBottomNav()
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun showBottomNavOnDestroy(){
            companionActivity.showBottomNav()
            lifecycle.removeObserver(this)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(requireActivity() is CompanionActivity){
            Fullscreen(this.lifecycle, requireActivity() as CompanionActivity)
        }
    }
}