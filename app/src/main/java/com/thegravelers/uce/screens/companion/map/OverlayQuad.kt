/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import com.mapbox.mapboxsdk.geometry.LatLng

data class OverlayQuad(val southWest: LatLng, val northEast: LatLng){
    val center : LatLng get() =
          LatLng(northEast.latitude - (northEast.latitude - southWest.latitude)/2,
                  northEast.longitude - (northEast.longitude - southWest.longitude)/2)
    val northWest : LatLng get() =
            LatLng(northEast.latitude, southWest.longitude)
    val southEast : LatLng get() =
            LatLng(southWest.latitude, northEast.longitude)

}
