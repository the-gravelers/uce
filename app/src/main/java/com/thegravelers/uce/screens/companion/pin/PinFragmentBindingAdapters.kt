/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.pin

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.viewpager2.widget.ViewPager2
import com.thegravelers.uce.R
import com.thegravelers.uce.screens.companion.gallery.GalleryAdapter

@BindingAdapter("editTextEditability")
fun EditText.isEditable(isEditable: Boolean) {
    // We need save the input type somehow since that can differ between
    // EditText fields
    if (this.inputType != EditorInfo.TYPE_NULL) {
        this.setTag(this.inputType)
    }
    if (isEditable) {
        this.visibility = View.VISIBLE
        this.inputType = this.tag as Int
        this.setBackgroundResource(R.drawable.text_rounded_background)
    } else {
        this.visibility = View.GONE
        this.inputType = EditorInfo.TYPE_NULL
        this.background = null
    }
}

@BindingAdapter("editTextVisiblity")
fun EditText.isVisible(isVisible: Boolean) {
    // We need save the input type somehow since that can differ between
    // EditText fields
    if (this.inputType != EditorInfo.TYPE_NULL) {
        this.setTag(this.inputType)
    }
    if (isVisible) {

        this.inputType = this.tag as Int
        this.setBackgroundResource(R.drawable.text_rounded_background)
    } else {

        this.inputType = EditorInfo.TYPE_NULL
        this.background = null
    }
}

@BindingAdapter("buttonEditability")
fun Button.isEditable(isEditable: Boolean) {
    if (isEditable) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("imageButtonEditability")
fun ImageButton.isEditable(isEditable: Boolean) {
    if (isEditable) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("clearGalleryAdapter")
fun ViewPager2.clear(shouldClear: Boolean){
    if(shouldClear) {
        if (this.adapter is GalleryAdapter) {
            val adapter = this.adapter as GalleryAdapter
            adapter.submitList(emptyList())
        }
    }
}

@BindingAdapter("cardViewEditability")
fun CardView.isEditable(isEditable: Boolean) {
    if (isEditable) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("textViewEditability")
fun TextView.isEditable(isEditable: Boolean) {
    if (isEditable) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("changeBackButtonImagePinFeed")
fun ImageButton.fromPinFeed(fromPinFeed: Boolean) {
    if (fromPinFeed) {
        this.setImageResource(R.drawable.pin_left_arrow)
    } else {
        this.setImageResource(R.drawable.pin_down_arrow)
    }
}

@BindingAdapter("visibilityDragbarPinFeed")
fun androidx.constraintlayout.widget.ConstraintLayout.fromPinFeed(fromPinFeed: Boolean) {
    if (fromPinFeed) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}


@BindingAdapter("clearEditText")
fun EditText.clear(shouldClear: Boolean){
    if(shouldClear){
        this.setText("")
    }
}