/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.book

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.thegravelers.uce.database.entity.BookContent
import com.thegravelers.uce.database.entity.BookContentType
import com.thegravelers.uce.databinding.BookContentListItemBinding

class BookContentAdapter : ListAdapter<BookContent, BookContentAdapter.ViewHolder>(BookContentItemCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    /**
     * A ViewHolder holds a view for the [RecyclerView] as well as providing additional information
     * to the RecyclerView such as where on the screen it was last drawn during scrolling.
     */
    class ViewHolder private constructor(val binding: BookContentListItemBinding) : RecyclerView.ViewHolder(binding.root){
        companion object{
            fun from(parent : ViewGroup) : ViewHolder{
                val inflater = LayoutInflater.from(parent.context)
                val binding = BookContentListItemBinding.inflate(inflater, parent, false)
                return ViewHolder(binding)
            }
        }

        //sets all the values for the items for the feed item in xml to that of the feed item data
        fun bind(item: BookContent){
            if (item.contentType == BookContentType.TEXT){
                binding.textView.text = item.content
            }
            else if (item.contentType == BookContentType.IMAGE){
                Glide.with(binding.imageView.context).load(item.content.toUri()).into(binding.imageView)
            }
            binding.executePendingBindings()
        }
    }

    /**
     * [BookContentItemCallback] extends [DiffUtil] to calculate the changes between content when the list changes
     * and update the list accordingly.
     */
    class BookContentItemCallback : DiffUtil.ItemCallback<BookContent>(){
        override fun areItemsTheSame(oldItem: BookContent, newItem: BookContent): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: BookContent, newItem: BookContent): Boolean {
            // ContentFeedItem is a data class that contains methods for comparing items
            return oldItem == newItem
        }
    }
}