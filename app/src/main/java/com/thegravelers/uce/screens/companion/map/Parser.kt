/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import android.content.res.AssetManager
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import java.io.InputStream


@AssistedFactory
interface ParserFactory {
    fun create(path: String): Parser
}

/**
 * A parser to obtain data for a specific map, these files are created by the preprocessor and obtained
 * from the server.
 * Tries to parse asynchronous when there are multiple files
 * @param[assetManager] the AssetManager required to retrieve assets
 * @param[path] the file path where the different assets for this map are stored
 */
class Parser @AssistedInject constructor(private val assetManager: AssetManager, @Assisted path: String) {

    var polygonDB : HashMap<String, Pair<String, Polygon>>
        private set
    var gridData : List<List<String>>
        private set
    var mapBounds : MutableList<Double>
        private set
    var maxLayers : Int
        private set

    init {
        polygonDB = parsePolygonsToDB(assetManager.open("$path/polygonData.txt"))
        gridData = parseGridDataToDB(assetManager.open("$path/gridData.txt"))
        mapBounds = mutableListOf()
        maxLayers = 0
        parseMetaData(assetManager.open("$path/metaData.txt"))
    }

    private fun parseMetaData(inputStream: InputStream) {
        inputStream.bufferedReader().forEachLine { line ->
            if(line.length> 2){
                mapBounds.add(line.toDouble())
            }
            else {
                maxLayers = line.toInt()
            }
        }
    }

    /**
     * gridData file contains polygon IDs on every line, each line represents the correct index
     * each index represents a grid tile on the max layer
     * @param[inputStream] the inputStream to read from
     */
    private fun parseGridDataToDB(inputStream: InputStream) :List<List<String>>{
        val db = mutableListOf<List<String>>()
        inputStream.bufferedReader().forEachLine {
            db.add(it.split(','))
        }
        return db
    }

    /**
     * Custom parser to read polygon data from a file. Polygons start of with ID followed by CC, then
     * the boundaries start with outerboundary and innerboundaries if they are present
     * @param[inputStream] the inputStream to read from
     */
    @Suppress("LocalVariableName")
    private fun parsePolygonsToDB(inputStream: InputStream) :HashMap<String, Pair<String, Polygon>> {
        val polygonDB = hashMapOf<String, Pair<String, Polygon>>()
        var ID = ""
        var CC = ""
        var outer : LineString = LineString.fromLngLats(mutableListOf())
        var inners : MutableList<LineString> = mutableListOf()
        var CCIsNext = false
        var outerIsNext = false
        var innerIsNext = false

        inputStream.bufferedReader().forEachLine forEach@{
            if (it.startsWith("ID")){
                if (innerIsNext || outerIsNext){
                    polygonDB[ID] = Pair(CC, Polygon.fromOuterInner(outer, inners))
                    inners = mutableListOf()
                    innerIsNext = false
                    outerIsNext = false
                }
                ID = it
                CCIsNext = true
                return@forEach
            }
            if (CCIsNext){
                CC = it
                CCIsNext = false
                return@forEach
            }
            if (it.startsWith("outer")){
                outerIsNext = true
                return@forEach
            }
            when {
                it.startsWith("inner") -> {
                    innerIsNext = true
                    return@forEach
                }
                innerIsNext -> {
                    val inner = LineString.fromLngLats(createPointListFromString(it))
                    inners.add(inner)
                    return@forEach
                }
                outerIsNext -> {
                    outer = LineString.fromLngLats(createPointListFromString(it))
                    return@forEach
                }
            }
        }
        polygonDB[ID] = Pair(CC, Polygon.fromOuterInner(outer, inners))
        return polygonDB
    }

    /**
     * Each coordinate is separated by a space ' '
     * Each point is separated by a comma ','
     * @param[string] represents a polygon by points
     */
    private fun createPointListFromString(string: String) : List<Point>{
        val points = mutableListOf<Point>()
        val list = string.split(' ')
        for(point in list){
            val sub = point.split(",")
            points.add(Point.fromLngLat(sub[0].toDouble(), sub[1].toDouble()))
        }
        return points
    }
}
