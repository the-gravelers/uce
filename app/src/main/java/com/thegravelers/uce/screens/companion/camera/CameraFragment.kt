/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.camera

import android.Manifest.permission.*
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.thegravelers.uce.R
import com.thegravelers.uce.databinding.CameraFragmentBinding
import com.thegravelers.uce.databinding.CameraUiBinding
import com.thegravelers.uce.screens.companion.OnBackPressed
import com.thegravelers.uce.domain.Image
import com.thegravelers.uce.screens.companion.basefragments.FullscreenFragment
import com.thegravelers.uce.screens.companion.gallery.SharedGalleryViewModel
import com.thegravelers.uce.util.Animation
import com.thegravelers.uce.util.ImageValues
import com.thegravelers.uce.util.MediaTypes
import com.thegravelers.uce.util.getCurrentTime
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

/**
 * Fragment used to take a picture
 */
@AndroidEntryPoint
class CameraFragment : FullscreenFragment(), MultiplePermissionsListener, OnBackPressed {
    companion object {
        private val PERMISSIONS_REQUIRED = mutableListOf(CAMERA)
        private val PERMISSIONS_REQUIRED_BEFORE_ANDROID_10 = mutableListOf(WRITE_EXTERNAL_STORAGE)
        private const val RATIO_4_3 = 4.0 / 3.0
        private const val RATIO_16_9 = 16.0 / 9.0
    }

    val sharedViewModel: SharedGalleryViewModel by activityViewModels()

    private lateinit var cameraContainer: ConstraintLayout
    private lateinit var cameraPreview: PreviewView
    private lateinit var cameraUiBinding: CameraUiBinding

    private var lensFacing: Int = CameraSelector.LENS_FACING_BACK
    val getLensFacing get() = lensFacing
    private var preview: Preview? = null
    private var imageCapture: ImageCapture? = null
    private var camera: Camera? = null
    private var cameraProvider: ProcessCameraProvider? = null
    private lateinit var oldBackground: Drawable

    /** Blocking camera operations are performed using this executor */
    private lateinit var cameraExecutor: ExecutorService

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding = CameraFragmentBinding.inflate(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.activity
        cameraContainer = view as ConstraintLayout
        cameraPreview = cameraContainer.findViewById(R.id.camera_preview)

        cameraPreview.previewStreamState.observe(viewLifecycleOwner, {
            if (it == PreviewView.StreamState.STREAMING) {
                setBackgroundVisibility(false)
            }
        })
        // Initialize background executor
        cameraExecutor = Executors.newSingleThreadExecutor()

        // Wait for the views to be properly laid out
        cameraPreview.post {
            // Redraw the camera UI controls
            updateCameraUi()

            // Enable or disable switching between cameras
            updateCameraSwitchButton()
        }
        if (Build.VERSION.SDK_INT < 29) {
            PERMISSIONS_REQUIRED.addAll(PERMISSIONS_REQUIRED_BEFORE_ANDROID_10)
        }
        Dexter.withContext(requireContext())
                .withPermissions(PERMISSIONS_REQUIRED)
                .withListener(this)
                .check()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cameraExecutor.shutdown()
    }

    //this function is called to see if permissions are accepted or denied
    //and does action depending on if it is denied or accepted
    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
        //permission is accepted
        if (report?.areAllPermissionsGranted() == true) {
            startCamera()
        }
        //when permission is denied
        else {
            //if(ContextCompat.checkSelfPermission(requireContext(), CAMERA)!= PERMISSION_DENIED||(Build.VERSION.SDK_INT < 29 && ContextCompat.checkSelfPermission(requireContext(), WRITE_EXTERNAL_STORAGE)!= PERMISSION_DENIED))
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Camera permission")
            builder.setMessage("Accessing your camera is needed to take pictures for pins by denying you will only be able to add pictures from your local storage to pins")
            builder.setIcon(R.drawable.ic_image)
            builder.setNeutralButton("okay") { dialog, _ ->
                findNavController().navigateUp()
                dialog.cancel()
            }
            builder.show()
        }
    }

    override fun onPermissionRationaleShouldBeShown(p0: MutableList<PermissionRequest>?, token: PermissionToken?) {
        token?.continuePermissionRequest()
    }

    /** Method used to re-draw the camera UI controls, called every time configuration changes. */
    private fun updateCameraUi() {

        // Remove previous UI if any
        cameraContainer.findViewById<ConstraintLayout>(R.id.camera_ui)?.let {
            cameraContainer.removeView(it)
        }

        cameraUiBinding = CameraUiBinding.inflate(LayoutInflater.from(requireContext()), cameraContainer, true)

        // Listener for button used to capture photo
        cameraUiBinding.cameraCaptureButton.setOnClickListener {
            capturePhoto()
        }

        // Setup for button used to switch cameras
        cameraUiBinding.cameraSwitchButton.let {

            // Disable the button until the camera is set up
            it.isEnabled = false

            // Listener for button used to switch camera. Only called if the button is enabled
            it.setOnClickListener {
                lensFacing = if (lensFacing == CameraSelector.LENS_FACING_FRONT) {
                    setBackgroundVisibility(true)
                    CameraSelector.LENS_FACING_BACK
                } else {
                    setBackgroundVisibility(true)
                    CameraSelector.LENS_FACING_FRONT
                }
                // Re-bind use cases to update selected camera
                bindCameraUseCases()
            }
        }

        // Listener for button used to view the most recent photo
        cameraUiBinding.photoViewButton.setOnClickListener {
            setBackgroundVisibility(true)
            goToGallery()
        }

        // Check if list is empty
        sharedViewModel.images.observe(viewLifecycleOwner, {
            val show = (it.size > 0)
            cameraUiBinding.photoViewButton.isEnabled = show
            cameraUiBinding.photoViewButton.isVisible = show

            if (show) {
                setGalleryThumbnail(it.last())
            }
        })
    }

    /** Initialize CameraX, and prepare to bind the camera use cases  */
    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener( {

            cameraProvider = cameraProviderFuture.get()

            // Select lensFacing depending on the available cameras
            lensFacing = when {
                hasBackCamera() -> CameraSelector.LENS_FACING_BACK
                hasFrontCamera() -> CameraSelector.LENS_FACING_FRONT
                else -> throw IllegalStateException("Back and front camera are unavailable")
            }

            updateCameraSwitchButton()

            bindCameraUseCases()

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun goToGallery() {
        findNavController().navigate(R.id.action_cameraFragment_to_galleryFragment)
    }

    /** Declare and bind preview, capture and analysis use cases */
    private fun bindCameraUseCases() {
        // Get screen metrics used to setup camera for full screen resolution
        val metrics = DisplayMetrics().also { cameraPreview.display.getRealMetrics(it) }
        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
        val rotation = cameraPreview.display.rotation
        // CameraProvider
        val cameraProvider = cameraProvider
                ?: throw IllegalStateException("Camera initialization failed.")
        // CameraSelector
        val cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()

        // Preview
        preview = Preview.Builder()
                // We request aspect ratio but no resolution
                .setTargetAspectRatio(screenAspectRatio)
                // Set initial target rotation
                .setTargetRotation(rotation)
                .build()

        // ImageCapture
        imageCapture = ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                // We request aspect ratio but no resolution to match preview config, but letting
                // CameraX optimize for whatever specific resolution best fits our use cases
                .setTargetAspectRatio(screenAspectRatio)
                // Set initial target rotation, we will have to call this again if rotation changes
                // during the lifecycle of this use case
                .setTargetRotation(rotation)
                .build()

        // Must unbind the use-cases before rebinding them
        cameraProvider.unbindAll()

        try {
            // A variable number of use-cases can be passed here -
            // camera provides access to CameraControl & CameraInfo
            camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            // Attach the viewfinder's surface provider to preview use case
            preview?.setSurfaceProvider(cameraPreview.surfaceProvider)
        } catch (exc: Exception) {
            Timber.e("Use case binding failed, $exc")
        }
    }

    /**
     * Function for setting the background to fix rendering issues within android
     * Background should be set whenever the camera isn't ready yet to show the data stream
     * and can be turned off when it's ready
     */
    private fun setBackgroundVisibility(visible: Boolean) {
        if(!visible) {
            oldBackground = cameraUiBinding.cameraUi.background
            cameraUiBinding.cameraUi.background = null
        }
        else{
            if(this::oldBackground.isInitialized) {
                cameraUiBinding.cameraUi.background = oldBackground
            }
        }
    }

    private fun capturePhoto() {
        // Get a stable reference of the modifiable image capture use case
        imageCapture?.let { imageCapture ->

            // Create options to write captured image to MediaStore
            val contentValues = Image.getContentValues(
                    displayName = ImageValues.DISPLAY_NAME + getCurrentTime(),
                    mimeType = MediaTypes.JPEG)

            // Setup image capture metadata
            val metadata = ImageCapture.Metadata().apply {
                // Mirror image when using the front camera
                isReversedHorizontal = lensFacing == CameraSelector.LENS_FACING_FRONT
            }

            // Create output options object which contains file + metadata
            val outputOptions = ImageCapture.OutputFileOptions.Builder(
                    requireActivity().contentResolver,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    contentValues)
                    .setMetadata(metadata)
                    .build()

            // Setup image capture listener which is triggered after photo has been taken
            imageCapture.takePicture(outputOptions, cameraExecutor, object : ImageCapture.OnImageSavedCallback {

                override fun onError(exc: ImageCaptureException) {
                    Timber.e("Photo capture failed: ${exc.message}")
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = output.savedUri
                            ?: throw IllegalStateException("Could not resolve Uri")

                    // Save the image in the viewModel
                    lifecycleScope.launch {
                        sharedViewModel.addImage(savedUri)
                    }

                    setGalleryThumbnail(savedUri)
                }
            })

            contentValues.clear()

            // Play flash animation to indicate a picture was taken
            flashAnimation()
        }
    }

    /**
     * Update the gallery thumbnail with latest picture taken
     */
    private fun setGalleryThumbnail(uri: Uri) {
        // Reference of the view that holds the gallery thumbnail
        val thumbnail = cameraContainer.findViewById<ImageButton>(R.id.photo_view_button)

        // Run the operations in the view's thread
        thumbnail.post {

            // Remove thumbnail padding
            thumbnail.setPadding(resources.getDimension(R.dimen.stroke_small).toInt())

            // Load thumbnail into circular button using Glide
            Glide.with(thumbnail)
                    .load(uri)
                    .apply(RequestOptions.circleCropTransform())
                    .into(thumbnail)
        }
    }

    /** Enabled or disabled a button to switch cameras depending on the available cameras */
    private fun updateCameraSwitchButton() {
        val switchCamerasButton = cameraUiBinding.cameraSwitchButton
        try {
            switchCamerasButton.isEnabled = hasBackCamera() && hasFrontCamera()
        } catch (exception: CameraInfoUnavailableException) {
            switchCamerasButton.isEnabled = false
        }
    }

    private fun flashAnimation() {
        // We can only change the foreground Drawable using API level 23+ API
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Display flash animation to indicate that photo was captured
            cameraContainer.postDelayed({
                cameraContainer.foreground = ColorDrawable(Color.WHITE)
                cameraContainer.postDelayed(
                        { cameraContainer.foreground = null }, Animation.FAST_MILLIS)
            }, Animation.SLOW_MILLIS)
        }
    }

    /**
     *  [androidx.camera.core.AspectRatio]. Currently it has values of 4:3 & 16:9.
     *
     *  Detecting the most suitable ratio for dimensions provided in @params by counting absolute
     *  of preview ratio to one of the provided values.
     *
     *  @param width - preview width
     *  @param height - preview height
     *  @return suitable aspect ratio
     */
    private fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = max(width, height).toDouble() / min(width, height)
        if (abs(previewRatio - RATIO_4_3) <= abs(previewRatio - RATIO_16_9)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }

    /** Returns true if the device has an available back camera. False otherwise */
    private fun hasBackCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA) ?: false
    }

    /** Returns true if the device has an available front camera. False otherwise */
    private fun hasFrontCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA) ?: false
    }

    override fun onBackPressed(): Boolean {
        setBackgroundVisibility(true)
        return true
    }

    override fun onStop() {
        setBackgroundVisibility(true)
        super.onStop()
    }
}
