/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.uceserver

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Data classes to parse or prepare network calls as described by the server documentation
 * https://gitlab.com/companion-to-the-earth/uce-server/-/blob/master/openapi3.yml
 */

// ---------------------------------------- AUTH
/**
 * @param[grantType] for a auth
 */
@JsonClass(generateAdapter = true)
data class AuthRequest(
        @Json(name = "grant_type")
        val grantType: String = "password",
        val email: String,
        val password: String)

@JsonClass(generateAdapter = true)
data class RefreshTokenRequest(
        @Json(name = "grant_type")
        val grantType: String = "refresh_token",
        @Json(name = "refresh_token")
        val refreshToken: String,
        @Json(name = "session_id")
        val sessionId: Long)

@JsonClass(generateAdapter = true)
data class AuthResponse(
        @Json(name = "session_id")
        val sessionId: Long,
        @Json(name = "access_token")
        val accessToken: String,
        @Json(name = "token_type")
        val tokenType: String,
        @Json(name = "expires_in")
        val expiresIn: Long,
        @Json(name = "refresh_token")
        val refreshToken: String)

// ---------------------------------------- USERS
@JsonClass(generateAdapter = true)
data class UserDataResponse(
        val id: Long,
        @Json(name = "id_str")
        val idStr: String,
        val name: String,
        val email: String,
        @Json(name = "auth_level")
        val authLevel: Int,
        @Json(name = "class_id")
        val classId: Long?,
        @Json(name = "class_id_str")
        val classIdStr: String?)

// ---------------------------------------- USER-GROUPS
@JsonClass(generateAdapter = true)
data class UserGroupResponse(
        @Json(name = "user_group_id")
        val userGroupId: Long,
        @Json(name = "pin_group_id")
        val pinGroupId: Long,
        val name: String,
        val members: List<Member>)

@JsonClass(generateAdapter = true)
data class Member(
        @Json(name = "user_id")
        val userId: Long)

// ---------------------------------------- PINS
@JsonClass(generateAdapter = true)
data class PostPinRequest(
        val title: String,
        val lat: Double,
        val lon: Double,
        val text: String)

@JsonClass(generateAdapter = true)
data class PostPinResponse(
        val id: Long)

@JsonClass(generateAdapter = true)
data class GetPinGroupResponse(
        val id: Long,
        val name: String,
        val pins: List<PinId>)

@JsonClass(generateAdapter = true)
data class PinId(
        val id: Long,
        val title: String)

@JsonClass(generateAdapter = true)
data class GetPinsResponse(
        val id: Long,
        @Json(name = "last_edited")
        val lastEdited: Long,
        val title: String,
        val latitude: Double,
        val longitude: Double,
        @Json(name = "user_id")
        val userId: Long,
        val text: String)

@JsonClass(generateAdapter = true)
data class GetPinResponse(
        val id: Long,
        @Json(name = "last_edited")
        val lastEdited: Long,
        val title: String,
        val latitude: Double,
        val longitude: Double,
        @Json(name = "user_id")
        val userId: Long,
        val text: String,
        val contents: List<PinContentInfo>)

// ---------------------------------------- CONTENTS
@JsonClass(generateAdapter = true)
data class PostPinContentRequest(
        @Json(name = "content_type")
        val contentType: String,
        @Json(name = "content_nr")
        val contentNr: Int)

/**
 * @param[contentType] can be of type "text", "image", "video" or "quiz"
 */
@JsonClass(generateAdapter = true)
data class PinContentInfo(
        val id: Long,
        @Json(name = "content_type")
        val contentType: String,
        @Json(name = "content_nr")
        val contentNr: Int)

// ---------------------------------------- LIBRARY
@JsonClass(generateAdapter = true)
data class GetBooksResponse(
        @Json(name = "book_id")
        val bookId: Long,
        @Json(name = "last_edited")
        val lastEdited: Long
)

@JsonClass(generateAdapter = true)
data class GetBookResponse(
        @Json(name = "book_id")
        val id: Long,
        val title: String,
        @Json(name = "last_edited")
        val lastEdited: Long,
        val sections: List<GetBookSectionResponse>
)

@JsonClass(generateAdapter = true)
data class GetBookSectionResponse(
        @Json(name = "book_section_id")
        val bookSectionId: Long,
        @Json(name = "book_id")
        val bookId: Long,
        val title: String,
        @Json(name = "section_nr")
        val sectionNr: Int,
        val contents: List<GetBookContentResponse>
)

@JsonClass(generateAdapter = true)
data class GetBookContentResponse(
        @Json(name = "book_content_id")
        val bookContentId: Long,
        @Json(name = "book_section_id")
        val bookSectionId: Long,
        @Json(name = "content_type")
        val contentType: String,
        @Json(name = "content_nr")
        val contentNr: Int
)