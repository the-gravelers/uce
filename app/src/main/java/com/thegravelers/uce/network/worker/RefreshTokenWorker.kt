/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.worker

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.thegravelers.uce.network.uceserver.RefreshTokenRequest
import com.thegravelers.uce.network.uceserver.UceServerApiService
import com.thegravelers.uce.repository.sessionmanager.AuthInfo
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.util.WorkerConstants
import com.thegravelers.uce.util.getCurrentTimeSeconds
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.runBlocking

/**
 * [Worker] that is supposed to refresh our AUTH_TOKEN with the server once every [WorkerConstants.REFRESH_TOKEN_INTERVAL] minutes.
 */
@HiltWorker
class RefreshTokenWorker @AssistedInject constructor(
        @Assisted appContext: Context,
        @Assisted workerParams: WorkerParameters,
        private val sessionManager: SessionManager,
        private val uceServerApi: UceServerApiService
) : Worker(appContext, workerParams) {

    override fun doWork(): Result {
        // Check if this is our first time performing this work
        // If it is, this is right after we logged in so no need to refresh our AUTH TOKEN
        val first: Boolean = inputData.getBoolean(WorkerConstants.KEY_REFRESH_TOKEN_FIRST, false)
        val output = workDataOf(WorkerConstants.KEY_REFRESH_TOKEN_FIRST to false)

        if (!first) {
            var newAuthInfo: AuthInfo? = null
            try {
                // First, we collect our old data and create a RefreshTokenRequest
                val oldAuthInfo = sessionManager.fetchAuthInfo()!!
                val request = RefreshTokenRequest(
                        refreshToken = oldAuthInfo.refreshToken,
                        sessionId = oldAuthInfo.sessionId
                )

                // Make the request to the server
                val response = runBlocking { uceServerApi.refreshToken(request) }

                // Save our data is the request was successful
                if (response.isSuccessful) {
                    response.body()?.let {
                        newAuthInfo = AuthInfo(
                                it.sessionId,
                                it.accessToken,
                                it.refreshToken,
                                it.expiresIn + getCurrentTimeSeconds()
                        )
                    }
                }
                else if(response.code() == 400){
                    // Something is wrong with our RefreshToken / sessionId.
                    return Result.failure()
                }
                else{
                    return Result.retry()
                }
            } catch (exception: Exception) {
                // Something went wrong with either our SessionManager or our server call
                return Result.retry()
            }

            newAuthInfo?.let { sessionManager.saveAuthInfo(it) }
        }
        return Result.success(output)
    }
}
