/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.uceserver

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

/**
 * This api service is used to communicate with the server at defined at [UceServerPaths].
 */
interface UceServerApiService {
    // ---------------------------------------- AUTH
    /**
     * Authenticate/login a user
     */
    @POST(UceServerPaths.AUTH)
    suspend fun authenticate(
            @Body authRequest: AuthRequest
    ): Response<AuthResponse>

    /**
     * If an access token expired you can request a new one using the refresh token provided in a previous access token request.
     */
    @POST(UceServerPaths.AUTH)
    suspend fun refreshToken(
            @Body refreshTokenRequest: RefreshTokenRequest
    ): Response<AuthResponse>

    // ---------------------------------------- USERS
    @GET(UceServerPaths.USER_DATA)
    suspend fun getUserData(
    ): Response<UserDataResponse>

    // ---------------------------------------- USER-GROUPS
    @GET(UceServerPaths.USER_GROUPS)
    suspend fun getUserGroups(
            @Query("class_id") classId: Long
    ): Response<List<UserGroupResponse>>

    @GET(UceServerPaths.USER_GROUPS_ID)
    suspend fun getUserGroup(
            @Path("id") groupId: Long
    ): Response<UserGroupResponse>

    @GET(UceServerPaths.USER_GROUPS_JOIN)
    suspend fun joinUserGroup(
            @Path("id") groupId: Long
    ): Response<Unit>

    @GET(UceServerPaths.USER_GROUPS_LEAVE)
    suspend fun leaveUserGroup(
            //@Path("id") groupId: Long
    ): Response<Unit>

    // ---------------------------------------- PINS
    @POST(UceServerPaths.PINS)
    suspend fun postPin(
            @Body createPostPinRequest: PostPinRequest
    ): Response<PostPinResponse>

    /**
     * @param[classId] Get all pins from a specific class, including user groups and users within the class
     * @param[groupId] Get all pins from a specific user group, including pins from group members
     * @param[userId] Get all pins from a specific user
     */
    @GET(UceServerPaths.PINS)
    suspend fun getPins(
            @Query("class") classId: Long? = null,
            @Query("group") groupId: Long? = null,
            @Query("user") userId: Long? = null
    ): Response<List<GetPinsResponse>>

    /**
     * @param[pinId] The server id in [Pin]
     */
    @GET(UceServerPaths.PINS_ID)
    suspend fun getPin(
            @Path("id") pinId: Long
    ): Response<GetPinResponse>

    @PUT(UceServerPaths.PINS_ID)
    suspend fun updatePin(
            @Path("id") pinId: Long,
            @Body postPinRequest: PostPinRequest
    ): Response<Unit>

    @DELETE(UceServerPaths.PINS_ID)
    suspend fun deletePin(
            @Path("id") pinId: Long
    ): Response<Unit>

    // ---------------------------------------- PIN CONTENT
    /**
     * Uploads an image to the PinContent Api on the server.
     * @param[pinId] The pin to which the content belongs
     * @param[contentType] Info about the MultipartBody, should include "multipart/form-data; boundary="
     * @param[body] The body of the request, including a JSON object about pincontentInfo and the image
     */
    @POST(UceServerPaths.PIN_CONTENTS)
    suspend fun postPinContent(
            @Path("pin_id") pinId: Long,
            @Header("Content-Type") contentType: String,
            @Body body: MultipartBody
    ): Response<Unit>

    @GET(UceServerPaths.PIN_CONTENTS_ID)
    @Streaming
    suspend fun getPinContent(
            @Path("pin_id") pinId: Long,
            @Path("content_id") contentId: Long
    ): Response<ResponseBody>

    @DELETE(UceServerPaths.PIN_CONTENTS_ID)
    suspend fun deletePinContent(
            @Path("pin_id") pinId: Long,
            @Path("content_id") contentId: Long
    ): Response<Unit>

    // ---------------------------------------- BOOKS
    @GET(UceServerPaths.BOOKS)
    suspend fun getBooks(
    ): Response<List<GetBooksResponse>>

    @GET(UceServerPaths.BOOKS_ID)
    suspend fun getBook(
           @Path("id") bookId: Long
    ): Response<GetBookResponse>

    @GET(UceServerPaths.BOOK_CONTENTS_ID)
    @Streaming
    suspend fun getBookContent(
            @Path("book_content_id") bookContentId: Long
    ): Response<ResponseBody>
}
