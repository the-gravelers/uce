/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.uceserver

object UceServerPaths {
    const val BASE_URL = "https://uce.science.uu.nl/"
    private const val API = "api"
    // Auth
    const val AUTH = "$API/auth/token"
    // Users
    private const val USERS = "$API/users"
    const val USER_DATA = "$USERS/info"
    const val UPDATE_PASSWORD = "$USERS/{id}/password"
    // User Groups
    const val USER_GROUPS = "$API/user-groups"
    const val USER_GROUPS_ID = "$USER_GROUPS/{id}"
    const val USER_GROUPS_JOIN = "$USER_GROUPS/{id}/join"
    const val USER_GROUPS_LEAVE= "$USER_GROUPS/leave"
    // Pins
    const val PINS = "$API/pins/"
    const val PINS_ID = "$PINS{id}"
    // Pins Content
    const val PIN_CONTENTS = "$API/pins/{pin_id}/contents/"
    const val PIN_CONTENTS_ID = "$PIN_CONTENTS{content_id}"
    // Books
    const val BOOKS = "$API/books"
    const val BOOKS_ID = "$BOOKS/{id}"
    const val BOOK_CONTENTS_ID = "$API/books/sections/contents/{book_content_id}"
}
