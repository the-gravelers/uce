/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.uceserver

import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.network.ServiceGenerator
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * With every request send to the server this interceptor is automatically added for authorization.
 * It will use the token set in the [sessionManager].
 * @see [ServiceGenerator]
 */
class AuthInterceptor @Inject constructor(
        private val sessionManager: SessionManager
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        // If we have a saved token
        sessionManager.fetchAuthInfo()?.let {
            requestBuilder.addHeader("Authorization", "Bearer ${it.authToken}")
        }
        return chain.proceed(requestBuilder.build())
    }
}