/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network

import com.thegravelers.uce.network.uceserver.AuthInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Creates an instance of a services. This service will log its requests and responses and uses
 * an [authentication interceptor][AuthInterceptor]. This interceptor will automatically add
 * the necessary authentication headers to each request.
 */
class ServiceGenerator
@Inject
constructor(
        private val builder: Retrofit.Builder,
        private val authInterceptor: AuthInterceptor
        ) {

    // For logging the actual requests send and responses received before parsing
    private val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val httpClient = OkHttpClient.Builder()

    fun <S> createService(service: Class<S>?): S {
        if (!httpClient.interceptors().contains(logging)) {
            httpClient.addInterceptor(logging)
            builder.client(httpClient.build())
        }
        if (!httpClient.interceptors().contains(authInterceptor)) {
            httpClient.addInterceptor(authInterceptor)
            builder.client(httpClient.build())
        }

        return builder.build().create(service)
    }
}