/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.worker

import android.content.ContentResolver
import android.content.Context
import androidx.core.net.toUri
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.squareup.moshi.Moshi
import com.thegravelers.uce.database.dao.PinDao
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.PinContent
import com.thegravelers.uce.database.entity.PinWithContents
import com.thegravelers.uce.network.uceserver.*
import com.thegravelers.uce.util.MediaTypes
import com.thegravelers.uce.util.WorkerConstants
import com.thegravelers.uce.util.roundDoubleTo
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.outputStream

/**
 * [Worker] that is supposed to refresh our local pins with the pins on the server.
 * Should retrieve [Pin] from the server if our pin is older than the server pin.
 * Should upload our [Pin] if our pin is newer than the server pin, or if the pin does not exist on the server.
 */
@ExperimentalPathApi
@HiltWorker
class UploadPinsWorker @AssistedInject constructor(
        @Assisted appContext: Context,
        @Assisted workerParams: WorkerParameters,
        private val uceServerApi: UceServerApiService,
        private val pinDao: PinDao,
        private val moshi: Moshi,
        private val contentResolver: ContentResolver
) : Worker(appContext, workerParams) {

    /**
     * Uploads or updates the supplied pins to the server.
     */
    override fun doWork(): Result {
        val pinIds = inputData.getLongArray(WorkerConstants.KEY_UPLOAD_PINS_IDS)
        var successful = true
        runBlocking {
            pinIds?.forEach {
                val pinWithContent = pinDao.getPinWithContents(it)
                successful = successful &&
                        if (pinWithContent.pin.serverId == -1L)
                            uploadPin(pinWithContent) else updatePin(pinWithContent)
            }
        }

        return if (successful) Result.success()
        else Result.retry()
    }

    /**
     * Uploads a single pin to the server, including its contents
     * @return True if everything went well. False if either the pin or one of its contents failed.
     */
    private suspend fun uploadPin(pinWithContents: PinWithContents): Boolean {

        val response = uceServerApi.postPin(PostPinRequest(
                pinWithContents.pin.title,
                roundDoubleTo(pinWithContents.pin.lat, 6),
                roundDoubleTo(pinWithContents.pin.lon, 6),
                pinWithContents.pin.text
        ))

        if (response.isSuccessful) {
            val pinId = response.body()?.id
            var success = true
            pinId?.let {
                // Update our current pin with the new server_id
                pinWithContents.pin.serverId = pinId
                pinDao.update(pinWithContents.pin)

                // Post all of our PinContent that we have for this pin
                for (pinContent in pinWithContents.contents) {
                    success = success && postPinContent(it, pinContent)
                }
            }
            return success
        }
        return false
    }

    /**
     * Updates a pin on the server.
     * At the moment, it deletes all current PinContents on the server and uploads our own.
     * Then it Updates the pin info.
     * We assume that the pin that you want updated EXISTS on the server (so its serverId != -1)
     */
    private suspend fun updatePin(pinWithContents: PinWithContents): Boolean {
        if (pinWithContents.pin.serverId == -1L) {
            return false
        }

        // First we delete all pincontent that we are aware of on the server
        val getPinResponse = uceServerApi.getPin(pinWithContents.pin.serverId)
        if (getPinResponse.isSuccessful) {
            getPinResponse.body()?.let {
                for (content in it.contents) {
                    uceServerApi.deletePinContent(pinWithContents.pin.serverId, content.id)
                }
            }
        }

        var success = true
        // Now we upload all of our current content
        for (content in pinWithContents.contents) {
            success = success && postPinContent(pinWithContents.pin.serverId, content)
        }

        val response = uceServerApi.updatePin(pinWithContents.pin.serverId,
                PostPinRequest(
                        pinWithContents.pin.title,
                        pinWithContents.pin.lat,
                        pinWithContents.pin.lon,
                        pinWithContents.pin.text
                ))
        return success && response.isSuccessful
    }

    private suspend fun postPinContent(pinId: Long, content: PinContent): Boolean {
        content.uri?.let{ uriString ->
            val uri = uriString.toUri()
            @Suppress("BlockingMethodInNonBlockingContext")
            val inputStream = contentResolver.openInputStream(uri)
            val f = kotlin.io.path.createTempFile("uploadFile", "pin_$pinId")
            inputStream!!.copyTo(f.outputStream())
            val file = File(f.toString())

            val fileBody = file.asRequestBody(MediaTypes.PNG.toMediaTypeOrNull())

            val info = PostPinContentRequest(
                    content.contentType,
                    content.contentIndex
            )
            val jsonAdapter = moshi.adapter(PostPinContentRequest::class.java)

            val body = MultipartBody.Builder()
                    .addFormDataPart("pin_content", jsonAdapter.toJson(info))
                    .addFormDataPart("file", "${file.name}.png", fileBody)
                    .build()

            val response = uceServerApi.postPinContent(
                    pinId,
                    "multipart/form-data; boundary=" + body.boundary,
                    body)
            file.delete()
            return response.isSuccessful
        }
        return false
    }
}