/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.core.util.Preconditions
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavGraphBuilder
import androidx.navigation.createGraph
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import com.thegravelers.uce.screens.HiltTestActivity
import com.thegravelers.uce.screens.companion.CompanionActivity
import org.mockito.Mockito
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

inline fun <reified T : Fragment> launchFragmentInHiltContainer(
        fragmentArgs: Bundle? = null,
        crossinline action: Fragment.() -> Unit = {}
) {
    val startActivityIntent = Intent.makeMainActivity(
            ComponentName(
                    ApplicationProvider.getApplicationContext(),
                    HiltTestActivity::class.java
            )
    )

    ActivityScenario.launch<HiltTestActivity>(startActivityIntent).onActivity { activity ->
        val fragment: Fragment = activity.supportFragmentManager.fragmentFactory.instantiate(
                Preconditions.checkNotNull(T::class.java.classLoader),
                T::class.java.name
        )
        fragment.arguments = fragmentArgs
        activity.supportFragmentManager
                .beginTransaction()
                .add(android.R.id.content, fragment, "HiltTestFragment")
                .commitNowAllowingStateLoss()
        fragment.action()
    }
}

inline fun <reified T : Fragment> launchFragmentInNavHostFragment(
        fragmentArgs: Bundle? = null,
        fragmentId: Int? = null,
        crossinline action: NavHostFragment.() -> Unit = {}
) {
    val startActivityIntent = Intent.makeMainActivity(
            ComponentName(
                    ApplicationProvider.getApplicationContext(),
                    HiltTestActivity::class.java
            )
    )

    ActivityScenario.launch<HiltTestActivity>(startActivityIntent).onActivity { activity ->
        val navHostFragment = (activity.supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment)
        val inflater = navHostFragment.navController.navInflater
        val graph = inflater.inflate(R.navigation.companion_navigation)
        if (fragmentId != null) {
            graph.setStartDestination(fragmentId)
        }
        navHostFragment.navController.graph = graph

        activity.recreate()
        navHostFragment.action()
    }
}

/**
 * Launch fragment when u need the companion activity. Else use the HiltContainer.
 * If u need to test navigation, also use the HiltContainer
 * @see launchFragmentInHiltContainer
 */
inline fun <reified T : Fragment> launchFragmentInCompanionActivity(
        fragmentArgs: Bundle? = null,
        crossinline action: Fragment.() -> Unit = {}
) {
    val startActivityIntent = Intent.makeMainActivity(
            ComponentName(
                    ApplicationProvider.getApplicationContext(),
                    CompanionActivity::class.java
            )
    )

    ActivityScenario.launch<CompanionActivity>(startActivityIntent).onActivity { activity ->
        val fragment: Fragment = activity.supportFragmentManager.fragmentFactory.instantiate(
                Preconditions.checkNotNull(T::class.java.classLoader),
                T::class.java.name
        )
        fragment.arguments = fragmentArgs
        activity.supportFragmentManager
                .beginTransaction()
                .add(android.R.id.content, fragment, "HiltTestFragment")
                .commitNowAllowingStateLoss()
        fragment.action()
    }
}

@VisibleForTesting(otherwise = VisibleForTesting.NONE)
fun <T> LiveData<T>.getOrAwaitValue(
        time: Long = 2,
        timeUnit: TimeUnit = TimeUnit.SECONDS,
        afterObserve: () -> Unit = {}
): T {
    var data: T? = null
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(o: T?) {
            data = o
            latch.countDown()
            this@getOrAwaitValue.removeObserver(this)
        }
    }
    this.observeForever(observer)

    try {
        afterObserve.invoke()

        // Don't wait indefinitely if the LiveData is not set.
        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

    } finally {
        this.removeObserver(observer)
    }

    @Suppress("UNCHECKED_CAST")
    return data as T
}

object MockitoHelper {
    fun <T> anyObject(): T {
        Mockito.any<T>()
        return uninitialized()
    }
    @Suppress("UNCHECKED_CAST")
    fun <T> uninitialized(): T =  null as T
}
