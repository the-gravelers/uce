/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asLiveData
import androidx.test.filters.SmallTest
import com.thegravelers.uce.database.UceDatabase
import com.thegravelers.uce.database.dao.*
import com.thegravelers.uce.repository.mediaaccess.MediaAccess
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.work.*
import androidx.work.impl.OperationImpl
import com.thegravelers.uce.MockitoHelper
import com.thegravelers.uce.database.entity.*
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.network.uceserver.*
import com.thegravelers.uce.util.WorkerConstants
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.internal.platform.android.AndroidLogHandler.close
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.*
import org.mockito.stubbing.Answer
import retrofit2.Response
import javax.inject.Inject

@HiltAndroidTest
@SmallTest
class UserRepositoryImplTest{

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var sessionManager: SessionManager

    private lateinit var userRepository: UserRepository
    private lateinit var pinGroupDao: PinGroupDao
    private lateinit var userDao: UserDao
    private lateinit var uceServerApiService: UceServerApiService
    private lateinit var userGroupDao: UserGroupDao
    private lateinit var workManager: WorkManager
    private lateinit var username: String
    private lateinit var password: String
    private var login: Boolean = false


    @Before
    fun setUp(){
        hiltRule.inject()
        pinGroupDao = Mockito.mock(PinGroupDao::class.java)
        userDao = Mockito.mock(UserDao::class.java)
        uceServerApiService = Mockito.mock(UceServerApiService::class.java)
        userGroupDao = Mockito.mock(UserGroupDao::class.java)
        workManager = Mockito.mock(WorkManager::class.java)

        userRepository = UserRepositoryImpl(uceServerApiService, sessionManager, userDao, userGroupDao, pinGroupDao, workManager)

        username = "username"
        password = "password"

        runBlocking {
            Mockito.`when`(uceServerApiService.authenticate(
                    AuthRequest(
                            email = username,
                            password = password
                    ))).thenReturn(Response.success(AuthResponse(0L, "token", "test", 5000L, "refresh")))
            Mockito.`when`(uceServerApiService.getUserData()).thenReturn(Response.success(UserDataResponse(0L, "0", username, "admin@uce.nl", 1, 0L, "0")))
            login = userRepository.login(username, password)
        }
    }

    @Test
    fun testLoginSuccessLogoutLoginError(){
        Mockito.verify(workManager).enqueueUniquePeriodicWork(anyString(), MockitoHelper.anyObject(), MockitoHelper.anyObject())
        assertThat(sessionManager.fetchAuthInfo()?.email, `is`(username))
        assertThat(sessionManager.fetchAuthInfo()?.password, `is`(password))
        assertThat(login, `is`(true))

        runBlocking {
            Mockito.`when`(uceServerApiService.leaveUserGroup()).thenReturn(Response.success(null))
            userRepository.logout()
        }
        assertThat(sessionManager.fetchAuthInfo(), `is`(nullValue()))
        assertThat(sessionManager.fetchUserInfo(), `is`(nullValue()))
        Mockito.verify(workManager).cancelUniqueWork(matches(WorkerConstants.REFRESH_TOKEN_NAME))
        
        val newUsername = "newUsername"
        val newPassword = "newPassword"
        runBlocking {
            Mockito.`when`(uceServerApiService.authenticate(
                    AuthRequest(
                            email = newUsername,
                            password = newPassword
                    ))).thenReturn(Response.error(400, "invalid".toResponseBody()))
            login = userRepository.login(newUsername,newPassword)
        }
        assertThat(login, `is`(false))
    }

    @Test
    fun testGetTeam(){
        val teamId = 0L
        val userGroup = UserGroupWithUsers(UserGroup(0L), listOf())
        val retvalTeam: UserGroupWithUsers
        val retvalTeams: Flow<List<UserGroupWithUsers>>
        runBlocking {
            Mockito.`when`(userGroupDao.insert(userGroup.userGroup)).thenReturn(0L)
            Mockito.`when`(userGroupDao.getUserGroupWithUsers(teamId)).thenReturn(userGroup)
            Mockito.`when`(userGroupDao.getUserGroupsWithUsers()).thenReturn(flow { emit(listOf(userGroup)) })
            Mockito.`when`(uceServerApiService.getUserGroups(sessionManager.fetchUserId())).thenReturn(Response.success(listOf(UserGroupResponse(
                    0L, 0L,"", listOf(Member(0L))
                    ))))
            retvalTeam = userRepository.getTeam(teamId)
            userRepository.refreshTeams()
            retvalTeams = userRepository.getTeams()
        }
        assertThat(retvalTeam, `is`(userGroup))
        retvalTeams.asLiveData().getOrAwaitValue().also {
            assertThat(it, `is`(listOf(userGroup)))
        }
        runBlocking {
            Mockito.verify(userGroupDao).deleteAll()
            Mockito.verify(userDao).deleteAll()
            Mockito.verify(userGroupDao).insert(UserGroup(0L, "", 0L))
            Mockito.verify(userDao).insert(User(0L, "Temp name", 0L))
        }
    }

    @Test
    fun testJoinTeamSuccess(){
        val teamId = 0L
        var retval: Boolean
        runBlocking {
            Mockito.`when`(uceServerApiService.joinUserGroup(teamId)).thenReturn(Response.success(null))
            Mockito.`when`(uceServerApiService.getUserGroups(sessionManager.fetchUserId())).thenReturn(Response.error(400, "invalid".toResponseBody() ))
            retval = userRepository.joinTeam(teamId)
        }

        assertThat(retval, `is`(true))
        runBlocking {
            Mockito.verify(uceServerApiService).getUserGroups(sessionManager.fetchUserId())
        }
    }

    @Test
    fun testJoinTeamError(){
        val teamId = 0L
        var retval: Boolean
        runBlocking {
            Mockito.`when`(uceServerApiService.joinUserGroup(teamId)).thenReturn(Response.error(400, "invalid".toResponseBody()))
            retval = userRepository.joinTeam(teamId)
        }

        assertThat(retval, `is`(false))
    }

    @Test
    fun testLeaveTeam(){
        val userGroupId = 0L
        runBlocking {
            Mockito.`when`(userDao.get(sessionManager.fetchUserId())).thenReturn(User(0L, "test", userGroupId))
            Mockito.`when`(uceServerApiService.getUserGroups(sessionManager.fetchUserId())).thenReturn(Response.error(400, "invalid".toResponseBody()))
            userRepository.leaveTeam()
            Mockito.verify(uceServerApiService).leaveUserGroup()
        }
    }

}