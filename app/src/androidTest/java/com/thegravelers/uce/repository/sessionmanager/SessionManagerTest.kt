/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository.sessionmanager

import androidx.test.filters.SmallTest
import com.thegravelers.uce.database.entity.User
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.nullValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import javax.inject.Inject

@HiltAndroidTest
@SmallTest
class SessionManagerTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var sessionManager: SessionManager

    private val token = "test"

    @Before
    fun setUp() {
        hiltRule.inject()
    }

    @Test
    fun saveAuthInfo() {
        val authInfo = AuthInfo(authToken = token)
        sessionManager.saveAuthInfo(authInfo)
        val fetchInfo = sessionManager.fetchAuthInfo()
        assertThat(fetchInfo, `is`(authInfo))
    }

    @Test
    fun clearAuthInfo() {
        saveAuthInfo()
        sessionManager.clearAuthInfo()
        val fetchInfo = sessionManager.fetchAuthInfo()
        assertThat(fetchInfo, `is`(nullValue()))
    }

    @Test
    fun saveDarkMode() {
        sessionManager.saveDarkMode(token)
        val fetchToken = sessionManager.fetchDarkMode()
        assertThat(fetchToken, `is`(token))
    }

    @Test
    fun saveGhostMode() {
        sessionManager.saveGhostMode(token)
        val fetchToken = sessionManager.fetchGhostMode()
        assertThat(fetchToken, `is`(token))
    }

    private val userId = 1L
    private val university = "university"
    private val course = "course"
    private val classId = 2L
    private val pinGroupId = 3L

    @Test
    fun saveUserInfo() {
        val userInfo = UserInfo(userId, university, course, classId, pinGroupId)
        sessionManager.saveUserInfo(userInfo)
        val fetchInfo = sessionManager.fetchUserInfo()
        assertThat(fetchInfo, `is`(userInfo))

        val fetchUserId = sessionManager.fetchUserId()
        assertThat(fetchUserId, `is`(userId))

        val fetchUniversity = sessionManager.fetchUniversity()
        assertThat(fetchUniversity, `is`(university))

        val fetchCourse = sessionManager.fetchCourse()
        assertThat(fetchCourse, `is`(course))

        val fetchClassId = sessionManager.fetchClassId()
        assertThat(fetchClassId, `is`(classId))

        val fetchPinGroupId = sessionManager.fetchPinGroupId()
        assertThat(fetchPinGroupId, `is`(pinGroupId))
    }

    @Test
    fun clearUserInfo() {
        saveUserInfo()
        sessionManager.clearUserInfo()
        val fetchInfo = sessionManager.fetchUserInfo()
        assertThat(fetchInfo, `is`(nullValue()))
    }
}