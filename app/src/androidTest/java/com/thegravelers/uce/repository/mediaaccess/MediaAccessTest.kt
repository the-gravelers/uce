/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository.mediaaccess

import android.graphics.Bitmap
import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

/**
 * Test if images are stored, retrieved and deleted correctly.
 * Should be run on a device below and equal or higher than sdk 29
 */
@HiltAndroidTest
@SmallTest
class MediaAccessTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var mediaAccess: MediaAccess
    private val fileName = "test_file"
    private val folderName = "test_folder"
    private lateinit var storedUri: Uri

    @Before
    fun setUp() {
        hiltRule.inject()
        // Store image on device
        val bitmap = Bitmap.createBitmap(420, 420, Bitmap.Config.ARGB_8888)
        runBlocking {
            mediaAccess.storeImage(fileName, folderName, bitmap, Bitmap.CompressFormat.PNG)?.let {
                storedUri = it
            }
        }
        // Check if image is stored
        assertThat(storedUri, `is`(not(nullValue())))
    }

    @After
    fun tearDown() {
        runBlocking {
            mediaAccess.deleteImage(storedUri)
        }
        // Check if file is deleted
        val exists = runBlocking {
            mediaAccess.exists(storedUri)
        }
        assertFalse(exists)
    }

    @Test
    fun testMediaAccess() {
        // Check if uri is correct
        storedUri.path?.let{
            val exists = runBlocking {
                mediaAccess.exists(storedUri)
            }
            assertTrue(exists)
        }
    }
}