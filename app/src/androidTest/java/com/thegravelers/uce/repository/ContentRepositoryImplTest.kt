/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.core.net.toUri
import androidx.lifecycle.asLiveData
import androidx.test.filters.SmallTest
import com.thegravelers.uce.database.UceDatabase
import com.thegravelers.uce.database.dao.*
import com.thegravelers.uce.repository.mediaaccess.MediaAccess
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.work.*
import androidx.work.impl.OperationImpl
import com.thegravelers.uce.MockitoHelper
import com.thegravelers.uce.R
import com.thegravelers.uce.database.entity.*
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.network.uceserver.*
import com.thegravelers.uce.repository.sessionmanager.UserInfo
import com.thegravelers.uce.util.Caching
import com.thegravelers.uce.util.MediaTypes
import com.thegravelers.uce.util.getCurrentTime
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.hamcrest.CoreMatchers.`is`
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations
import org.mockito.stubbing.Answer
import java.io.File
import javax.inject.Inject

@HiltAndroidTest
@SmallTest
class ContentRepositoryImplTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var sessionManager: SessionManager

    @Mock
    private lateinit var mediaAccess: MediaAccess
    @Mock
    private lateinit var pinDao: PinDao
    @Mock
    private lateinit var pinContentDao: PinContentDao
    @Mock
    private lateinit var pinGroupDao: PinGroupDao
    @Mock
    private lateinit var cachedPinDao: CachedPinDao
    @Mock
    private lateinit var bookDao: BookDao
    @Mock
    private lateinit var contentRepository: ContentRepository
    @Mock
    private lateinit var uceServerApiService: UceServerApiService
    @Mock
    private lateinit var workManager: WorkManager

    private lateinit var pin: Pin
    private lateinit var content: PinContent

    @Before
    fun setUp() {
        hiltRule.inject()
        sessionManager.saveUserInfo(UserInfo())
        MockitoAnnotations.openMocks(this)
        contentRepository = ContentRepositoryImpl(pinDao, pinContentDao, cachedPinDao,
                bookDao, pinGroupDao, sessionManager, uceServerApiService, mediaAccess, workManager)
        pin = Pin("pin_title", text = "pin_text", userId = sessionManager.fetchUserId())
        content = PinContent(uri = "content_uri", pinId = pin.id)

    }

    @After
    fun tearDown() {
        sessionManager.saveUserInfo(UserInfo())
    }

    @Test
    fun testInsertNewPin() {
        runBlocking {
            Mockito.`when`(pinDao.insert(pin)).thenReturn(pin.id)
            Mockito.`when`(pinDao.getAllLocalWithContents()).thenReturn(listOf(PinWithContents(pin, listOf(content))))
            contentRepository.insertNewPin(pin, listOf(content))
            Mockito.verify(pinDao).insert(pin)
            Mockito.verify(pinContentDao).insert(content)
            Mockito.verify(pinDao).getAllLocalWithContents()
            Mockito.verify(workManager).cancelUniqueWork(anyString())
            Mockito.verify(workManager).enqueueUniqueWork(anyString(), MockitoHelper.anyObject(), MockitoHelper.anyObject<OneTimeWorkRequest>())
        }
    }

    @Test
    fun testUpdatePin() {
        val newContent = PinContent(uri = "new_content_uri", pinId = pin.id)
        var count = 0
        runBlocking {
            Mockito.`when`(pinDao.update(pin)).then(Answer { }).also {
                count++
            }
            Mockito.`when`(pinContentDao.insert(newContent)).then(Answer { }).also {
                count++
            }
            Mockito.`when`(pinContentDao.delete(content)).then(Answer { }).also {
                count++
            }
            contentRepository.updatePin(pin, newContent.uri?.let { listOf(it.toUri()) })
        }
        assertThat(count, `is`(3))
    }

    @Test
    fun testGetAllPinsFeed() {
        var retval: Flow<List<ContentFeedItem>>
        runBlocking {
            Mockito.`when`(pinDao.getAllFlow()).thenReturn(flow { emit(listOf(pin)) })
            retval = contentRepository.getAllPinsFeed()
        }
        retval.asLiveData().getOrAwaitValue().also {
            assertThat(it.first().title, `is`(pin.title))
            assertThat(it.first().type, `is`(ContentType.PIN))
        }
    }

    @Test
    fun testGetAllPins() {
        var retval: Flow<List<Pin>>
        runBlocking {
            Mockito.`when`(pinDao.getAllFlow()).thenReturn(flow { emit(listOf(pin)) })
            retval = contentRepository.getAllPins()
        }
        retval.asLiveData().getOrAwaitValue().also {
            assertThat(it.first(), `is`(pin))
        }
    }

    @Test
    fun testGetMyPinsFeed() {
        var retval: Flow<List<ContentFeedItem>>
        runBlocking {
            Mockito.`when`(pinDao.getAllFromUser(pin.userId)).thenReturn(flow { emit(listOf(pin)) })
            retval = contentRepository.getMyPinsFeed()
        }
        retval.asLiveData().getOrAwaitValue().also {
            assertThat(it.first().title, `is`(pin.title))
            assertThat(it.first().type, `is`(ContentType.PIN))
        }
    }

    @Test
    fun testGetSavedPinsFeed() {
        var retval: Flow<List<ContentFeedItem>>
        runBlocking {
            Mockito.`when`(pinDao.getAllSaved()).thenReturn(flow { emit(listOf(pin)) })
            retval = contentRepository.getSavedPinsFeed()
        }
        retval.asLiveData().getOrAwaitValue().also {
            assertThat(it.first().title, `is`(pin.title))
            assertThat(it.first().type, `is`(ContentType.PIN))
        }
    }

    @Test
    fun testGetAllBooksFeed() {
        var retval: Flow<List<ContentFeedItem>>
        val book = Book(0L, "book_title", "preview")
        runBlocking {
            Mockito.`when`(bookDao.getAll()).thenReturn(flow { emit(listOf(book)) })
            retval = contentRepository.getAllBooksFeed()
        }
        retval.asLiveData().getOrAwaitValue().also {
            assertThat(it.first().title, `is`(book.title))
            assertThat(it.first().type, `is`(ContentType.BOOK))
        }
    }

    @Test
    fun testGetBookWithContent() {
        var retval: BookWithContents
        val bookId = 0L
        val book = Book(bookId, "book_title", "preview")
        val bookWithContents = BookWithContents(book, listOf())
        runBlocking {
            Mockito.`when`(bookDao.getBookWithContents(bookId)).thenReturn(bookWithContents)
            retval = contentRepository.getBookWithContent(bookId)
        }
        assertThat(retval, `is`(bookWithContents))
    }

    @Test
    fun testRefreshPins() {
        val localPins = listOf(
                // Perfectly synced pin
                Pin("1", 1.0, 1.0, "content", PinStatus.Nothing, 11L, 100, 1L).apply { id = 1 },
                // Pin that needs to be deleted on the server
                Pin("2", 1.0, 1.0, "content", PinStatus.Deleted, 12L, 100, 1L).apply { id = 2 },
                // Pin that is deleted on the server
                Pin("3", 1.0, 1.0, "content", PinStatus.Nothing, 13L, 100, 1L).apply { id = 3 },
                // Pin that is updated on the server
                Pin("4", 1.0, 1.0, "content", PinStatus.Nothing, 14L, 50, 1L).apply { id = 4 }
        )
        val serverPins = listOf(
                // Perfectly synced pin (nothing should happen)
                GetPinsResponse(11L, 100, "1", 1.0, 1.0, 1L, "content"),
                // Pin that will be deleted
                GetPinsResponse(12L, 100, "2", 1.0, 1.0, 1L, "content"),
                // Pin that is newer than localPin
                GetPinsResponse(14L, 100, "3 new", 1.0, 1.0, 1L, "content new"),
                // Pin that we do not have yet
                GetPinsResponse(15L, 100, "5", 1.0, 1.0, 1L, "content")
        )
        sessionManager.saveUserInfo(UserInfo(userId = 1L, classId = 1L))
        runBlocking {
            Mockito.`when`(uceServerApiService.getPins(eq(null), eq(null), anyLong()))
                    .thenReturn(retrofit2.Response.success(serverPins))
            Mockito.`when`(uceServerApiService.getPins(anyLong(), eq(null), eq(null)))
                    .thenReturn(retrofit2.Response.success(emptyList()))
            Mockito.`when`(pinDao.getAll())
                    .thenReturn(localPins)
            Mockito.`when`(pinContentDao.getAll(anyLong()))
                    .thenReturn(emptyList())
            Mockito.`when`(pinDao.getWithServerId(11L))
                    .thenReturn(localPins[0])
            Mockito.`when`(pinDao.getWithServerId(12L))
                    .thenReturn(localPins[1])
            Mockito.`when`(pinDao.getWithServerId(14L))
                    .thenReturn(localPins[3])
            Mockito.`when`(pinDao.getWithServerId(15L))
                    .thenReturn(null)
            Mockito.`when`(pinDao.getDeletedPins())
                    .thenReturn(listOf(localPins[1]))
            Mockito.`when`(uceServerApiService.deletePin(anyLong()))
                    .thenReturn(retrofit2.Response.success(null))

            contentRepository.refreshPins()

            Mockito.verify(pinDao).delete(localPins[2])
            Mockito.verify(pinDao).update(localPins[3].apply {
                title = serverPins[2].title
                text = serverPins[2].text
                lastEdited = serverPins[2].lastEdited
            })
            Mockito.verify(pinDao).insert(Pin(
                    serverPins[3].title,
                    serverPins[3].latitude,
                    serverPins[3].longitude,
                    serverPins[3].text,
                    PinStatus.Nothing,
                    serverPins[3].id,
                    serverPins[3].lastEdited,
                    serverPins[3].userId))
            Mockito.verify(uceServerApiService).deletePin(localPins[1].serverId)
            Mockito.verify(pinDao).delete(localPins[1])
        }
    }

    @Test
    fun testDownloadBooks() {
        val localBooks = listOf(
                // A book with 1 section, which has text and an image.
                // This book is outdated, thus it should be deleted when downloading books
                BookWithContents(Book(1L, "Book 1", "This is a preview", 50L),
                        listOf(BookSectionWithContents(BookSection(1L, 1L, "Section 1", 0),
                                listOf(BookContent(1L, 1L, "this is text", BookContentType.TEXT, 0),
                                        BookContent(2L, 1L, "uri/image1.png", BookContentType.IMAGE, 1)))))
        )
        val serverBooks = listOf(
                // Newer book
                GetBookResponse(1L, "Book 1", 100L, listOf(
                        GetBookSectionResponse(1L, 1L, "Section 1", 0, listOf(
                                GetBookContentResponse(1L, 1L, "text", 0)
                        ))
                )),
                // Brand new book
                GetBookResponse(2L, "Book 2", 100L, listOf(
                        GetBookSectionResponse(2L, 2L, "Section 1", 0, listOf(
                                GetBookContentResponse(3L, 2L, "image", 0)
                        ))
                ))
        )

        runBlocking{
            Mockito.`when`(uceServerApiService.getBooks())
                    .thenReturn(retrofit2.Response.success(listOf(
                            GetBooksResponse(serverBooks[0].id, serverBooks[0].lastEdited),
                            GetBooksResponse(serverBooks[1].id, serverBooks[1].lastEdited)
                    )))
            Mockito.`when`(uceServerApiService.getBook(1L))
                    .thenReturn(retrofit2.Response.success(serverBooks[0]))
            Mockito.`when`(uceServerApiService.getBook(2L))
                    .thenReturn(retrofit2.Response.success(serverBooks[1]))
            Mockito.`when`(bookDao.get(1L))
                    .thenReturn(localBooks[0].book)
            Mockito.`when`(bookDao.getBookWithContents(1L))
                    .thenReturn(localBooks[0])
            Mockito.`when`(uceServerApiService.getBookContent(anyLong()))
                    .thenReturn(retrofit2.Response.error(400,
                            "Bad request".toResponseBody()))

            contentRepository.downloadAllBooks()

            Mockito.verify(mediaAccess).deleteImage("uri/image1.png".toUri())
            Mockito.verify(bookDao).delete(localBooks[0].book)
            Mockito.verify(bookDao).insert(Book(
                    serverBooks[0].id, serverBooks[0].title, "Preview", serverBooks[0].lastEdited
            ))
            Mockito.verify(bookDao).insert(Book(
                    serverBooks[1].id, serverBooks[1].title, "Preview", serverBooks[1].lastEdited
            ))
            Mockito.verify(bookDao).insertSection(BookSection(
                    serverBooks[0].sections[0].bookSectionId, serverBooks[0].sections[0].bookId,
                    serverBooks[0].sections[0].title, serverBooks[0].sections[0].sectionNr
            ))
            Mockito.verify(bookDao).insertSection(BookSection(
                    serverBooks[1].sections[0].bookSectionId, serverBooks[1].sections[0].bookId,
                    serverBooks[1].sections[0].title, serverBooks[1].sections[0].sectionNr
            ))
        }
    }

    @Test
    fun testSavePin(){
        val pin = Pin("Me Pin", status = PinStatus.Nothing).apply{id = 1L}
        Mockito.`when`(pinDao.get(1L))
                .thenReturn(pin)

        runBlocking{
            // Confirm that we cannot unsave this pin
            contentRepository.unsavePin(pin.id)
            Mockito.verify(pinDao, never()).update(pin)

            // Save the pin
            contentRepository.savePin(pin.id)
            assertThat(pin.status, `is`(PinStatus.Saved))
            Mockito.verify(pinDao).update(pin)
            Mockito.verify(cachedPinDao).delete(pin.id)

            // Confirm that we cannot save this pin again
            contentRepository.savePin(pin.id)
            Mockito.verify(pinDao, times(1)).update(pin)

            // Unsave the pin
            contentRepository.unsavePin(pin.id)
            assertThat(pin.status, `is`(PinStatus.Nothing))
            Mockito.verify(pinDao, times(2)).update(pin)
        }
    }

    @Test
    fun testCachedPin(){
        val firstPin = Pin().apply{id = 1}
        val firstCachedPin = CachedPin(firstPin.id, 100L)
        val pins = mutableListOf(CachedPinWithInfo(firstCachedPin, pin))

        runBlocking{
            Mockito.`when`(pinContentDao.getAll(anyLong()))
                    .thenReturn(emptyList())
            Mockito.`when`(cachedPinDao.getCachedPins())
                    .thenReturn(pins.toList())

            contentRepository.cachePin(firstPin.id, 100L)
            Mockito.verify(cachedPinDao).insert(firstCachedPin)
            Mockito.verify(cachedPinDao, never()).delete(firstPin.id)

            for(i in 2L..4L)
            {
                val timestamp = 100L + 50L*i
                val pin = Pin().apply{id = i}
                val cachedPin = CachedPin(pin.id, timestamp)
                pins.add(CachedPinWithInfo(cachedPin, pin))

                Mockito.`when`(cachedPinDao.getCachedPins())
                        .thenReturn(pins.toList())

                contentRepository.cachePin(pin.id, timestamp)
                Mockito.verify(cachedPinDao).insert(cachedPin)
            }

            // We have a total of 4 cached pins, so the first one should have been deleted
            Mockito.verify(cachedPinDao).delete(firstCachedPin)
        }
    }

    @Test
    fun testDeleteUserPin(){
        val pin = Pin().apply{id = 1L}
        Mockito.`when`(pinDao.get(1L))
                .thenReturn(pin)
        Mockito.`when`(pinContentDao.getAll(1L))
                .thenReturn(emptyList())

        runBlocking{
            contentRepository.deleteUserPin(pin.id)
            assertThat(pin.status, `is`(PinStatus.Deleted))
            Mockito.verify(pinDao).update(pin)
        }
    }

    @Test
    fun testGetPinContent_NoResponse(){
        val pin = Pin(serverId = 10L, lastEdited = 50L, status = PinStatus.Nothing).apply{id = 1L}
        val pinContents = listOf(PinContent(1L, "", 0, "image"))

        runBlocking {
            Mockito.`when`(uceServerApiService.getPin(pin.serverId))
                    .thenReturn(retrofit2.Response.error(400,
                            "Bad request".toResponseBody()))
            Mockito.`when`(pinDao.getPinWithContents(pin.id))
                    .thenReturn(PinWithContents(pin, pinContents))

            val retval = contentRepository.getPinContent(pin.id)

            assertThat(retval, `is`(pinContents))
        }
    }

    @Test
    fun  testGetPinContent_NoNewContent(){
        val pin = Pin(serverId = 10L, lastEdited = 100L, status = PinStatus.Nothing).apply{id = 1L}
        val pinContents = listOf(PinContent(1L, "", 0, "image"))

        val serverPin = GetPinResponse(10L, 100L, "", 1.0, 1.0, 1L, "", listOf(
                PinContentInfo(1L, "image", 0)))

        runBlocking {
            Mockito.`when`(uceServerApiService.getPin(pin.serverId))
                    .thenReturn(retrofit2.Response.success(serverPin))
            Mockito.`when`(pinDao.getPinWithContents(pin.id))
                    .thenReturn(PinWithContents(pin, pinContents))
            Mockito.`when`(cachedPinDao.getCachedPins())
                    .thenReturn(listOf(CachedPinWithInfo(CachedPin(pin.id, 100L), pin)))

            val retval = contentRepository.getPinContent(pin.id)

            assertThat(retval, `is`(pinContents))
        }
    }

    @Test
    fun testGetPin(){
        val pin = Pin().apply{id = 1L}
        Mockito.`when`(pinDao.get(pin.id))
                .thenReturn(pin)
        runBlocking{
            val gettedPin = contentRepository.getPin(pin.id)
            assertThat(gettedPin, `is`(pin))
        }
    }
}
