/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce

import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import com.thegravelers.uce.database.entity.*
import com.thegravelers.uce.di.RepositoryModule
import com.thegravelers.uce.repository.*
import com.thegravelers.uce.repository.ContentType
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.stubbing.Answer

@Module
@TestInstallIn(
        components = [SingletonComponent::class],
        replaces = [RepositoryModule::class]
)
object FakeRepositoryModule {

    lateinit var bookWithContents: BookWithContents
    lateinit var title: String
    lateinit var pin: Pin
    lateinit var pinWithContents: PinWithContents
    lateinit var contentRepository: ContentRepository
    lateinit var team: UserGroupWithUsers
    lateinit var userRepository: UserRepository

    @Provides
    fun provideUserRepository(impl: UserRepositoryImpl): UserRepository{
        val userRepository = Mockito.mock(UserRepository::class.java)
        if(this@FakeRepositoryModule::team.isInitialized){
            runBlocking {
                Mockito.`when`(userRepository.login("valid", "valid")).thenReturn(true)
                Mockito.`when`(userRepository.login("invalid", "invalid")).thenReturn(false)
                Mockito.`when`(userRepository.getTeam(team.userGroup.id)).thenReturn(team)
                Mockito.`when`(userRepository.getTeamIdFromUser()).thenReturn(team.userGroup.id)
                Mockito.`when`(userRepository.joinTeam(team.userGroup.id)).thenReturn(true)
                Mockito.`when`(userRepository.getTeams()).thenReturn(flow { emit(listOf(team)) })
                Mockito.`when`(userRepository.refreshTeams()).thenReturn(null)
            }
        }
        this.userRepository = userRepository
        return userRepository
    }

    @Provides
    fun provideContentRepository(impl: ContentRepositoryImpl): ContentRepository{
        val contentRepository = Mockito.mock(ContentRepository::class.java)

        runBlocking {
            Mockito.`when`(contentRepository.getAllPins()).thenReturn(flow { emit(listOf<Pin>()) })
            if(this@FakeRepositoryModule::title.isInitialized) {
                val list = mutableListOf(ContentFeedItem(0L, ContentType.PIN, title, 0))
                Mockito.`when`(contentRepository.getAllPinsFeed()).thenReturn(flow { emit(list) })
                Mockito.`when`(contentRepository.getAllBooksFeed()).thenReturn(flow { emit(list) })
                Mockito.`when`(contentRepository.getMyPinsFeed()).thenReturn(flow { emit(list) })
                Mockito.`when`(contentRepository.getSavedPinsFeed()).thenReturn(flow { emit(list) })
            }


            if(this@FakeRepositoryModule::bookWithContents.isInitialized) {
                Mockito.`when`(contentRepository.getBookWithContent(bookWithContents.book.id)).thenReturn(bookWithContents)
            }

            if(this@FakeRepositoryModule::pin.isInitialized && this@FakeRepositoryModule::pinWithContents.isInitialized){
                Mockito.`when`(contentRepository.getPinContent(pin.id)).thenReturn(pinWithContents.contents)
                Mockito.`when`(contentRepository.getPinContent(-1L)).thenReturn(null)
                Mockito.`when`(contentRepository.getPin(pin.id)).thenReturn(pin)
            }

        }
        this.contentRepository = contentRepository
        return contentRepository
    }

    fun verifyDeletePin(shouldCall: Boolean){
        runBlocking {
            if(shouldCall) {
                Mockito.verify(contentRepository).deleteUserPin(0L)
            }
            else {
                Mockito.verify(contentRepository, never()).deleteUserPin(0L)
            }
        }
    }

    fun verifyUpdatePin(updatedPin: Pin){
        runBlocking {
            Mockito.verify(contentRepository).updatePin(ArgumentMatchers.eq(updatedPin)
                    ?: updatedPin, ArgumentMatchers.eq(listOf("content".toUri(), "updatedContent".toUri())))
        }
    }


}