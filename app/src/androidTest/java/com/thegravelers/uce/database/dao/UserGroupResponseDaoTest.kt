/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asLiveData
import androidx.test.filters.SmallTest
import com.thegravelers.uce.database.UceDatabase
import com.thegravelers.uce.database.entity.User
import com.thegravelers.uce.database.entity.UserGroup
import com.thegravelers.uce.getOrAwaitValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
@SmallTest
class UserGroupResponseDaoTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var database: UceDatabase

    private lateinit var userGroupDao: UserGroupDao
    private lateinit var userDao: UserDao
    private lateinit var pinGroupDao: PinGroupDao

    @Before
    fun setUp() {
        hiltRule.inject()
        userGroupDao = database.userGroupDao()
        userDao = database.userDao()
        pinGroupDao = database.pinGroupDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun testGetUserGroupWithUsers() = runBlocking{
        val group1 = 1L
        val group2 = 2L
        val groups = listOf(
                UserGroup(group1),
                UserGroup(group2)
        )
        val users = listOf(
                User(1, userGroupId = group1),
                User(2, userGroupId = group1),
                User(3, userGroupId = group2),
                User(4, userGroupId = group2),
        )

        userGroupDao.insert(groups)
        userDao.insert(users)

        val getUserGroupWithUsers1 = userGroupDao.getUserGroupWithUsers(group1)
        for(ug in getUserGroupWithUsers1.users){
            assertThat(ug.userGroupId, `is`(group1))
        }

        val getUserGroupWithUsers2 = userGroupDao.getUserGroupWithUsers(group2)
        for(ug in getUserGroupWithUsers2.users){
            assertThat(ug.userGroupId, `is`(group2))
        }
    }

    
}