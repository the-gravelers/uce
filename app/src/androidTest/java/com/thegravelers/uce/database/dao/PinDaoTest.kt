/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.database.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asLiveData
import androidx.test.filters.SmallTest
import com.thegravelers.uce.database.UceDatabase
import com.thegravelers.uce.database.entity.*
import com.thegravelers.uce.getOrAwaitValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInAnyOrder
import org.hamcrest.collection.IsIterableContainingInOrder
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import kotlin.random.Random

@HiltAndroidTest
@SmallTest
class PinDaoTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var database: UceDatabase

    private lateinit var pinDao: PinDao
    private lateinit var pinContentDao: PinContentDao
    private lateinit var pinGroupDao: PinGroupDao
    private lateinit var cachedPinDao: CachedPinDao

    private fun getAllPins(): List<Pin> {
        return pinDao.getAllFlow().asLiveData().getOrAwaitValue()
    }

    @Before
    fun setUp() {
        hiltRule.inject()
        //Dispatchers.setMain(TestCoroutineDispatcher())
        pinDao = database.pinDao()
        pinContentDao = database.pinContentDao()
        pinGroupDao = database.pinGroupDao()
        cachedPinDao = database.cachedPinDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun testEmptyDatabase_ReturnsNothing() {
        val allPins = pinDao.getAllFlow().asLiveData().getOrAwaitValue()
        assertThat(allPins.size, `is`(0))
    }


    @Test
    fun testInsertDeletePin_SinglePin() = runBlocking {
        val pin = Pin("Test Pin", 15.0, 30.0, "text")

        // Insert the pin and retrieve all pins
        val id: Long = pinDao.insert(pin)
        var allSyncedPins: List<Pin> = getAllPins()

        // All pins should contain only the inserted pin
        assertThat(allSyncedPins.contains(pin), `is`(true))
        assertThat(allSyncedPins.size, `is`(1))
        assertThat(allSyncedPins.first().id, `is`(id))

        // Delete inserted pin
        pinDao.delete(id)
        allSyncedPins = getAllPins()

        // Check if the pin has been deleted
        assertThat(allSyncedPins.isEmpty(), `is`(true))
    }

    @Test
    fun testInsertDelete_MultiplePins() = runBlocking {
        val pins: Array<Pin> = arrayOf(
                Pin(),
                Pin(),
                Pin(),
                Pin())

        // Insert the pins
        pinDao.insert(pins.toList())
        var allPins = getAllPins()

        // Check if the lists contain the same pins with the spread (*) operator
        assertThat(allPins, IsIterableContainingInAnyOrder.containsInAnyOrder(*pins))

        // Delete all the pins
        pinDao.deleteAll()
        allPins = getAllPins()

        assertThat(allPins.isEmpty(), `is`(true))
    }

    /**
     * Tests whether the PinWithContents doesn't select content from other pins
     */
    @Test
    fun testPinWithContent_NoOtherContent() = runBlocking {
        val pins = listOf(Pin(), Pin())
        val ids = pinDao.insert(pins)
        // The content has two entries that belong to the pin, and one that belongs to another pin
        val contentOneSynced: Array<PinContent> = arrayOf(
                PinContent(ids[0]),
                PinContent(ids[0]))
        val contentTwo = PinContent(ids[1])

        // Insert the pins and the content and select the first pin with content
        pinContentDao.insert(contentOneSynced.toList())
        pinContentDao.insert(contentTwo)
        val pinOneWithContent = pinDao.getPinWithContents(1)
        val pinTwoWithContent = pinDao.getPinWithContents(2)

        // Assert that the first pin has two pieces of content
        assertThat(pinOneWithContent.pin, `is`(pins[0]))
        assertThat(pinOneWithContent.contents, IsIterableContainingInAnyOrder.containsInAnyOrder(*contentOneSynced))
        // Assert that the second pin has one piece of content
        assertThat(pinTwoWithContent.pin, `is`(pins[1]))
        assertThat(pinTwoWithContent.contents.size, `is`(1))
        assertThat(pinTwoWithContent.contents[0], `is`(contentTwo))
    }

    @Test
    fun testPinsPerPinGroup() = runBlocking {
        val pinGroups = arrayOf(
                PinGroup(1),
                PinGroup(2))
        val pinsOne = arrayOf(
                Pin(),
                Pin())
        val pinsTwo = arrayOf(
                Pin(),
                Pin())

        // Insert the pins, pingroups
        val pinsOneIds = pinDao.insert(pinsOne.toList())
        val pinsTwoIds = pinDao.insert(pinsTwo.toList())
        pinGroupDao.insert(pinGroups.toList())

        // Create and insert the relations
        val pinPerPinGroup = arrayOf(
                PinPerPinGroup(pinsOneIds[0], 1),
                PinPerPinGroup(pinsTwoIds[0], 2),
                PinPerPinGroup(pinsOneIds[1], 1),
                PinPerPinGroup(pinsTwoIds[1], 2))

        pinGroupDao.insertPinRelation(pinPerPinGroup.toList())

        // Select the pins per pin group
        val pGrOneWithPins = pinGroupDao.getPinGroupsWithPins(1).asLiveData().getOrAwaitValue()[0].pins
        val pGrTwoWithPins = pinGroupDao.getPinGroupsWithPins(2).asLiveData().getOrAwaitValue()[0].pins

        val pinGroupOneIds = pGrOneWithPins.map { it.id }.toTypedArray()
        val pinGroupTwoIds = pGrTwoWithPins.map { it.id }.toTypedArray()

        // Assert that both pin groups have the correct lists
        assertThat(pGrOneWithPins.map { it.id }, IsIterableContainingInAnyOrder.containsInAnyOrder(*(pinGroupOneIds)))
        assertThat(pGrTwoWithPins.map { it.id }, IsIterableContainingInAnyOrder.containsInAnyOrder(*(pinGroupTwoIds)))
    }

    @Test
    fun testGetOldestPins() = runBlocking {
        val limit = 5
        val pins = arrayOf(
                Pin(),
                Pin(),
                Pin(),
                Pin(),
                Pin())
        val pinIds = pinDao.insert(pins.toList())
        val newestPin = Pin()
        val newestPinId = pinDao.insert(newestPin)

        // The cachedPins should have an increasing timestamp
        val cachedPins = arrayOf(
                CachedPin(pinIds[0]),
                CachedPin(pinIds[1]),
                CachedPin(pinIds[2]),
                CachedPin(pinIds[3]),
                CachedPin(pinIds[4]))
        val newestCachedPin = CachedPin(newestPinId)

        cachedPinDao.insert(cachedPins.toList())
        cachedPinDao.insert(newestCachedPin)

        val oldestCachedPins = cachedPinDao.getOldestPins(limit).asLiveData().getOrAwaitValue()
        val oldestPins = oldestCachedPins.map { it.pin.id }

        assertThat(oldestPins.size, `is`(limit))
        assertThat(oldestPins, IsIterableContainingInOrder.contains(*pinIds.toTypedArray()))
        assertThat(oldestPins.contains(newestPinId), `is`(false))
    }

    @Test
    fun testGetAllSaved() = runBlocking {
        // savedPins defines how many of total pins should have saved = true
        val totalPins = 5
        val savedPins = 3

        // Fill list
        val pins = mutableListOf<Pin>()
        for (i in 1..totalPins) {
            if (i <= savedPins) {
                pins.add(Pin(status = PinStatus.Saved))
            } else {
                pins.add(Pin())
            }
        }

        pinDao.insert(pins)
        val saved = pinDao.getAllSaved().asLiveData().getOrAwaitValue()
        assertThat(saved.size, `is`(savedPins))
    }

    @Test
    fun testGetAllLocal() = runBlocking {
        // localPins defines how many of total pins should have server_id = null
        val totalPins = 5
        val localPins = 3

        // Fill list
        val pins = mutableListOf<Pin>()
        for (i in 1..totalPins) {
            if (i <= localPins) {
                pins.add(Pin())
            } else {
                pins.add(Pin(serverId = i.toLong()))
            }
        }

        pinDao.insert(pins)
        val local = pinDao.getAllLocalWithContents().map { it.pin }
        assertThat(local.size, `is`(localPins))
    }

    @Test
    fun testDelete_SinglePin(): Unit = runBlocking {
        val totalPins = 5
        // idToDelete has to a Long value between 0 - totalPins
        val idToDelete = 2L
        for (i in 1..totalPins) {
            pinDao.insert(Pin())
        }

        // Assert that pin is inserted correctly
        val notNull = pinDao.getFlow(idToDelete).asLiveData().getOrAwaitValue()
        assertThat(notNull.id, `is`(idToDelete))

        // Assert that pins is deleted correctly
        pinDao.delete(idToDelete)
        val isNull = pinDao.getFlow(idToDelete).asLiveData().getOrAwaitValue()
        assertThat(isNull, `is`(nullValue()))
    }

    @Test
    fun testDelete_MultiplePin() = runBlocking {
        val totalPins = 10
        // idsToDelete has to a Long value between 0 - totalPins
        for (i in 1..totalPins) {
            pinDao.insert(Pin())
        }

        // Get some idsToDelete
        val idsToDelete = mutableListOf<Pin>()
        for (i in 1..totalPins step 2) {
            val pin = pinDao.getFlow(i.toLong()).asLiveData().getOrAwaitValue()
            idsToDelete.add(pin)
        }

        // Assert that pins are delete correctly
        pinDao.delete(*idsToDelete.toTypedArray())
        for (i in idsToDelete) {
            val isNull = pinDao.getFlow(i.id).asLiveData().getOrAwaitValue()
            assertThat(isNull, `is`(nullValue()))
        }
    }

    @Test
    fun testUpdate_SinglePin() = runBlocking {
        val firstTitle = "first"
        val newTitle = "new"
        val pin = Pin(title = firstTitle)
        val pinId = pinDao.insert(pin)

        val getPin = pinDao.getFlow(pinId).asLiveData().getOrAwaitValue()
        // We know that there is only one pin inserted
        getPin.title = newTitle
        pinDao.update(getPin)

        val update = pinDao.getFlow(getPin.id).asLiveData().getOrAwaitValue()
        assertThat(update.title, `is`(newTitle))
    }

    @Test
    fun testUpdate_MultiplePin() = runBlocking {
        val firstTitle = "first"
        val newTitle = "new"
        val totalPins = 10

        // Insert
        val updateIds = mutableListOf<Long>()
        for (i in 1..totalPins) {
            if (Random.nextInt(100) < 50) {
                val id = pinDao.insert(Pin(title = firstTitle))
                updateIds.add(id)
            } else {
                pinDao.insert(Pin(title = firstTitle))
            }
        }

        // Update
        var allPins = getAllPins()
        for (p in allPins) {
            if (updateIds.contains(p.id)) {
                p.title = newTitle
            }
        }
        pinDao.update(*allPins.toTypedArray())

        // Assert
        allPins = getAllPins()
        for (p in allPins) {
            if (updateIds.contains(p.id)) {
                assertThat(p.title, `is`(newTitle))
            } else {
                assertThat(p.title, `is`(firstTitle))
            }
        }
    }

    @Test
    fun testGetAllFromUser() = runBlocking {
        val userId = 1L
        val totalPins = Random.nextInt(10, 100)
        val userPins = Random.nextInt(5, totalPins)
        val pins = mutableListOf<Pin>()
        for (t in 1..totalPins) {
            if (t <= userPins) {
                pins.add(Pin(userId = userId))
            } else {
                pins.add(Pin())
            }
        }
        pinDao.insert(pins)

        val getUserPins = pinDao.getAllFromUser(userId).asLiveData().getOrAwaitValue()
        assertThat(getUserPins.size, `is`(userPins))
        for (up in getUserPins) {
            assertThat(up.userId, `is`(userId))
        }
    }

    @Test
    fun testGetWithServerId() = runBlocking {
        val serverId = 1L
        val title = "serverPin"
        pinDao.insert(Pin(serverId = serverId, title = title))
        val getServerPin = pinDao.getWithServerId(serverId)
        assertThat(getServerPin?.serverId, `is`(serverId))
        assertThat(getServerPin?.title, `is`(title))
    }
}
