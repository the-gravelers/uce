/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.profile

import androidx.test.platform.app.InstrumentationRegistry
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.repository.UserRepository
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ProfileViewModelTest{
    @Mock
    lateinit var userRepository: UserRepository

    lateinit var viewModel: ProfileViewModel

    @Before
    fun setUp(){
        MockitoAnnotations.openMocks(this)
        viewModel = ProfileViewModel(userRepository)
    }

    @Test
    fun testNavigationFunctions(){
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            viewModel.navigateToLogin()
            viewModel.navigateToLogin.getOrAwaitValue().also{
                assertThat(it, `is`(true))
            }
            viewModel.doneNavigatingToLogin()
            viewModel.navigateToLogin.getOrAwaitValue().also{
                assertThat(it, `is`(false))
            }

            viewModel.navigateToMyPins()
            viewModel.navigateToMyPins.getOrAwaitValue().also{
                assertThat(it, `is`(true))
            }
            viewModel.doneNavigatingToMyPins()
            viewModel.navigateToMyPins.getOrAwaitValue().also{
                assertThat(it, `is`(false))
            }

            viewModel.navigateToSavedPins()
            viewModel.navigateToSavedPins.getOrAwaitValue().also{
                assertThat(it, `is`(true))
            }
            viewModel.doneNavigatingToSavedPins()
            viewModel.navigateToSavedPins.getOrAwaitValue().also{
                assertThat(it, `is`(false))
            }
        }
    }

    @Test
    fun testLogout(){
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            viewModel.logout()
            runBlocking {
                Mockito.verify(userRepository).logout()
            }
            viewModel.navigateToLogin.getOrAwaitValue().also{
                assertThat(it, `is`(true))
            }
        }
    }
}