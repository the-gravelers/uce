/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.R
import com.thegravelers.uce.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class BlankFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var blankFragment: BlankFragment
    private lateinit var navHostController: TestNavHostController

    @Before
    fun setUp(){
        hiltRule.inject()

        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()
        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)
        navHostController = TestNavHostController(ApplicationProvider.getApplicationContext())
        launchFragmentInHiltContainer<BlankFragment> {
            blankFragment = this as BlankFragment
            setUpNavHostController(blankFragment, R.id.blankFragment)
        }
    }

    private fun setUpNavHostController(fragment: Fragment, fragmentRId: Int) {
        val inflater = navHostController.navInflater
        val graph = inflater.inflate(R.navigation.companion_navigation)
        graph.setStartDestination(fragmentRId)
        navHostController.graph = graph
        Navigation.setViewNavController(fragment.requireView(), navHostController)
    }

    @Test
    fun testNavigateToGallery(){
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            blankFragment.findNavController().navigate(BlankFragmentDirections.actionGlobalCameraFragment())
            val currentDestId = blankFragment.findNavController().currentDestination?.id
            assertThat(currentDestId, `is`(R.id.cameraFragment))
        }
    }
}
