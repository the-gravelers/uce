/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.teamcode

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.NavHostController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeDown
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.repository.ContentRepository
import com.thegravelers.uce.screens.companion.map.MapFragment
import com.thegravelers.uce.screens.companion.map.MapViewModel
import com.thegravelers.uce.screens.companion.map.geologic.GeologicInfoViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.thegravelers.uce.*
import com.thegravelers.uce.database.entity.User
import com.thegravelers.uce.database.entity.UserGroup
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.repository.sessionmanager.UserInfo
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.atLeast
import javax.inject.Inject

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class StartTeamListFragmentTest{

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var sessionManager: SessionManager

    private lateinit var startTeamListFragment: StartTeamListFragment
    private lateinit var startTeamListViewModel: StartTeamListViewModel
    private lateinit var navHostController: NavHostController

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        FakeRepositoryModule.team = UserGroupWithUsers(UserGroup(0L, "userGroupName"), listOf(User(0L, "username")))

        hiltRule.inject()

        sessionManager.saveUserInfo(UserInfo())



        launchFragmentInHiltContainer<StartTeamListFragment>() {

            startTeamListFragment = this as StartTeamListFragment
            startTeamListViewModel = startTeamListFragment.viewModel
        }
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            navHostController = TestNavHostController(
                    ApplicationProvider.getApplicationContext())
            val inflater = navHostController.navInflater
            val graph = inflater.inflate(R.navigation.start_navigation)
            graph.startDestination = R.id.teamListFragment
            navHostController.graph = graph
            Navigation.setViewNavController(startTeamListFragment.requireView(), navHostController)
        }
    }

    @Test
    fun testGetTeams(){
        runBlocking {
            Mockito.verify(FakeRepositoryModule.userRepository).refreshTeams()
        }
        onView(withText("username")).check(matches(isDisplayed()))
        onView(withText("userGroupName")).check(matches(isDisplayed()))
    }

    @Test
    fun testJoinTeam(){
        onView(withId(R.id.Join_team_button)).perform(click())
        Thread.sleep(1000)
        assertThat(navHostController.currentDestination, `is`(navHostController.graph.findNode(R.id.startTeamFragment)))
    }

    @Test
    fun testSwipeRefresh(){
        onView(withId(R.id.start_team_list_fragment)).perform(swipeDown())
        runBlocking {
            Mockito.verify(FakeRepositoryModule.userRepository, atLeast(2)).refreshTeams()
        }
    }

    @Test
    fun testExpandButton(){
        onView(withId(R.id.Expand_button)).perform(click())
        onView(withText("username")).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.Expand_button)).perform(click())
        onView(withText("username")).check(matches(isDisplayed()))
    }

}