/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.gallery

import android.net.Uri
import android.util.Log
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.R
import com.thegravelers.uce.launchFragmentInCompanionActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class GalleryFragmentTest {
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var galleryFragment: GalleryFragment
    private lateinit var galleryViewModel: SharedGalleryViewModel

    @Before
    fun setUp(){
        hiltRule.inject()
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        launchFragmentInCompanionActivity<GalleryFragment> {
            galleryFragment = this as GalleryFragment
            galleryViewModel = galleryFragment.sharedViewModel
            // Add image to delete
            galleryViewModel.addImage(Uri.EMPTY)
        }
        assert(galleryViewModel.images.value?.size == 1)
    }

    @Test
    fun test_deleteImage() {
        onView(withId(R.id.delete_button)).perform(click())
        onView(withText("Yes"))
                .inRoot(isDialog())
                .check(matches(isDisplayed()))
                .perform(click())

        assert(galleryViewModel.images.value?.size == 0)
    }
}