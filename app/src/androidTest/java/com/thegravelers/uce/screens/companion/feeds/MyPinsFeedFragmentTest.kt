/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import android.util.Log
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.thegravelers.uce.FakeRepositoryModule
import com.thegravelers.uce.R
import com.thegravelers.uce.launchFragmentInCompanionActivity
import com.thegravelers.uce.screens.companion.camera.CameraFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.WorkManager
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.launchFragmentInHiltContainer
import com.thegravelers.uce.repository.ContentRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.anyLong
import org.mockito.Mockito.never
import javax.inject.Inject

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class MyPinsFeedFragmentTest: FeedFragmentTest(){

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    override fun setUp(){
        super.setUp()
        launchFragmentInHiltContainer<MyPinsFeedFragment>() {
            feedFragment = this as MyPinsFeedFragment
            setUpNavHostController(R.id.myPinsFeedFragment)
            feedViewModel = feedFragment.activityViewModels<MyPinsFeedViewModel>().value
        }
        returnLabel = "PinFragment"
    }

    @Test
    fun testSwipeCallbackYes(){
        onView(withText("content_title")).check(matches(isDisplayed())).perform(ViewActions.swipeRight())
        Thread.sleep(1000)
        onView(withText("Yes")).check(matches(isDisplayed())).perform(ViewActions.click())
        Thread.sleep(1000)
        FakeRepositoryModule.verifyDeletePin(true)
    }

    @Test
    fun testSwipeCallbackNo(){
        onView(withText("content_title")).check(matches(isDisplayed())).perform(ViewActions.swipeRight())
        Thread.sleep(1000)
        onView(withText("No")).check(matches(isDisplayed())).perform(ViewActions.click())
        Thread.sleep(1000)
        FakeRepositoryModule.verifyDeletePin(false)
    }
}