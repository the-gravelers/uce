/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.splashscreen

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.R
import com.thegravelers.uce.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

//@HiltAndroidTest
//@LargeTest
//@RunWith(AndroidJUnit4::class)
//class SplashScreenFragmentTest {
//
//    @get:Rule
//    var hiltRule = HiltAndroidRule(this)
//
//    private lateinit var splashScreenFragment: SplashScreenFragment
//    private lateinit var splashScreenViewModel: SplashScreenViewModel
//    private lateinit var navHostController: TestNavHostController
//
//    @Before
//    fun setUp(){
//        hiltRule.inject()
//        val context = InstrumentationRegistry.getInstrumentation().targetContext
//        val config = Configuration.Builder()
//                .setMinimumLoggingLevel(Log.DEBUG)
//                .setExecutor(SynchronousExecutor())
//                .build()
// TODO For some reason this launchFragment.. will give a error that LinearLayout does not have a NavController set
//        // Initialize WorkManager for instrumentation tests.
//        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)
//        launchFragmentInHiltContainer<SplashScreenFragment> {
//            splashScreenFragment = this as SplashScreenFragment
//            setUpNavHostController(splashScreenFragment, R.id.splashScreenFragment)
//            splashScreenViewModel = splashScreenFragment.viewModel
//        }
//    }
//
//    private fun setUpNavHostController(fragment: Fragment, fragmentRId: Int) {
//        navHostController = TestNavHostController(ApplicationProvider.getApplicationContext())
//        val inflater = navHostController.navInflater
//        val graph = inflater.inflate(R.navigation.start_navigation)
//        graph.setStartDestination(fragmentRId)
//        navHostController.graph = graph
//        Navigation.setViewNavController(fragment.requireView(), navHostController)
//    }
//
//    @Test
//    fun testLaunchFragment(){
//
//    }
//}