/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map.geologic

import android.util.Log
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.launchFragmentInCompanionActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class GeologicInfoFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var geologicInfoFragment: GeologicInfoFragment
    private lateinit var geologicInfoViewModel: GeologicInfoViewModel
    private val id = "P8I"

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        launchFragmentInCompanionActivity<GeologicInfoFragment>(fragmentArgs = bundleOf("id" to id)) {
            geologicInfoFragment = this as GeologicInfoFragment
            geologicInfoViewModel = geologicInfoFragment.activityViewModels<GeologicInfoViewModel>().value
        }
    }

    @Test
    fun testFragmentCreation(){
        assertThat(geologicInfoFragment.binding.textView.text, `is`(id))
    }
}