/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.FakeRepositoryModule
import com.thegravelers.uce.R
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.launchFragmentInCompanionActivity
import com.thegravelers.uce.repository.ContentRepository
import com.thegravelers.uce.screens.companion.map.MapFragment
import com.thegravelers.uce.screens.companion.map.MapViewModel
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.screens.companion.map.geologic.GeologicInfoViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.thegravelers.uce.screens.BlankFragmentDirections
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
abstract class FeedFragmentTest{

    protected lateinit var feedFragment: ContentFeedFragment
    protected lateinit var feedViewModel: ContentFeedViewModel
    protected lateinit var navHostController: TestNavHostController
    protected lateinit var title: String
    protected lateinit var returnLabel: String

    @Before
    open fun setUp(){
        navHostController = TestNavHostController(
                ApplicationProvider.getApplicationContext())

        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        title = "content_title"
        FakeRepositoryModule.title = title
    }

    @Test
    fun testFeedFragmentCreation(){
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            feedViewModel.updateFeedItemList()
            feedViewModel.feedItemList.getOrAwaitValue().also {
                assertThat(it.first().title, `is`(title))
            }
        }
    }

    @Test
    fun testFeedFragmentNavigation(){
        onView(withText("content_title")).perform(ViewActions.click())
        Thread.sleep(1000)
        assertThat(navHostController.currentDestination?.label, `is`(returnLabel))
    }

    protected fun setUpNavHostController(fragmentId: Int){
        val inflater = navHostController.navInflater
        val graph = inflater.inflate(R.navigation.companion_navigation)
        graph.setStartDestination(fragmentId)
        navHostController.graph = graph
        Navigation.setViewNavController(feedFragment.requireView(), navHostController)
    }
}