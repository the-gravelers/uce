/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.login

import androidx.test.platform.app.InstrumentationRegistry
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.repository.UserRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LoginViewModelTest {
    @Mock
    private lateinit var userRepository: UserRepository

    private lateinit var loginViewModel: LoginViewModel

    private val validUsername = "valid"
    private val validPassword = "valid"
    private val invalidUsername = "invalid"
    private val invalidPassword = "invalid"

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        runBlocking {
            Mockito.`when`(userRepository.login(validUsername, validPassword)).thenReturn(true)
            Mockito.`when`(userRepository.login(invalidUsername, invalidPassword)).thenReturn(false)
        }
        loginViewModel = LoginViewModel(userRepository)
    }

    @Test
    fun testCheckLoginCredentials_Valid() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            loginViewModel.mUserId.set(validUsername)
            loginViewModel.mPassword.set(validPassword)
            loginViewModel.checkLoginCredentials()
            loginViewModel.loginSuccessful.getOrAwaitValue().also {
                assertThat(it, `is`(true))
            }
        }
    }

    @Test
    fun testCheckLoginCredentials_Invalid() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            loginViewModel.mUserId.set(invalidUsername)
            loginViewModel.mPassword.set(invalidPassword)
            loginViewModel.checkLoginCredentials()
            loginViewModel.loginSuccessful.getOrAwaitValue().also {
                assertThat(it, `is`(false))
            }
        }
    }
}