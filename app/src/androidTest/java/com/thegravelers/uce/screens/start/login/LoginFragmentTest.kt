/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.login

import android.util.Log
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.R
import com.thegravelers.uce.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class LoginFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var loginFragment: LoginFragment
    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setUp(){
        hiltRule.inject()
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)
        launchFragmentInHiltContainer<LoginFragment> {
            loginFragment = this as LoginFragment
            loginViewModel = loginFragment.viewModel
        }
    }

    @Test
    fun testLogin_Successful() {
        onView(withId(R.id.username_field)).perform(clearText(), typeText("valid"))
        onView(withId(R.id.password_field)).perform(clearText(), typeText("valid"))
        // TODO This click will throw a nullpointer exception because it cant run viewModelScope.launch
        // TODO In order to fix that viewModelScope.launch needs to use a Unconfined Dispatcher when running test
        // TODO onView(withId(R.id.button_login)).perform(click())
    }
}