/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.pin

import androidx.core.net.toUri
import androidx.test.platform.app.InstrumentationRegistry
import com.mapbox.mapboxsdk.geometry.LatLng
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.PinContent
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.repository.ContentRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PinViewModelTest {

    @Mock
    private lateinit var contentRepository: ContentRepository
    private lateinit var pinViewModel: PinViewModel

    private val pinId = 1L
    private val pin = Pin("test")
    private val pinContent = PinContent(pinId = pinId)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        runBlocking {
            Mockito.`when`(contentRepository.getPin(pinId)).thenReturn(pin)
            Mockito.`when`(contentRepository.getPinContent(pinId)).thenReturn(listOf(pinContent))
        }
        pinViewModel = PinViewModel(Dispatchers.Unconfined, contentRepository)
    }

    @Test
    fun testRetrievePin() {
        pinViewModel.retrievePin(pinId)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            pinViewModel.pinInFocus.getOrAwaitValue().also {
                assertThat(it, `is`(pin))
            }
        }
    }

    @Test
    fun testGetPinContent() {
        pinViewModel.getPinContent(pinId)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            pinViewModel.newPinContent.getOrAwaitValue().also {
                assertThat(it[0], `is`(pinContent))
            }
        }
    }

    @Test
    fun testCreatePin(){
        pinViewModel.setPinLocation(LatLng())
        pinViewModel.createPin("test", "test", listOf("uri1".toUri(), "uri2".toUri()))
        InstrumentationRegistry.getInstrumentation().runOnMainSync{
            pinViewModel.pinCreated.getOrAwaitValue().also {
                assert(it)
            }
        }
    }

    @Test
    fun testUpdatePin(){
        val title = "test"
        val notes = "test"
        pinViewModel.updatePin(pin, title, notes, listOf("uri1".toUri(), "uri2".toUri()))
        InstrumentationRegistry.getInstrumentation().runOnMainSync{
            pinViewModel.pinInFocus.getOrAwaitValue().also {
                assertThat(pin.title, `is`(title))
                assertThat(pin.title, `is`(notes))
            }
        }
    }
}