/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.google.gson.JsonPrimitive
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.thegravelers.uce.launchFragmentInCompanionActivity
import com.thegravelers.uce.repository.ContentRepository
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.thegravelers.uce.getOrAwaitValue
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import javax.inject.Inject

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class MapFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var mapFragment: MapFragment
    private lateinit var mapViewModel: MapViewModel

    @Inject
    lateinit var contentRepository: ContentRepository

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        hiltRule.inject()

        launchFragmentInCompanionActivity<MapFragment>{
            mapFragment = this as MapFragment
            this@MapFragmentTest.mapViewModel = mapFragment.mapViewModel
        }
    }

    @Test
    fun testMapMarkerPin() {
        val testSymbol = Mockito.mock(Symbol::class.java)
        val pinId = 0L
        Mockito.`when`(testSymbol.latLng).thenReturn(LatLng(0.0, 0.0))
        Mockito.`when`(testSymbol.data).thenReturn(JsonPrimitive(pinId))
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            mapViewModel.onMarkerClick(testSymbol)
            mapViewModel.pinInFocus.getOrAwaitValue().also {
                assertThat(it as Long, `is`(pinId))
            }
        }
    }

    @Test
    fun testMapMarkerGroundType() {
        val testSymbol = Mockito.mock(Symbol::class.java)
        val groundTypeId = "P8I"
        Mockito.`when`(testSymbol.latLng).thenReturn(LatLng(0.0, 0.0))
        Mockito.`when`(testSymbol.data).thenReturn(JsonPrimitive(groundTypeId))
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            mapViewModel.onMarkerClick(testSymbol)
            mapViewModel.groundTypeId.getOrAwaitValue().also {
                assertThat(it, `is`(groundTypeId))
            }
        }
    }

    @Test
    fun testToggleMap() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            for (layer in mapViewModel.overlayManager.style.layers) {
                if (layer.id.startsWith("Overlay/0")) {
                    assertThat(layer.visibility.value, `is`("visible"))
                }
            }
            mapViewModel.toggleMapLayer()
            for (layer in mapViewModel.overlayManager.style.layers) {
                if (layer.id.startsWith("Overlay/0")) {
                    assertThat(layer.visibility.value, `is`("none"))
                }
            }
        }
    }

    @Test
    fun testMapClick() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            mapViewModel.onMapClick(mapViewModel.overlayManager.maxBounds.center)
            assertThat(mapViewModel.createdGroundTypeSymbolList.count(), `is`(1))

        }
    }

    @Test
    fun testMapLongClick() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            Thread.sleep(5000)
            assertThat(mapFragment.bottomSheetBehavior.state, `is`(BottomSheetBehavior.STATE_HIDDEN))
            val latLng = LatLng(0.0, 0.0)
            mapViewModel.onMapLongClick(latLng)
            mapViewModel.pinInFocus.getOrAwaitValue().also {
                assertThat(it as LatLng, `is`(latLng))
            }
        }
    }
}