/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.gallery

import android.content.res.Resources
import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.WorkManager
import com.thegravelers.uce.database.UceDatabase
import com.thegravelers.uce.database.dao.*
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.network.uceserver.UceServerApiService
import com.thegravelers.uce.repository.ContentRepositoryImpl
import com.thegravelers.uce.repository.mediaaccess.MediaAccess
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@HiltAndroidTest
@MediumTest
@RunWith(AndroidJUnit4::class)
class GalleryViewModelTest {
    @get:Rule
    var hiltRule = HiltAndroidRule(this)
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var resources: Resources
    @Inject
    lateinit var database: UceDatabase
    @Inject
    lateinit var sessionManager: SessionManager
    @Inject
    lateinit var uceServerApi: UceServerApiService
    @Inject
    lateinit var mediaAccess: MediaAccess
    @Inject
    lateinit var workManager: WorkManager

    private lateinit var pinDao: PinDao
    private lateinit var pinContentDao: PinContentDao
    private lateinit var pinGroupDao: PinGroupDao
    private lateinit var cachedPinDao: CachedPinDao
    private lateinit var bookDao: BookDao
    private lateinit var contentRepositoryImpl: ContentRepositoryImpl
    private lateinit var galleryViewModel: SharedGalleryViewModel

    @Before
    fun setUp() {
        hiltRule.inject()

        pinDao = database.pinDao()
        pinContentDao = database.pinContentDao()
        pinGroupDao = database.pinGroupDao()
        cachedPinDao = database.cachedPinDao()
        bookDao = database.bookDao()
        contentRepositoryImpl = ContentRepositoryImpl(pinDao, pinContentDao, cachedPinDao, bookDao,
                pinGroupDao, sessionManager, uceServerApi, mediaAccess, workManager)

        galleryViewModel = SharedGalleryViewModel()

        galleryViewModel.addImage(Uri.EMPTY)
    }

    @Test
    fun test_AddImage() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            val size = galleryViewModel.images.value?.size!!
            galleryViewModel.addImage(Uri.EMPTY)
            assert(galleryViewModel.images.value?.size == size.plus(1))
        }
    }

    @Test
    fun test_DeleteImage() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            val size = galleryViewModel.images.value?.size!!
            val removeAtIndex = 0
            galleryViewModel.deleteImage(removeAtIndex)
            assert(galleryViewModel.images.value?.size == size.minus(1))
            galleryViewModel.notifyItemRemoved.getOrAwaitValue().also {
                assert(it == removeAtIndex)
            }
        }
    }

    @Test
    fun test_FlushImages() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            assert(galleryViewModel.images.value?.size!! > 0)
            galleryViewModel.flushImages()
            assert(galleryViewModel.images.value?.size == 0)
        }
    }
}