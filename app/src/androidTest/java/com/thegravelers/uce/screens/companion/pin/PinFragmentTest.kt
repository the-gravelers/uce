/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.pin

import android.util.Log
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.R
import com.thegravelers.uce.getOrAwaitValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.*
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.mapbox.mapboxsdk.geometry.LatLng
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import com.thegravelers.uce.*
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.PinContent
import com.thegravelers.uce.database.entity.PinWithContents
import org.junit.runner.RunWith

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class PinFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var pinFragment: PinFragment
    private lateinit var pinViewModel: PinViewModel
    private lateinit var navHostController: TestNavHostController

    private val pin = Pin(title = "test")
    private val pinWithContents = PinWithContents(pin, listOf(PinContent(pin.id, "uri1")))

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        FakeRepositoryModule.pin = pinWithContents.pin
        FakeRepositoryModule.pinWithContents = pinWithContents

        launchFragmentInHiltContainer<PinFragment>(fragmentArgs = bundleOf("pinId" to pinWithContents.pin.id)) {
            pinFragment = this as PinFragment
            setUpNavHostController(pinFragment, R.id.pinFragment)
            this@PinFragmentTest.pinViewModel = pinFragment.pinViewModel
        }
    }

    private fun setUpNavHostController(fragment: Fragment, fragmentRId: Int) {
        navHostController = TestNavHostController(ApplicationProvider.getApplicationContext())
        val inflater = navHostController.navInflater
        val graph = inflater.inflate(R.navigation.companion_navigation)
        graph.startDestination = fragmentRId
        navHostController.graph = graph
        Navigation.setViewNavController(fragment.requireView(), navHostController)
    }

    @Test
    fun testOnClickListenersEmptyPin(){
        pinViewModel.clearPinInFocus()
        pinViewModel.setPinLocation(LatLng(0.0, 0.0))
        val pref : Boolean = pinViewModel.isEditable.value ?: false
        onView(allOf(withId(R.id.edit_button), equalTo(pinFragment.binding.editButton))).perform(click())
        Thread.sleep(1000)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            assertThat(pinViewModel.isEditable.getOrAwaitValue(), `is`(!pref))
        }

        val newPinTitle = "new_pin_title"
        onView(withId(R.id.title_field)).perform(typeText(newPinTitle)).perform(closeSoftKeyboard())
        onView(withId(R.id.apply_button)).perform(click())
        Thread.sleep(1000)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            assertThat(pinViewModel.isEditable.getOrAwaitValue(), `is`(pref))
            assertThat(pinViewModel.pinCreated.getOrAwaitValue(), `is`(true))
        }
    }

    @Test
    fun testOnClickListenersLoadedPin(){
        val newPinTitle = "new_pin_title"
        val pref : Boolean = pinViewModel.isEditable.value ?: false
        Thread.sleep(1000)
        onView(allOf(withId(R.id.edit_button), equalTo(pinFragment.binding.editButton))).perform(click())
        onView(withId(R.id.title_field)).perform(clearText()).perform(typeText(newPinTitle)).perform(closeSoftKeyboard())
        onView(withId(R.id.apply_button)).perform(click())
        Thread.sleep(1000)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            assertThat(pinViewModel.isEditable.getOrAwaitValue(), `is`(pref))
            assertThat(pinViewModel.pinInFocus.getOrAwaitValue()?.title, `is`(newPinTitle))
        }
        onView(allOf(withText(newPinTitle), withId(R.id.title_field_view))).check(matches(isDisplayed()))
    }

    @Test
    fun testLoadPin(){
        pinViewModel.retrievePin(pin.id)
        pinViewModel.getPinContent(pin.id)
        onView(allOf(withText(pinWithContents.pin.title), withId(R.id.title_field_view))).check(matches(isDisplayed()))
    }

    @Test
    fun testClearPin(){
        pinViewModel.clearPinInFocus()
        Thread.sleep(1000)
        onView(withId(R.id.title_field_view)).check(matches(withText("")))
    }

    @Test
    fun testEditability(){
        onView(withId(R.id.notes_card_view)).check(matches(isDisplayed()))
        onView(withId(R.id.edit_button)).perform(click())
        onView(withId(R.id.notes_field)).check(matches(isDisplayed()))
    }

    @Test
    fun testApply_UpdatePin(){
        val newTitle = "changed"
        pinViewModel.retrievePin(pin.id)
        Thread.sleep(1000)
        onView(withId(R.id.title_field)).check(matches(withText(pin.title)))
        onView(withId(R.id.edit_button)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.title_field)).perform(clearText()).perform(typeText(newTitle)).perform(closeSoftKeyboard())
        onView(withText("Apply")).perform(click())
        onView(withId(R.id.title_field)).check(matches(withText(newTitle)))
    }

    @Test
    fun testApply_CreatePin(){
        val newTitle = "changed"
        pinViewModel.clearPinInFocus()
        onView(withId(R.id.title_field)).check(matches(withText("")))
        onView(withId(R.id.edit_button)).perform(click())
        onView(withId(R.id.title_field)).perform(clearText()).perform(typeText(newTitle)).perform(closeSoftKeyboard())
        onView(withId(R.id.apply_button)).perform(click())
        onView(withId(R.id.title_field)).check(matches(withText(newTitle)))
    }

    @Test
    fun testCancelEditLoadedPin(){
        val newPinTitle = "new_pin_title"
        val pref : Boolean = pinViewModel.isEditable.value ?: false
        Thread.sleep(1000)
        onView(allOf(withId(R.id.edit_button), equalTo(pinFragment.binding.editButton))).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.title_field)).perform(clearText()).perform(typeText(newPinTitle)).perform(closeSoftKeyboard())
        onView(withId(R.id.cancel_button)).perform(click())
        Thread.sleep(1000)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            assertThat(pinViewModel.isEditable.getOrAwaitValue(), `is`(pref))
            assertThat(pinViewModel.pinInFocus.getOrAwaitValue()?.title, `is`(pinWithContents.pin.title))
        }
        onView(allOf(withText(pinWithContents.pin.title), withId(R.id.title_field_view))).check(matches(isDisplayed()))
    }

    @Test
    fun testCancelEditEmptyPin(){
        pinViewModel.clearPinInFocus()
        val newPinTitle = "new_pin_title"
        val pref : Boolean = pinViewModel.isEditable.value ?: false
        Thread.sleep(1000)
        onView(allOf(withId(R.id.edit_button), equalTo(pinFragment.binding.editButton))).perform(click())
        onView(withId(R.id.title_field)).perform(clearText()).perform(typeText(newPinTitle)).perform(closeSoftKeyboard())
        onView(withId(R.id.cancel_button)).perform(click())
        Thread.sleep(1000)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            assertThat(pinViewModel.isEditable.getOrAwaitValue(), `is`(pref))
        }
        onView(withId(R.id.title_field_view)).check(matches(withText("")))
    }

    @Test
    fun testBookmarkPin(){
        onView(withId(R.id.bookmark)).perform(click())
        onView(withId(R.id.bookmark)).perform(click())
    }

    @Test
    fun testBottomSheetDialog_Camera(){
        onView(withId(R.id.open_picture_modal_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.edit_button)).perform(click())
        onView(withId(R.id.open_picture_modal_button)).perform(click())
        onView(withId(R.id.photo_modal_bottom_sheet)).check(matches(isDisplayed()))
        onView(withId(R.id.image_camera_button)).perform(click())
    }

    @Test
    fun testBottomSheetDialog_Gallery(){
        onView(withId(R.id.open_picture_modal_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.edit_button)).perform(click())
        onView(withId(R.id.open_picture_modal_button)).perform(click())
        onView(withId(R.id.photo_modal_bottom_sheet)).check(matches(isDisplayed()))
        onView(withId(R.id.image_gallery_button)).perform(click())
    }
}
