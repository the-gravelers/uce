/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.splashscreen

import androidx.test.platform.app.InstrumentationRegistry
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.repository.UserRepository
import com.thegravelers.uce.repository.sessionmanager.AuthInfo
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SplashScreenViewModelTest {

    @Mock
    lateinit var userRepository: UserRepository
    lateinit var splashScreenViewModel: SplashScreenViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        splashScreenViewModel = SplashScreenViewModel(userRepository)
    }

    @Test
    fun testCheckLoggedIn_True() {
        Mockito.`when`(userRepository.getAuthInfo()).thenReturn(AuthInfo())
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            splashScreenViewModel.checkIfUserLoggedIn()
            splashScreenViewModel.loggedIn.getOrAwaitValue().also {
                assert(it)
            }
        }
    }

    @Test
    fun testCheckLoggedIn_False() {
        Mockito.`when`(userRepository.getAuthInfo()).thenReturn(null)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            splashScreenViewModel.checkIfUserLoggedIn()
            splashScreenViewModel.loggedIn.getOrAwaitValue().also {
                assert(!it)
            }
        }
    }

    @Test
    fun testCheckIfInGroup(){
        runBlocking {
            Mockito.`when`(userRepository.getTeamIdFromUser()).thenReturn(1)
        }
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            splashScreenViewModel.checkIfInGroup()
            splashScreenViewModel.inTeam.getOrAwaitValue().also {
                assert(it)
            }
        }
    }
}