/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.profile

import android.util.Log
import androidx.core.os.bundleOf
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.FakeRepositoryModule
import com.thegravelers.uce.R
import com.thegravelers.uce.launchFragmentInCompanionActivity
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.screens.companion.book.BookFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
@LargeTest
class ProfileFragmentTest{

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var profileFragment: ProfileFragment
    private lateinit var profileViewModel: ProfileViewModel

    @Inject
    lateinit var sessionManager: SessionManager

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        hiltRule.inject()

        sessionManager.saveGhostMode("false")
        sessionManager.saveDarkMode("false")

        launchFragmentInCompanionActivity<ProfileFragment>{
            profileFragment = this as ProfileFragment
            profileViewModel = profileFragment.viewModel
        }
    }

    @Test
    fun testSwitchStates(){
        assertThat(profileFragment.binding.switchGhost.isChecked, `is`(false))
        assertThat(profileFragment.binding.switchDark.isChecked, `is`(false))

        onView(withId(R.id.switchGhost)).perform(ViewActions.click())
        onView(withId(R.id.switchDark)).perform(ViewActions.click())

        assertThat(profileFragment.binding.switchGhost.isChecked, `is`(true))
        assertThat(profileFragment.binding.switchDark.isChecked, `is`(true))
    }
}