/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.book

import android.util.Log
import androidx.core.os.bundleOf
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.FakeRepositoryModule
import com.thegravelers.uce.database.entity.*
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.launchFragmentInCompanionActivity
import com.thegravelers.uce.repository.ContentRepository
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class BookFragmentTest{

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var bookFragment: BookFragment
    private lateinit var bookViewModel: BookViewModel

    @Inject
    lateinit var contentRepository: ContentRepository

    private val id = 0L

    val book = Book(id, "book_title")
    val section = BookSection(id, id, "section_title", 0)
    val content = BookContent(id, id, "content", BookContentType.TEXT, 0)
    val bookWithContents = BookWithContents(book, listOf(BookSectionWithContents(section, listOf(content))))

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        FakeRepositoryModule.bookWithContents = bookWithContents

        hiltRule.inject()

        launchFragmentInCompanionActivity<BookFragment>(fragmentArgs = bundleOf("bookId" to id)) {
            bookFragment = this as BookFragment
            bookViewModel = bookFragment.viewModel
        }
    }

    @Test
    fun testFragmentCreation(){
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            bookViewModel.bookInFocus.getOrAwaitValue().also {
                assertThat(it.book.title, `is`(book.title))
                assertThat(bookFragment.binding.bookTitle.text, `is`(book.title))
            }
        }
    }
}