/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.feeds

import androidx.fragment.app.activityViewModels
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.thegravelers.uce.R
import com.thegravelers.uce.launchFragmentInCompanionActivity
import com.thegravelers.uce.launchFragmentInHiltContainer
import com.thegravelers.uce.screens.companion.camera.CameraFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class PinFeedFragmentTest: FeedFragmentTest(){

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    override fun setUp(){
        super.setUp()
        launchFragmentInHiltContainer<PinFeedFragment> {
            feedFragment = this as PinFeedFragment
            setUpNavHostController(R.id.pinFeedFragment)
            feedViewModel = feedFragment.activityViewModels<PinFeedViewModel>().value
        }
        returnLabel = "PinFragment"
    }

}