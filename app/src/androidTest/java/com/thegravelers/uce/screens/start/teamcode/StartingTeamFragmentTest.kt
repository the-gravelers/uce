/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.teamcode

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.repository.ContentRepository
import com.thegravelers.uce.screens.companion.map.MapFragment
import com.thegravelers.uce.screens.companion.map.MapViewModel
import com.thegravelers.uce.screens.companion.map.geologic.GeologicInfoViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.thegravelers.uce.*
import com.thegravelers.uce.database.entity.User
import com.thegravelers.uce.database.entity.UserGroup
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.repository.sessionmanager.UserInfo
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import javax.inject.Inject

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class StartingTeamFragmentTest{

    @get:Rule
    var hiltRule = HiltAndroidRule(this)


    @Inject
    lateinit var sessionManager: SessionManager

    private lateinit var startTeamFragment: StartingTeamFragment
    private lateinit var startTeamViewModel: StartingTeamViewModel

    @Before
    fun setUp(){
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        FakeRepositoryModule.team = UserGroupWithUsers(UserGroup(0L, "userGroupName"), listOf(User(0L, "username")))

        hiltRule.inject()

        sessionManager.saveUserInfo(UserInfo())

        launchFragmentInCompanionActivity<StartingTeamFragment>() {

            startTeamFragment = this as StartingTeamFragment
            startTeamViewModel = startTeamFragment.viewModel
        }
    }

    @Test
    fun testGetTeam(){
        onView(withText("username")).check(matches(isDisplayed()))
        onView(withText("userGroupName")).check(matches(isDisplayed()))
    }

    @Test
    fun testLeaveTeam(){
        onView(withText("Leave team")).perform(click())
        runBlocking {
            Mockito.verify(FakeRepositoryModule.userRepository).leaveTeam()
        }
    }

    @Test
    fun testStartActivity() {
        onView(withText("To app")).perform(click())
        Thread.sleep(1000)
        assertThat(startTeamFragment.activity?.intent, `is`(nullValue()))
    }

}