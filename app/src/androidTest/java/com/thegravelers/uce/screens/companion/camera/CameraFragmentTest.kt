/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.camera

import android.util.Log
import androidx.camera.core.CameraSelector
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.thegravelers.uce.R
import com.thegravelers.uce.getOrAwaitValue
import com.thegravelers.uce.launchFragmentInCompanionActivity
import com.thegravelers.uce.launchFragmentInHiltContainer
import com.thegravelers.uce.screens.companion.gallery.SharedGalleryViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock

@HiltAndroidTest
@LargeTest
@RunWith(AndroidJUnit4::class)
class CameraFragmentTest {

    private lateinit var cameraFragment: CameraFragment
    private lateinit var galleryViewModel: SharedGalleryViewModel
    private lateinit var navHostController: TestNavHostController

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()
        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        launchFragmentInHiltContainer<CameraFragment> {
            cameraFragment = this as CameraFragment
            setUpNavHostController(cameraFragment, R.id.cameraFragment)
            galleryViewModel = cameraFragment.sharedViewModel
        }
    }

    private fun setUpNavHostController(fragment: Fragment, fragmentRId: Int) {
        navHostController = TestNavHostController(ApplicationProvider.getApplicationContext())
        val inflater = navHostController.navInflater
        val graph = inflater.inflate(R.navigation.companion_navigation)
        graph.setStartDestination(fragmentRId)
        navHostController.graph = graph
        Navigation.setViewNavController(fragment.requireView(), navHostController)
    }

    @Test
    fun testTakePicture() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            galleryViewModel.images.getOrAwaitValue().also {
                assertThat(it.size, `is`(0))
            }
        }
        onView(withId(R.id.camera_capture_button)).perform(click())
        // Wait for the value to be set
        Thread.sleep(1000)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            galleryViewModel.images.getOrAwaitValue().also {
                assertThat(it.size, `is`(1))
            }
        }
    }

    @Test
    fun testChangeLens() {
        onView(withId(R.id.camera_switch_button)).perform(click())
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            assertThat(cameraFragment.getLensFacing, `is`(CameraSelector.LENS_FACING_FRONT))
        }
    }

    @Test
    fun testNavigateToGallery() {
        onView(withId(R.id.camera_capture_button)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.photo_view_button)).perform(click())
        assertThat(navHostController.currentDestination?.label, `is`("GalleryFragment"))
    }
}