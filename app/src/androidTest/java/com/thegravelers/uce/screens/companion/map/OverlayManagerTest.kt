/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.map

import android.util.Log
import androidx.fragment.app.activityViewModels
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import com.mapbox.mapboxsdk.geometry.LatLng
import com.thegravelers.uce.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.awaitility.Awaitility.await
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@MediumTest
@RunWith(AndroidJUnit4::class)
class OverlayManagerTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var overlayManager: OverlayManager
    private lateinit var mapFragment: MapFragment

    @Before
    fun setUp() {

        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val config = Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(SynchronousExecutor())
                .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        launchFragmentInHiltContainer<MapFragment> {
            mapFragment = this as MapFragment
        }

        await().until { mapFragment.mapViewModel.isInitialized }

        overlayManager = mapFragment.activityViewModels<MapViewModel>().value.overlayManager
    }

    @Test
    fun testIndexFromInput_outsideMap_null() {
        val retval = overlayManager.gridIndexFromInput(LatLng(0.0, 0.0))
        assertThat(retval, `is`(nullValue()))
    }

    @Test
    fun testIndexFromInput_withinMap_positiveIndex() {
        var retval = overlayManager.gridIndexFromInput(overlayManager.maxBounds.northWest)
        assertThat(retval, not(`is`(-1)))
        retval = overlayManager.gridIndexFromInput(overlayManager.maxBounds.northEast)
        assertThat(retval, not(`is`(-1)))
        retval = overlayManager.gridIndexFromInput(overlayManager.maxBounds.southWest)
        assertThat(retval, not(`is`(-1)))
        retval = overlayManager.gridIndexFromInput(overlayManager.maxBounds.southEast)
        assertThat(retval, not(`is`(-1)))
    }

    @Test
    fun testUpdateMap_fullyContained() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            overlayManager.updateMap(1, overlayManager.maxBounds)
            for (layer in overlayManager.style.layers) {
                if (layer.id.startsWith("Overlay/0/0"))
                    assertThat(layer.visibility.value, `is`("visible"))
                else if (layer.id.startsWith("Overlay"))
                    assertThat(layer.visibility.value, `is`("none"))
            }
        }
    }

    @Test
    fun testUpdateMap_notContained() {
        val newLat = LatLng(0.0, 0.0)
        val newQuad = OverlayQuad(newLat, newLat)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            overlayManager.updateMap(1, newQuad)
            for (layer in overlayManager.style.layers) {
                if (layer.id.startsWith("Overlay/0"))
                    assertThat(layer.visibility.value, `is`("visible"))
                else if (layer.id.startsWith("Overlay"))
                    assertThat(layer.visibility.value, `is`("none"))
            }
        }
    }

    @Test
    fun testUpdateMap_partialContained() {
        val newQuad = OverlayQuad(overlayManager.maxBounds.northWest, overlayManager.maxBounds.northWest)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            overlayManager.updateMap(100, newQuad)
            for (layer in overlayManager.style.layers) {
                if (layer.id.startsWith("Overlay/4/0") || layer.id.startsWith("Overlay/0"))
                    assertThat(layer.visibility.value, `is`("visible"))
                else if (layer.id.startsWith("Overlay"))
                    assertThat(layer.visibility.value, `is`("none"))
            }
        }
    }
}