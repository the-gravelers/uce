/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce

import android.content.Context
import androidx.room.Room
import com.thegravelers.uce.database.UceDatabase
import com.thegravelers.uce.database.dao.*
import com.thegravelers.uce.di.DatabaseModule
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
        components = [SingletonComponent::class],
        replaces = [DatabaseModule::class]
)
object FakeDatabaseModule {

    @Provides
    fun provideInMemoryDb(@ApplicationContext context: Context): UceDatabase =
            Room.inMemoryDatabaseBuilder(
                    context, UceDatabase::class.java
            ).allowMainThreadQueries()
                    .build()
    @Provides
    fun providePinDao(database: UceDatabase): PinDao {
        return database.pinDao()
    }

    @Provides
    fun providePinContentDao(database: UceDatabase): PinContentDao {
        return database.pinContentDao()
    }

    @Provides
    fun providePinGroupDao(database: UceDatabase): PinGroupDao {
        return database.pinGroupDao()
    }

    @Provides
    fun provideUserDao(database: UceDatabase): UserDao {
        return database.userDao()
    }

    @Provides
    fun provideUserGroupDao(database: UceDatabase): UserGroupDao {
        return database.userGroupDao()
    }

    @Provides
    fun provideBookDao(database: UceDatabase): BookDao {
        return database.bookDao()
    }

    @Provides
    fun provideCachedPinDao(database: UceDatabase): CachedPinDao{
        return database.cachedPinDao()
    }
}