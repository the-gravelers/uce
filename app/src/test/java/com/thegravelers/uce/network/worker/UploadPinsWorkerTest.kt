/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.worker

import android.content.ContentResolver
import android.content.Context
import androidx.work.Data
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.squareup.moshi.Moshi
import com.thegravelers.uce.database.dao.PinDao
import com.thegravelers.uce.database.entity.Pin
import com.thegravelers.uce.database.entity.PinWithContents
import com.thegravelers.uce.network.uceserver.GetPinResponse
import com.thegravelers.uce.network.uceserver.PostPinRequest
import com.thegravelers.uce.network.uceserver.PostPinResponse
import com.thegravelers.uce.network.uceserver.UceServerApiService
import com.thegravelers.uce.util.WorkerConstants
import com.thegravelers.uce.util.roundDoubleTo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import kotlin.io.path.ExperimentalPathApi

@ExperimentalCoroutinesApi
@OptIn(ExperimentalPathApi::class)
@RunWith(MockitoJUnitRunner::class)
class UploadPinsWorkerTest {

    @Mock
    lateinit var serverApi: UceServerApiService

    @Mock
    lateinit var pinDao: PinDao

    @Mock
    lateinit var context: Context

    @Mock
    lateinit var contentResolver: ContentResolver

    @Mock
    lateinit var moshi: Moshi

    lateinit var worker: UploadPinsWorker

    /**
     * Input data of this mock is set in setUp().
     * You can override the getInputData() for custom input when creating a worker.
     */
    @Mock
    lateinit var workerParams: WorkerParameters

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        // We set false as our input data, as RefreshTokenWorker otherwise skips the doWork()

        worker = UploadPinsWorker(context, workerParams, serverApi, pinDao, moshi, contentResolver)
    }

    @Test
    fun testDoWork_Upload_NoContent() = runBlockingTest {
        // We want to upload pins with ids from 1..5
        val inputData = Data.Builder()
                .putLongArray(WorkerConstants.KEY_UPLOAD_PINS_IDS,
                                longArrayOf(1L, 2L, 3L, 4L, 5L))
                .build()
        Mockito.`when`(workerParams.getInputData())
                .thenReturn(inputData)

        val pins: MutableList<Pin> = mutableListOf()
        val requests: MutableList<PostPinRequest> = mutableListOf()

        // We create five pins which have serverId -1
        // They should be uploaded (not updated)
        for(i in 1L..5L){
            val pin = Pin(
                    "$i",
                    i.toDouble(),
                    i.toDouble(),
            )
            pin.id = i
            pins.add(pin)
            Mockito.`when`(pinDao.getPinWithContents(i))
                    .thenReturn(PinWithContents(
                    pin,
                    emptyList()
            ))
            val request = PostPinRequest(
                    pin.title,
                    roundDoubleTo(pin.lat, 6),
                    roundDoubleTo(pin.lon, 6),
                    pin.text)
            requests.add(request)
            Mockito.`when`(serverApi.postPin(request))
                    .thenReturn(retrofit2.Response.success(
                    PostPinResponse(i+10)
            ))
        }

        val result = worker.doWork()

        assertThat(result, `is`(ListenableWorker.Result.success()))
        for((pin, request) in pins.zip(requests)){
            assertThat(pin.serverId, `is`(pin.id+10))
            Mockito.verify(serverApi).postPin(request)
        }
    }

    @Test
    fun testDoWork_Update_NoContent() = runBlockingTest {
        // We want to upload pins with ids from 1..5
        val inputData = Data.Builder()
                .putLongArray(WorkerConstants.KEY_UPLOAD_PINS_IDS,
                        longArrayOf(1L, 2L, 3L, 4L, 5L))
                .build()
        Mockito.`when`(workerParams.getInputData())
                .thenReturn(inputData)

        val pins: MutableList<Pin> = mutableListOf()
        val requests: MutableList<PostPinRequest> = mutableListOf()

        // We create five pins which have serverId -1
        // They should be uploaded (not updated)
        for(i in 1L..5L){
            val pin = Pin(
                    "$i",
                    i.toDouble(),
                    i.toDouble(),
                    serverId = i+10
            )
            pin.id = i
            pins.add(pin)
            Mockito.`when`(pinDao.getPinWithContents(i))
                    .thenReturn(PinWithContents(
                    pin,
                    emptyList()
            ))
            val request = PostPinRequest(
                    pin.title,
                    roundDoubleTo(pin.lat, 6),
                    roundDoubleTo(pin.lon, 6),
                    pin.text)
            requests.add(request)
            Mockito.`when`(serverApi.updatePin(pin.serverId, request))
                    .thenReturn(retrofit2.Response.success(Unit))
            Mockito.`when`(serverApi.getPin(pin.serverId))
                    .thenReturn(retrofit2.Response.success(
                            GetPinResponse(
                                  i,0L,"",0.0,0.0,0L,"",emptyList())
                    ))
        }

        val result = worker.doWork()

        assertThat(result, `is`(ListenableWorker.Result.success()))
        for((pin,request) in pins.zip(requests)){
            Mockito.verify(serverApi).updatePin(pin.serverId, request)
        }
    }

    @Test
    fun testDoWork_Upload_NoResponse() = runBlockingTest{
        val inputData = Data.Builder()
                .putLongArray(WorkerConstants.KEY_UPLOAD_PINS_IDS,
                        longArrayOf(1L))
                .build()
        Mockito.`when`(workerParams.getInputData()).thenReturn(inputData)

        val pin = Pin("title", 0.0, 0.0)
        pin.id = 1L

        Mockito.`when`(pinDao.getPinWithContents(1L)).thenReturn(PinWithContents(
                pin,
                emptyList()
        ))

        val request = PostPinRequest(
                pin.title,
                roundDoubleTo(pin.lat, 6),
                roundDoubleTo(pin.lon, 6),
                pin.text)
        Mockito.`when`(serverApi.postPin(request)).thenReturn(retrofit2.Response.error(
                400,
                "Invalid request".toResponseBody("application/json".toMediaTypeOrNull()))
        )

        val response = worker.doWork()

        assertThat(response, `is`(ListenableWorker.Result.retry()))

    }
}