/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.network.worker

import android.content.Context
import androidx.work.Data
import androidx.work.ListenableWorker.Result
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.thegravelers.uce.network.uceserver.AuthResponse
import com.thegravelers.uce.network.uceserver.RefreshTokenRequest
import com.thegravelers.uce.network.uceserver.UceServerApiService
import com.thegravelers.uce.repository.sessionmanager.AuthInfo
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import com.thegravelers.uce.util.WorkerConstants
import com.thegravelers.uce.util.getCurrentTimeSeconds
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RefreshTokenWorkerTest {

    @Mock
    lateinit var serverApi: UceServerApiService

    @Mock
    lateinit var sessionManager: SessionManager

    @Mock
    lateinit var context: Context

    lateinit var worker: RefreshTokenWorker

    /**
     * Input data of this mock is set in setUp().
     * You can override the getInputData() for custom input when creating a worker.
     */
    @Mock
    lateinit var workerParams: WorkerParameters

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        // We set false as our input data, as RefreshTokenWorker otherwise skips the doWork()
        val inputData = Data.Builder()
                .putBoolean(WorkerConstants.KEY_REFRESH_TOKEN_FIRST, false)
                .build()
        Mockito.`when`(workerParams.getInputData()).thenReturn(inputData)

        worker = RefreshTokenWorker(context, workerParams, sessionManager, serverApi)
    }

    @Test
    fun testDoWork_ValidRequest() = runBlockingTest {
        val oldAuthInfo = AuthInfo(
                1L,
                "MOCKED_OLD_AUTH_TOKEN",
                "MOCKED_OLD_REFRESH_TOKEN",
                100L)
        Mockito.`when`(sessionManager.fetchAuthInfo())
                .thenReturn(oldAuthInfo)

        // Mock our server response to give back a valid response
        val request = RefreshTokenRequest(
                refreshToken = "MOCKED_OLD_REFRESH_TOKEN",
                sessionId = 1L)
        val response = AuthResponse(
                1L,
                "MOCKED_NEW_AUTH_TOKEN",
                "bearer",
                100L,
                "MOCKED_NEW_REFRESH_TOKEN")
        Mockito.`when`(serverApi.refreshToken(request))
                .thenReturn(retrofit2.Response.success(200, response))

        val result = worker.doWork()

        val expected = Result.success(
                workDataOf(WorkerConstants.KEY_REFRESH_TOKEN_FIRST to false))
        val expectedAuthInfo = AuthInfo(
                response.sessionId,
                response.accessToken,
                response.refreshToken,
                response.expiresIn + getCurrentTimeSeconds()
        )

        assertThat(result, `is`(expected))
        Mockito.verify(serverApi).refreshToken(request)
        Mockito.verify(sessionManager).saveAuthInfo(expectedAuthInfo)
    }

    @Test
    fun testRefreshTokenWorker_BadRequest() = runBlockingTest {
        val oldAuthInfo = AuthInfo(
                1L,
                "MOCKED_AUTH_TOKEN",
                "ALTERED_REFRESH_TOKEN",
                100L)
        Mockito.`when`(sessionManager.fetchAuthInfo())
                .thenReturn(oldAuthInfo)

        // Mock our server response to give back an invalid response
        val request = RefreshTokenRequest(
                refreshToken = "ALTERED_REFRESH_TOKEN",
                sessionId = 1L)
        Mockito.`when`(serverApi.refreshToken(request))
                .thenReturn(retrofit2.Response.error(
                        400,
                        "Invalid refresh token".toResponseBody("application/json".toMediaTypeOrNull())))

        val result = worker.doWork()

        assertThat(result, `is`(Result.failure()))
    }

    @Test
    fun testRefreshTokenWorker_404Response() = runBlockingTest {
        val oldAuthInfo = AuthInfo(
                1L,
                "MOCKED_AUTH_TOKEN",
                "MOCKED_REFRESH_TOKEN",
                100L)
        Mockito.`when`(sessionManager.fetchAuthInfo())
                .thenReturn(oldAuthInfo)

        // Mock our server response to give back an invalid response
        val request = RefreshTokenRequest(
                refreshToken = "MOCKED_REFRESH_TOKEN",
                sessionId = 1L)
        Mockito.`when`(serverApi.refreshToken(request))
                .thenReturn(retrofit2.Response.error(
                        404,
                        "Page not found".toResponseBody("application/json".toMediaTypeOrNull())))

        val result = worker.doWork()

        assertThat(result, `is`(Result.retry()))
    }
}