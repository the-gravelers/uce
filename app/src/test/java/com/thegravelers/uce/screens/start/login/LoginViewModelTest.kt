/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.start.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.databinding.ObservableField
import com.thegravelers.uce.repository.UserRepositoryImpl
import com.thegravelers.uce.repository.sessionmanager.SessionManager
import dagger.hilt.android.testing.HiltAndroidTest
import getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.*
import org.junit.Test
import org.junit.Assert.*
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.mockito.Mockito

//@HiltAndroidTest
//@ExperimentalCoroutinesApi
//@RunWith(MockitoJUnitRunner::class)
//class LoginViewModelTest {
//
//    @get:Rule
//    var instantExecutorRule = InstantTaskExecutorRule()
//
//    private lateinit var userRepository: UserRepositoryImpl
//    private lateinit var sessionManager: SessionManager
//
//    @Before
//    fun setUp() {
//        Dispatchers.setMain(Dispatchers.Unconfined)
//
//        userRepository = Mockito.mock(UserRepositoryImpl::class.java)
//        sessionManager = Mockito.mock(SessionManager::class.java)
//    }
//
//    @After
//    fun tearDown() {
//        Dispatchers.resetMain()
//    }
//
//    @Test
//    fun checkLoginCredentials() {
//
//        runBlocking {
//            Mockito.`when`(userRepository.login("User", "Password")).thenReturn(true)
//            //test if user is only filled in
//            val loginViewModel = LoginViewModel(userRepository, sessionManager)
//            loginViewModel.mUserId = ObservableField("User")
//            loginViewModel.checkLoginCredentials()
//            var value = loginViewModel.loginSuccessful.getOrAwaitValue()
//            assertThat(value, `is`(not(nullValue())))
//            assertFalse(value)
//
//            //test if password is only filled in
//            loginViewModel.mPassword = ObservableField("Password")
//            loginViewModel.checkLoginCredentials()
//            value = loginViewModel.loginSuccessful.getOrAwaitValue()
//            assertThat(value, `is`(not(nullValue())))
//            assertFalse(value)
//
//            //test if password and user is filled in
//            loginViewModel.mPassword = ObservableField("Password")
//            loginViewModel.mUserId = ObservableField("User")
//            loginViewModel.checkLoginCredentials()
//            value = loginViewModel.loginSuccessful.getOrAwaitValue()
//            assertThat(value, `is`(not(nullValue())))
//            assertTrue(value)
//        }
//        //TODO test if correct password user combinations are passing with server
//    }
//}
