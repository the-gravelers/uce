/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.screens.companion.team

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.thegravelers.uce.database.entity.User
import com.thegravelers.uce.database.entity.UserGroup
import com.thegravelers.uce.database.entity.UserGroupWithUsers
import com.thegravelers.uce.repository.UserRepository
import com.thegravelers.uce.repository.UserRepositoryImpl
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import timber.log.Timber

//@HiltAndroidTest
//@ExperimentalCoroutinesApi
//@RunWith(MockitoJUnitRunner::class)
// class TeamMembersViewModelTest : TestCase() {
//
//    private lateinit var userRepository: UserRepository
//    @get:Rule
//    var instantExecutorRule = InstantTaskExecutorRule()
//    lateinit var viewModel: TeamMembersViewModel
//
//    @Before
//    public override fun setUp() {
//        userRepository = Mockito.mock(UserRepository::class.java)
//        viewModel = TeamMembersViewModel(userRepository)
//        super.setUp()
//    }
//
//    @Test
//    fun testRefreshTeamList_SingleTeam() {
//        val team = flowOf(UserGroupWithUsers(UserGroup(1), listOf(User(1))))
//        runBlocking {
//            Mockito.`when`(userRepository.getTeam(anyLong())).thenReturn(team)
//            viewModel.refreshTeamList()
//            assertThat(viewModel.teamList.value, `is`(notNullValue()))
//            viewModel.teamList.value?.let { assertThat(it, `is`(mutableListOf(team)) )  }
//        }
//    }
//
//}
