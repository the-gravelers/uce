/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.util.extensions

import org.junit.Test

class LiveDataExtTest {

    @Test
    fun testMutableLiveData_add(){
        val mutableList = mutableListOf<Int>()
        mutableList.add(1)
        assert(mutableList.contains(1))
    }

    @Test
    fun testMutableLiveData_clear(){
        val mutableList = mutableListOf(0, 1, 2)
        mutableList.clear()
        assert(mutableList.size == 0)
    }

    @Test
    fun testMutableLiveData_removeAt(){
        val mutableList = mutableListOf(0, 1, 2)
        mutableList.removeAt(1)
        assert(mutableList[1] == 2)
    }
}