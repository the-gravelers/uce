/*
 * This program has been developed by students from the bachelor Computer
 * Science at Utrecht University within the Software Project course. © Copyright
 * Utrecht University (Department of Information and Computing Sciences)
 */

package com.thegravelers.uce.util.extensions

import androidx.lifecycle.MutableLiveData
import com.mapbox.mapboxsdk.geometry.LatLng
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
/**
 * Contains tests for LatLngExt.kt and LiveDataExt.kt in util/extensions
 */
@RunWith(MockitoJUnitRunner::class)
class LatLngExtensionTest{

    @Test
    fun latLngExtensionTest(){
        val latLngs = listOf(
                LatLng(42.743259, 0.906432),
                LatLng(42.837997, -1.108500),
                LatLng(41.392934, -3.399095),
                LatLng(63.629383, 14.470717),
                LatLng(48.576867, 12.503581),
                LatLng(-29.808089, 28.235331),
                LatLng(73.825691, -20.001203)
        )
        val utmStrings = listOf(
                "31T 328644E 4734430N",
                "30T 654579E 4744560N",
                "30T 466635E 4582455N",
                "33V 473769E 7055825N",
                "33U 315871E 5383428N",
                "35J 619378E 6701840S",
                "27X 531054E 8192851N"
        )
        val pairs = latLngs.zip(utmStrings)

        for((latlng, utm) in pairs){
            assertThat(latlng.toUtm(), `is`(utm))
        }
    }
}