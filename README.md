# Uce-app

## Introduction

Utrecht Companion to the Earth is a project run by students from Utrecht University. It consists of four parts with this being the Android app and the other being the [preprocessor](https://git.science.uu.nl/the-gravelers/kml-preprocessor), [teacher webmodule](https://git.science.uu.nl/the-gravelers/teacher-module) and server.

The Android application gives students the opportunity to share their field discoveries, even when they are far away. These discoveries are  in the formof pins on a map. Users can create pins that contains images, title and text. When pins are created other users can view them.   Also the app contains a library where they can view wiki-like items, that teachers created through the teacher module.

## Documentation

You can find documentation [here](https://the-gravelers.gitlab.io/uce-docs).

### How to build it yourself
We document our kotlin code using [KDoc](https://kotlinlang.org/docs/kotlin-doc.html) and generate documentation from that using [Dokka](https://kotlinlang.org/docs/kotlin-doc.html)

- In the Android Studio terminal type or go to the root folder and type in cmd:
``` 
.\gradlew dokkaHtml
```
- Documentation can then be found in: `root\app\build\dokka\html\app`

## Code coverage
For code coverage we use `Jacoco`. To get a coverage report we wrote a task named `codeCoverageReport` that can be found in `app > build.gradle`. It will run both unit and instrumented test and will combine the results in a single report.

- In the Android Studio terminal type:
```
.\gradlew codeCoverageReport
```
- A test summery can be found in: `root\app\build\reports\androidTests\connected\flavors\debugAndroidTest`
- A detailed report can be found in: `root\app\build\reports\jacoco\codeCoverageReport\html`


